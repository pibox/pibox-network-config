/*******************************************************************************
 * wifiscan
 *
 * wifiscan.c:  Manage background wifi scans
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 * 
 * Based on:
 * Custom GTK+ widget - http://zetcode.com/tutorials/gtktutorial/customwidget/
 * Clock Widget - https://github.com/humbhenri/clocks/tree/master/gtk-clock
 ******************************************************************************/
#define WIFISCANWIDGET_C

#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <glib.h>
#include <pibox/log.h>
#include "wifiscanwidget.h"
#include "pnc.h"

#define RADIAN(x) (((x) * M_PI/30.0) - M_PI/2.0)
#define TITLE_FONT_SIZE     20
#define DATA_MEDIUM_SIZE    14
#define DATA_SMALL_SIZE     12
#define BSS_FONT_SIZE       11
#define BSS_SMALL_SIZE      9

/* A big static buffer for building multiline text strings. */
#define STR_SIZE    2048
static char str[STR_SIZE];

/* Default base color */
GdkColor    base;

/*
 * Data we save for BSS entries.
 */
typedef struct _bss_cb_t
{
    cairo_t *cr;
    guint    offset_x;
    guint    offset_y;

    guint    origin_x;
    guint    origin_y;
    guint    span;
    guint    width;
} BSS_CB_T;

/*
 * Channel frequency tick offset for graphs.
 */
typedef struct _ch_tick_t
{
    guint    ch;
    guint    low;
    guint    center;
    guint    high;
} CH_TICK_T;

/*
 * Local copy of widget structure so some callbacks can
 * access it.
 */
static GtkWifiscan *local_wifiscan = NULL;

/*
 * Static table mapping channels to tick marks.
 * This is based on the low/center/high channel frequencies and how
 * those overlap.
 * These are based on 
 * http://www.radio-electronics.com/info/wireless/wi-fi/80211-channels-number-frequencies-bandwidth.php
 */
static CH_TICK_T   ch_ticks[] = {
    {  1,  0,  3,  8 },
    {  2,  1,  5, 11 },
    {  3,  2,  7, 14 },
    {  4,  4, 10, 17 },
    {  5,  6, 13, 20 },
    {  6,  9, 16, 23 },
    {  7, 12, 19, 26 },
    {  8, 15, 22, 29 },
    {  9, 18, 25, 32 },
    { 10, 21, 28, 35 },
    { 11, 24, 31, 36 },
    { 12, 27, 33, 37 },
    { 13, 30, 35, 38 },
    { 14, 36, 39, 40 }
};

/*
 *========================================================================
 * Prototypes
 *========================================================================
 */
static void gtk_wifiscan_class_init(GtkWifiscanClass *klass);
static void gtk_wifiscan_init(GtkWifiscan *wifiscan);
static void gtk_wifiscan_size_request(GtkWidget *widget, GtkRequisition *requisition);
static void gtk_wifiscan_size_allocate(GtkWidget *widget, GtkAllocation *allocation);
static void gtk_wifiscan_realize(GtkWidget *widget);
static gboolean gtk_wifiscan_expose(GtkWidget *widget, GdkEventExpose *event);
static void gtk_wifiscan_paint(GtkWidget *widget);
static void gtk_wifiscan_destroy(GtkObject *object);

static void free_key( gpointer data );
static void free_value( gpointer data );

/*
 *========================================================================
 *========================================================================
 * Private API
 *========================================================================
 *========================================================================
 */
GtkType
gtk_wifiscan_get_type(void)
{
    static GtkType gtk_wifiscan_type = 0;
    if (!gtk_wifiscan_type) {
        static const GtkTypeInfo gtk_wifiscan_info = {
            "GtkWifiscan",
            sizeof(GtkWifiscan),
            sizeof(GtkWifiscanClass),
            (GtkClassInitFunc) gtk_wifiscan_class_init,
            (GtkObjectInitFunc) gtk_wifiscan_init,
            NULL,
            NULL,
            (GtkClassInitFunc) NULL
        };
        gtk_wifiscan_type = gtk_type_unique(GTK_TYPE_WIDGET, &gtk_wifiscan_info);
    }
    return gtk_wifiscan_type;
}

GtkWidget * gtk_wifiscan_new()
{
    return GTK_WIDGET(gtk_type_new(gtk_wifiscan_get_type()));
}

static void
gtk_wifiscan_class_init(GtkWifiscanClass *klass)
{
    GtkWidgetClass *widget_class;
    GtkObjectClass *object_class;

    widget_class = (GtkWidgetClass *) klass;
    object_class = (GtkObjectClass *) klass;

    widget_class->realize = gtk_wifiscan_realize;
    widget_class->size_request = gtk_wifiscan_size_request;
    widget_class->size_allocate = gtk_wifiscan_size_allocate;
    widget_class->expose_event = gtk_wifiscan_expose;

    object_class->destroy = gtk_wifiscan_destroy;
}

static void
gtk_wifiscan_init(GtkWifiscan *wifiscan)
{
    wifiscan->wifi_devices = NULL;
    wifiscan->bss_list = NULL;
    wifiscan->channels = NULL;
    wifiscan->pixmap = NULL;
    wifiscan->color_hash = g_hash_table_new_full (
                                g_str_hash,   /* Hash function  */
                                g_str_equal,  /* Comparator     */
                                free_key,     /* Key destructor */
                                free_value);  /* Val destructor */
    base.red   = 200;
    base.green = 200;
    base.blue  = 200;
    local_wifiscan = wifiscan;
}

static void
gtk_wifiscan_size_request(GtkWidget *widget, GtkRequisition *requisition)
{
    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_WIFISCAN(widget));
    g_return_if_fail(requisition != NULL);
}

static void
gtk_wifiscan_size_allocate(GtkWidget *widget, GtkAllocation *allocation)
{
    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_WIFISCAN(widget));
    g_return_if_fail(allocation != NULL);

    widget->allocation = *allocation;
    if (GTK_WIDGET_REALIZED(widget)) {
        gdk_window_move_resize(
            widget->window,
            allocation->x, allocation->y,
            allocation->width, allocation->height
        );
    }
}

static void
gtk_wifiscan_realize(GtkWidget *widget)
{
    GdkWindowAttr attributes;
    guint attributes_mask;
    GtkStyle *style;
    GtkAllocation alloc;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_WIFISCAN(widget));

    GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);

    gtk_widget_get_allocation(widget, &alloc);

    piboxLogger(LOG_INFO, "alloc.x: %d\n", alloc.x);
    piboxLogger(LOG_INFO, "alloc.y: %d\n", alloc.y);
    piboxLogger(LOG_INFO, "alloc.w: %d\n", alloc.width);
    piboxLogger(LOG_INFO, "alloc.h: %d\n", alloc.height);

    attributes.window_type = GDK_WINDOW_CHILD;
    attributes.x           = alloc.x;
    attributes.y           = alloc.y;
    attributes.width       = alloc.width;
    attributes.height      = alloc.height;
    attributes.wclass      = GDK_INPUT_OUTPUT;
    attributes.event_mask  = gtk_widget_get_events(widget) | GDK_EXPOSURE_MASK;
    attributes_mask        = GDK_WA_X | GDK_WA_Y;

    widget->window = gdk_window_new(
        gtk_widget_get_parent_window (widget),
        & attributes, attributes_mask
    );

    style = gtk_widget_get_style (widget);
    if (style != NULL) 
    {
        GTK_WIFISCAN(widget)->bg = style->bg[GTK_STATE_NORMAL];
        GTK_WIFISCAN(widget)->fg = style->fg[GTK_STATE_NORMAL];
    }

    gdk_window_set_user_data(widget->window, widget);
    widget->style = gtk_style_attach(widget->style, widget->window);
    gtk_style_set_background(widget->style, widget->window, GTK_STATE_NORMAL);
}

static gboolean
gtk_wifiscan_expose(GtkWidget *widget, GdkEventExpose *event)
{
    int width, height;

    g_return_val_if_fail(widget != NULL, FALSE);
    g_return_val_if_fail(GTK_IS_WIFISCAN(widget), FALSE);
    g_return_val_if_fail(event != NULL, FALSE);

    // Copy the entire pixmap over the widget's drawing area.
    if ( GTK_WIFISCAN(widget)->pixmap != NULL )
    {
        gdk_draw_drawable(widget->window,
            widget->style->fg_gc[GTK_WIDGET_STATE(widget)], GTK_WIFISCAN(widget)->pixmap,
            event->area.x, event->area.y,
            event->area.x, event->area.y,
            event->area.width, event->area.height);
    }
    return TRUE;
}

/*========================================================================
 * Name:   free_key
 * Prototype:  void free_key( gpointer data )
 *
 * Description:
 * Frees up the storage allocated for the deep copy.
 *========================================================================*/
static void
free_key( gpointer data )
{
    g_free(data);
}

/*========================================================================
 * Name:   free_value
 * Prototype:  void free_value( gpointer data )
 *
 * Description:
 * Frees up the storage allocated for the deep copy.
 *========================================================================*/
static void
free_value( gpointer data )
{
    g_free(data);
}

/*
 *========================================================================
 * Name:    generateRandomColor
 * Prototype:   void generateRandomColor( GdkColor *mix )
 *
 * Description:
 * Generate a new color using the golden ratio.
 * Color mix is returned in the provided arguments.
 *
 * Arguments:
 * GdkColor *mix        Base color to generate the new color from.
 *========================================================================
 */
static void generateRandomColor(GdkColor *mix)
{
    int red   = rand() % 256;
    int green = rand() % 256;
    int blue  = rand() % 256;

    /* mix the color */
    if (mix != NULL) 
    {
        mix->red   = (red + mix->red) / 2;
        mix->green = (green + mix->green) / 2;
        mix->blue  = (blue + mix->blue) / 2;
    }
}

/*
 *========================================================================
 * Name:    listWifiDevices
 * Prototype:   void listWifiDevices( gpointer item, gpointer user_data )
 *
 * Description:
 * Callback when iterating over a GSList of devices to build a text list.
 *========================================================================
 */
static void
listWifiDevices( gpointer item, gpointer user_data )
{
    char    *ptr = (char *)user_data;
    if ( strlen(ptr) == 0 )
    {
        sprintf(ptr, "%s", (char *)item);
    }
    else
    {
        strcat(ptr, ",");
        strcat(ptr, (char *)item);
    }
}

/*
 *========================================================================
 * Name:    graphBSS
 * Prototype:   void graphBSS( gpointer item, gpointer user_data )
 *
 * Description:
 * Callback when iterating over a GSList of BSS entries to build a text list.
 *========================================================================
 */
static void
graphBSS( gpointer item, gpointer user_data )
{
    BSS_T                   *bss = (BSS_T *)item;
    BSS_CB_T                *bss_cb_data = (BSS_CB_T *)user_data;
    GdkColor                *mix;
    char                    *ssid;
    char                    buf[128];
    char                    ssid_str[33];
    char                    key[128];
    guint                   low_y, center_y, hi_y;
    guint                   radius;
    gfloat                  percent;
    guint                   box_x, box_y, box_width, box_height;
    cairo_text_extents_t    extents;

    // piboxLogger(LOG_INFO, "Entered: str = %s\n", str);
    if ( bss == NULL )
    {
        piboxLogger(LOG_ERROR, "BSS user data is null!\n");
        return;
    }

    piboxLogger(LOG_TRACE1, 
        "BSS entry:\n"
        "ssid: %s\n"
        "signal: %d\n"
        "channel: %d\n",
        bss->ssid, bss->signal, bss->channel
        );

    /*
     * We don't handle 5GHz yet.
     */
    if ( bss->channel > 14 )
    {
        piboxLogger(LOG_ERROR, "========= BSS channel is > 14! ===========\n");
        return;
    }

    /* How do these happen? */
    if ( bss->channel < 1 )
    {
        piboxLogger(LOG_ERROR, "========= BSS channel is < 1! ===========\n");
        return;
    }

    if ( (bss->ssid != NULL) && (strlen(bss->ssid) > 0) )
    {
        ssid = bss->ssid;
    }
    else
    {
        ssid = "Unknown";
    }
    memset(ssid_str, 0, 33);
    if ( strlen(ssid) > 29 )
    {
        memcpy(ssid_str, ssid, 29);
        memcpy((ssid_str+29), "...", 3);
    }
    else
    {
        memcpy(ssid_str, ssid, strlen(ssid));
    }
    sprintf(buf, "%02d dbm, ch %d %s", bss->signal, bss->channel, ssid_str);

    /*
     * Select a color for this BSS entry.
     */
    sprintf(key, "%02d %s", bss->channel, ssid);
    if ( g_hash_table_contains (local_wifiscan->color_hash, key) == FALSE )
    {
        mix = (GdkColor *)calloc(1,sizeof(GdkColor));
        mix->red   = base.red;
        mix->green = base.green;
        mix->blue  = base.blue;
        generateRandomColor(mix);
        g_hash_table_insert(local_wifiscan->color_hash,
                (gpointer)strdup(key), (gpointer)mix);
    }
    else
        mix = g_hash_table_lookup (local_wifiscan->color_hash, key);

    /* 
     * Compute Y-radius of a stretched circle.
     * We map from -100 to -30 dBm, which is 70 units.
     */
    percent = (100 - (-1 * bss->signal)) / 70.0;
    radius = (guint)(bss_cb_data->width * percent);
    piboxLogger(LOG_INFO, "Width: %d, Percent: %d, Radius: %d\n", 
            bss_cb_data->width, percent, radius);

    /* Computer Y offsets from origin for all three frequency positions for this channel */
    low_y    = ch_ticks[bss->channel-1].low * bss_cb_data->span;
    center_y = ch_ticks[bss->channel-1].center * bss_cb_data->span;
    hi_y     = ch_ticks[bss->channel-1].high * bss_cb_data->span;
    piboxLogger(LOG_INFO, "span %d, low/center/high Y: %d / %d / %d\n", 
        bss_cb_data->span, low_y, center_y, hi_y);

    /* 
     * Define a box that will contain a scaled/translated arc.
     * Width, height are doubled because we want a half ellipse when we're done.
     */
    box_x      = bss_cb_data->origin_x;
    box_y      = bss_cb_data->origin_y + low_y;
    box_width  = 2*radius;
    box_height = (hi_y - low_y);
    piboxLogger(LOG_INFO, "Box x/y w/h: %d / %d, %d / %d\n", box_x, box_y, box_width, box_height);

    /* Save our cairo config before our transformations on the arc */
    cairo_save(bss_cb_data->cr);

    /* Move to the top of the arc to avoid painting a line from the last point. */
    cairo_move_to(bss_cb_data->cr, bss_cb_data->origin_x, bss_cb_data->origin_y);
    cairo_new_path(bss_cb_data->cr);

    /*
     * Set a new line width.
     */
    cairo_set_line_width(bss_cb_data->cr, 0.75);

    /*
     * Translate our origin so it's over the center frequency, or the mid point between the low and
     * high frequencies.
     */
    cairo_translate( bss_cb_data->cr, bss_cb_data->origin_x, bss_cb_data->origin_y + center_y);

    /*
     * Scale the the drawing so it fits in the box dimenions.
     */
    cairo_scale (bss_cb_data->cr, box_width / 2.0, box_height / 2.0);

    /*
     * Fill then draw the arc.
     */
    cairo_set_source_rgba(bss_cb_data->cr, mix->red/256.0, mix->green/256.0, mix->blue/256.0, 0.2);
    cairo_arc(bss_cb_data->cr, 0.0, 0.0, 1.0, 0, 2 * M_PI);
    cairo_fill(bss_cb_data->cr);
    cairo_set_source_rgb(bss_cb_data->cr, mix->red/256.0, mix->green/256.0, mix->blue/256.0);
    cairo_arc(bss_cb_data->cr, 0.0, 0.0, 1.0, 0, 2 * M_PI);

    /*
     * Reset our drawing context
     */
    cairo_restore(bss_cb_data->cr);

    /* Print the SSIDs. */
    cairo_set_font_size(bss_cb_data->cr, BSS_FONT_SIZE);
    cairo_set_source_rgb(bss_cb_data->cr, mix->red/256.0, mix->green/256.0, mix->blue/256.0);
    cairo_text_extents(bss_cb_data->cr, buf, &extents);
    cairo_move_to(bss_cb_data->cr, 
            bss_cb_data->offset_x+(bss_cb_data->width/2), bss_cb_data->offset_y);
    cairo_show_text(bss_cb_data->cr, buf);
    bss_cb_data->offset_y += 20;

    /*
     * Stroke it.
     */
    cairo_stroke(bss_cb_data->cr);
}

/*
 *========================================================================
 * Name:    gtk_wifiscan_paint
 * Prototype:   void gtk_wifiscan_paint( GtkWidget *widget )
 *
 * Description:
 * Handle painting (data update) of the widget.
 *========================================================================
 */
static void
gtk_wifiscan_paint(GtkWidget *widget)
{
    int                     i;
    int                     width, height;
    int                     length, offset;
    int                     cnt;
    int                     dbm, ch;
    gint                    offset_x, offset_y;
    char                    *ptr;
    char                    *bss_save;
    char                    buf[4];
    cairo_t                 *cr_pixmap;
    cairo_surface_t         *cst;
    cairo_t                 *cr;
    cairo_text_extents_t    extents;
    gpointer                key, value;
    GHashTableIter          iter;
    GdkPixmap               *pixmap;
    GdkColor                mix;
    static int              painting = 0;
    float                   fg_r;
    float                   fg_g;
    float                   fg_b;
    BSS_CB_T                bss_cb_data;

    /* Avoid multiple calls */
    if ( painting )
        return;

    painting = 1;
    piboxLogger(LOG_TRACE2, "Entered\n");

    /* Get the foreground color. */
    fg_r = (float)(GTK_WIFISCAN(widget)->fg.red)/65535;
    fg_g = (float)(GTK_WIFISCAN(widget)->fg.green)/65535;
    fg_b = (float)(GTK_WIFISCAN(widget)->fg.blue)/65535;
        
    if ( GTK_WIFISCAN(widget)->pixmap != NULL )
        g_object_unref( GTK_WIFISCAN(widget)->pixmap );
    GTK_WIFISCAN(widget)->pixmap = 
        gdk_pixmap_new(widget->window,widget->allocation.width,widget->allocation.height,-1);
    pixmap = GTK_WIFISCAN(widget)->pixmap;

    piboxLogger(LOG_TRACE2, "Getting cr\n");
    gdk_drawable_get_size(pixmap, &width, &height);
    cst = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, height);
    cr = cairo_create(cst);

    // Fill with black background with the default color
    cairo_set_source_rgb(cr, 0, 0, 0);
    cairo_rectangle(cr, 0, 0, width, height);
    cairo_fill(cr);

    /*
     * Display set of wifi devices found on this host.
     */

    /* Set offset on x axis */
    offset_x = 20;
    offset_y = 40; 

    /* Set font color */
    cairo_set_source_rgb(cr, 1, 1, 1);
    cairo_select_font_face(cr, "Sans",
            CAIRO_FONT_SLANT_NORMAL,
            CAIRO_FONT_WEIGHT_BOLD);

    cairo_set_font_size(cr, TITLE_FONT_SIZE);
    cairo_text_extents(cr, WIFI_DEVICES_S, &extents);
    cairo_move_to(cr, offset_x, offset_y);
    cairo_show_text(cr, WIFI_DEVICES_S);

    cairo_set_source_rgb(cr, 0.5, 1.0, 0.0);
    offset_y = 70;

    /* Check for wifi devices. */
    if ( GTK_WIFISCAN(widget)->wifi_devices == NULL )
    {
        cairo_set_font_size(cr, DATA_MEDIUM_SIZE);
        cairo_text_extents(cr, NO_WIFI_S, &extents);
        cairo_move_to(cr, offset_x, offset_y);
        cairo_show_text(cr, NO_WIFI_S);
    }
    else
    {
        cairo_set_font_size(cr, DATA_MEDIUM_SIZE);
        memset(str, 0, STR_SIZE);
        g_slist_foreach(GTK_WIFISCAN(widget)->wifi_devices, listWifiDevices, str);
        cairo_text_extents(cr, str, &extents);
        cairo_move_to(cr, offset_x, offset_y);
        cairo_show_text(cr, str);
    }

    /*
     * Display set of wifi channels in use in the local area.
     */

    offset_x = 20;
    offset_y = 110;

    cairo_set_font_size(cr, TITLE_FONT_SIZE);
    cairo_set_source_rgb(cr, 1, 1, 1);
    cairo_text_extents(cr, WIFI_CHANNEL_S, &extents);
    cairo_move_to(cr, offset_x, offset_y);
    cairo_show_text(cr, WIFI_CHANNEL_S);

    /* Set font characteristics. */
    cairo_select_font_face(cr, "Sans",
            CAIRO_FONT_SLANT_NORMAL,
            CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, DATA_MEDIUM_SIZE);
    cairo_set_source_rgb(cr, 0.5, 1.0, 0.0);

    /* Get the list of channels in the neighborhood. */
    offset_y = 140;
    offset_x = 20;
    if ( GTK_WIFISCAN(widget)->channels == NULL )
    {
        cairo_text_extents(cr, NO_CHANNEL_S, &extents);
        cairo_move_to(cr, offset_x, offset_y);
        cairo_show_text(cr, NO_CHANNEL_S);
    }
    else
    {
        piboxLogger(LOG_INFO, "Channel count: %d\n", 
                g_hash_table_size(GTK_WIFISCAN(widget)->channels));
        g_hash_table_iter_init (&iter, GTK_WIFISCAN(widget)->channels);
        cnt = 0;
        while (g_hash_table_iter_next (&iter, &key, &value))
        {
            sprintf(str, "%02d:%2d", key, value);
            cairo_text_extents(cr, str, &extents);
            cairo_move_to(cr, offset_x, offset_y);
            cairo_show_text(cr, str);
            offset_y += 15;
            cnt++;

            if ( (cnt % 3) == 0 )
            {
                offset_y = 140;
                offset_x += 75;
            }
        }
    }

    /*
     * Display BSS data: SSID, channels, and signal strength.
     * Do this in a graph to show which channels are causing 
     * interference..
     */

    /* Reset the offset on the X axis */
    offset_y = 215;
    offset_x = 20;

    cairo_set_source_rgb(cr, 1, 1, 1);
    cairo_set_font_size(cr, TITLE_FONT_SIZE);
    cairo_text_extents(cr, SSID_S, &extents);
    cairo_move_to(cr, offset_x, offset_y);
    cairo_show_text(cr, SSID_S);

    cairo_set_font_size(cr, DATA_SMALL_SIZE);
    offset_y += 35;

    /* Retrieve the BSS List so we can graph that data */
    cairo_set_font_size(cr, DATA_SMALL_SIZE);
    if ( GTK_WIFISCAN(widget)->bss_list == NULL )
    {
        cairo_text_extents(cr, NO_BSS_S, &extents);
        cairo_move_to(cr, offset_x+10, offset_y+10);
        cairo_show_text(cr, NO_BSS_S);
    }
    else
    {
        memset(str, 0, STR_SIZE);
        piboxLogger(LOG_INFO, "BSS list size: %d\n", g_slist_length(GTK_WIFISCAN(widget)->bss_list));

        /*
         * Draw semi-transparent filled arcs for each BSS entry.
         */
        bss_cb_data.cr       = cr;
        bss_cb_data.offset_x = offset_x;
        bss_cb_data.offset_y = offset_y;

        bss_cb_data.span     = (height-10-235) / MAX_2G_TICKS;
        bss_cb_data.origin_x = 15;
        bss_cb_data.origin_y = 235 + bss_cb_data.span;
        bss_cb_data.width    = width-15-10;
        g_slist_foreach(GTK_WIFISCAN(widget)->bss_list, graphBSS, &bss_cb_data);
    }

    /*
     * Create a mask so left half of ellipses are removed.
     */
    cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
    cairo_rectangle(cr, 0, 230, 14, height);
    cairo_set_source_rgb(cr, 0,0,0);
    cairo_fill (cr);

    /* overlay axis for the graphs on the mask */
    cairo_set_source_rgb(cr, 1.0, 1.0, 0.0);
    cairo_set_line_width(cr, 0.5);
    cairo_move_to(cr, 15, 235);
    cairo_line_to(cr, width-10, 235);
    cairo_stroke(cr);

    cairo_move_to(cr, 15, 235);
    cairo_line_to(cr, 15, height-10);
    cairo_stroke(cr);

    /* Ticks and labels for the graphs */
    cairo_set_font_size(cr, BSS_SMALL_SIZE);
    length = (height-10-235) / MAX_2G_TICKS;
    offset = 235+length;
    piboxLogger(LOG_INFO, "tick length: %d\n", length);
    cairo_set_line_width(cr, 1.0);
    ch = 1;
    for(i=0; i<MAX_2G_TICKS; i++)
    {
        cairo_move_to(cr, 15, offset);
        cairo_line_to(cr, 18, offset);
        cairo_stroke(cr);

        if ( ch_ticks[ch-1].center == i )
        {
            sprintf(buf, "%d", ch);
            cairo_text_extents(cr, buf, &extents);
            cairo_move_to(cr, 1, offset+(extents.height/2));
            cairo_show_text(cr, buf);
            ch++;
        }

        offset += length;
    }

    length = (width-15-10) / MAX_DBM_TICKS;
    offset = 15+length;
    dbm = -90;
    piboxLogger(LOG_INFO, "tick span: %d\n", length);
    for(i=0; i<MAX_DBM_TICKS; i++)
    {
        cairo_move_to(cr, offset, 235);
        cairo_line_to(cr, offset, 238);
        cairo_stroke(cr);

        sprintf(buf, "%d dBm", dbm);
        cairo_text_extents(cr, buf, &extents);
        cairo_move_to(cr, offset-(extents.width/2)-5, 230);
        cairo_show_text(cr, buf);
        dbm += 10;

        offset += length;
    }

    /* Don't need the cairo object now */
    cairo_destroy(cr);

    piboxLogger(LOG_INFO, "Paint the pixmap.\n");
    cr_pixmap = gdk_cairo_create(pixmap);
    cairo_set_source_surface (cr_pixmap, cst, 0, 0);
    cairo_paint(cr_pixmap);
    cairo_destroy(cr_pixmap);
    cairo_surface_destroy(cst);

    piboxLogger(LOG_INFO, "Done - Paint the pixmap.\n");
    painting = 0;
}

static void
gtk_wifiscan_destroy(GtkObject *object)
{
    GtkWifiscan *wifiscan;
    GtkWifiscanClass *klass;

    g_return_if_fail(object != NULL);
    g_return_if_fail(GTK_IS_WIFISCAN(object));

    wifiscan = GTK_WIFISCAN(object);
    klass = gtk_type_class(gtk_widget_get_type());
    if (GTK_OBJECT_CLASS(klass)->destroy) {
        (* GTK_OBJECT_CLASS(klass)->destroy) (object);
    }
}

/*
 *========================================================================
 *========================================================================
 * Public API
 *========================================================================
 *========================================================================
 */

void
gtk_wifiscan_update( GtkWifiscan *wifiscan, GSList *devices, GSList *bss, GHashTable *channels )
{
    /*
     * Retrieve data for use in updating the scan widget.
     */
    piboxLogger(LOG_INFO, "Set devices.\n");
    wifiscan->wifi_devices = devices;
    piboxLogger(LOG_INFO, "Set bss.\n");
    wifiscan->bss_list = bss;
    piboxLogger(LOG_INFO, "Set channels.\n");
    wifiscan->channels = channels;
    gtk_wifiscan_paint(GTK_WIDGET(wifiscan));
}
