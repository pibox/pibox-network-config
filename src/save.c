/*******************************************************************************
 * pibox-network-config
 *
 * save.c:  Functions for writing data files.
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define SAVE_C

#define _GNU_SOURCE 
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/types.h>
#include <dirent.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <glib.h>
#include <pibox/utils.h>
#include <pibox/log.h>

#include "cli.h"
#include "callbacks.h"

// Library API
#include "pnc.h"
#include "pncPrivate.h"

/*
 * ========================================================================
 * Type definitions and Data structures 
 * =======================================================================
 */
#define WPA_CTRLINT   "ctrl_interface=/var/run/wpa_supplicant"
#define WPA_APSCAN    "ap_scan=1"

/*
 *========================================================================
 *========================================================================
 * STATIC FUNCTIONS (not part of API)
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   writeHeader
 * Prototype:  void writeHeader( FILE *fd )
 *
 * Description:
 * Write a header to the specified file.
 *========================================================================
 */
void 
writeHeader( FILE *fd )
{
    // write it out
    fprintf(fd, "# Network interfaces configuration file\n");
    fprintf(fd, "# Created by pibox-network-config.\n");
    fprintf(fd, "# --------------------------------------\n");
}

/*
 *========================================================================
 * Name:   writeDNSHeader
 * Prototype:  void writeDNSHeader( FILE *fd )
 *
 * Description:
 * Write a header to the specified file.
 *========================================================================
 */
void 
writeDNSHeader( FILE *fd )
{
    // write it out
    fprintf(fd, "# Nameserver configuration file\n");
    fprintf(fd, "# Created by pibox-network-config.\n");
    fprintf(fd, "# --------------------------------------\n");
}

/*
 *========================================================================
 * Name:   writeWPAHeader
 * Prototype:  void writeWPAHeader( FILE *fd )
 *
 * Description:
 * Write a header to the specified file.
 *========================================================================
 */
void 
writeWPAHeader( FILE *fd )
{
    // write it out
    fprintf(fd, "# Wireless configuration file\n");
    fprintf(fd, "# Created by pibox-network-config.\n");
    fprintf(fd, "# --------------------------------------\n");
}

/*
 *========================================================================
 * Name:   writeLoopback
 * Prototype:  void writeLoopback( FILE *fd )
 *
 * Description:
 * Write the loopback entry to the specified file.
 *========================================================================
 */
void 
writeLoopback( FILE *fd )
{
    char *name = "lo";

    // write it out
    fprintf(fd, "auto %s\n", name);
    fprintf(fd, "iface %s inet loopback\n", name);
}

/*
 *========================================================================
 * Name:   writeWPARequired
 * Prototype:  void writeWPARequired( FILE *fd )
 *
 * Description:
 * Write required data to the specified file.
 *========================================================================
 */
void 
writeWPARequired( FILE *fd )
{
    // write it out
    fprintf(fd, "%s\n", WPA_CTRLINT);
    fprintf(fd, "%s\n", WPA_APSCAN);
}

/*
 *========================================================================
 * Name:   writeInterface
 * Prototype:  void writeInterface(gpointer, gpointer)
 *
 * Description:
 * Write interfaces data to a file.
 *========================================================================
 */
void 
writeInterface( gpointer item, gpointer user_data )
{
    PNC_INTERFACE_T *iface = (PNC_INTERFACE_T *)item;
    FILE *fd = (FILE *)user_data;
    if ( iface->enabled == 0 )
    {
        piboxLogger(LOG_INFO, "writeInterface: Skipping disabled interface: %s\n", iface->name);
        return;
    }

    // write it out
    fprintf(fd, "auto %s\n", iface->name);
    fprintf(fd, "iface %s inet %s\n", iface->name, iface->addressType);

    // Add static settings, if the interface is configured for static.
    if ( strcasecmp(iface->addressType, "static") == 0 )
    {
        if ( iface->address != NULL )
            fprintf(fd, "    address %s\n", iface->address);
        if ( iface->netmask != NULL )
            fprintf(fd, "    netmask %s\n", iface->netmask);
        if ( iface->gateway != NULL )
            fprintf(fd, "    gateway %s\n", iface->gateway);
    }

    fprintf(fd, "\n");
}

/*
 *========================================================================
 * Name:   writeDNS
 * Prototype:  void writeDNS(gpointer, gpointer)
 *
 * Description:
 * Write nameserver data to a file.
 *========================================================================
 */
void 
writeDNS( gpointer item, gpointer user_data )
{
    gchar *address = (gchar *)item;
    FILE *fd = (FILE *)user_data;

    if ( address == NULL )
        return;

    // write it out
    fprintf(fd, "nameserver %s\n", address);
}

/*
 *========================================================================
 *========================================================================
 * PUBLIC API FUNCTIONS 
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   pncSaveInterface
 * Prototype:  void pncSaveInterface( char *filename )
 *
 * Description:
 * Write out the interfaces configuration to the named file.
 *
 * Arguments:
 * char *filename    Name of the file to save to.  See pncGetFilename().
 *========================================================================
 */
void 
pncSaveInterface( char *dest )
{
    FILE *fd;
    GSList *interfaces;
    char *filename;

    pncPrintInterfaces();
    interfaces = pncGetInterfaces();
    if ( interfaces == NULL )
    {
        piboxLogger(LOG_ERROR, "No interfaces found.  That shouldn't happen!\n");
        return;
    }

    if ( getTestMode() )
        filename = g_strconcat( dest, ".bak", NULL );
    else
        filename = g_strdup( dest );

    // Open the named file
    piboxLogger(LOG_INFO, "pncSaveInterface: %s\n", filename);
    fd = fopen(filename, "w");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open interfaces file: %s - %s\n", filename, strerror(errno));
        g_free(filename);
        return;
    }
    g_free(filename);

    // Write a header 
    writeHeader(fd);

    // Write the loopback entry
    writeLoopback(fd);

    fprintf(fd, "\n");

    // Iterate over the interfaces, writing to the specified file.
    g_slist_foreach(interfaces, writeInterface, fd);

    // close the file.
    fclose(fd);
    pncPrintInterfaces();
}

/*
 *========================================================================
 * Name:   pncUpdateInterface
 * Prototype:  void pncUpdateInterface( gchar *name, const gchar *addr, const gchar *addrType, const gchar *mask, const gchar *gateway )
 *
 * Description:
 * Update a network interface configuration based on provided field settings.
 *========================================================================
 */
void 
pncUpdateInterface( gchar *name, const gchar *addr, const gchar *addrType, const gchar *mask, const gchar *gateway )
{
    gchar *value;
    if ( (name != NULL) && (addrType != NULL) )
    {
        value = g_strdup(addrType);
        piboxToLower(value);
        pncSetInterfaceField(name, PNC_ID_IF_ADDRTYPE, value );
        g_free(value);

        if ( strcasecmp(addrType, "static") == 0 )
        {
            value = g_strdup(addr);
            piboxToLower(value);
            pncSetInterfaceField(name, PNC_ID_IF_ADDR, value );
            g_free(value);

            value = g_strdup(mask);
            piboxToLower(value);
            pncSetInterfaceField(name, PNC_ID_IF_MASK, value );
            g_free(value);

            value = g_strdup(gateway);
            piboxToLower(value);
            pncSetInterfaceField(name, PNC_ID_IF_GATEWAY, value );
            g_free(value);
        }
    }
}

/*
 *========================================================================
 * Name:   pncSaveDNS
 * Prototype:  void pncSaveDNS( char *filename )
 *
 * Description:
 * Write out the DNS configuration to the named file.
 *
 * Arguments:
 * char *filename    Name of the file to save to.  See pncGetFilename().
 *========================================================================
 */
void 
pncSaveDNS( char *dest )
{
    FILE *fd;
    GSList *nameservers;
    char *filename;

    nameservers = pncGetNameservers();
    if ( nameservers == NULL )
        return;

    if ( getTestMode() )
        filename = g_strconcat( dest, ".bak", NULL );
    else
        filename = g_strdup( dest );

    // Open the named file
    piboxLogger(LOG_INFO, "pncSaveDNS: %s\n", filename);
    fd = fopen(filename, "w");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open nameservers file: %s - %s\n", filename, strerror(errno));
        g_free(filename);
        return;
    }
    g_free(filename);

    // Write a header 
    writeDNSHeader(fd);

    // Iterate over the interfaces, writing to the specified file.
    g_slist_foreach(nameservers, writeDNS, fd);

    // close the file.
    fclose(fd);
}

/*
 *========================================================================
 * Name:   pncRemoveDNS
 * Prototype:  void pncRemoveDNS( char *filename )
 *
 * Description:
 * Remove the DNS configuration file.
 *
 * Arguments:
 * char *filename    Name of the file to remove.  See pncGetFilename().
 *========================================================================
 */
void 
pncRemoveDNS( char *dest )
{
    char *filename;

    if ( getTestMode() )
        filename = g_strconcat( dest, ".bak", NULL );
    else
        filename = g_strdup( dest );

    unlink(dest);
    g_free(filename);
}

/*
 *========================================================================
 * Name:   pncSaveWPA
 * Prototype:  void pncSaveWPA( char *filename )
 *
 * Description:
 * Write out the WPA configuration to the named file.
 *
 * Arguments:
 * char *filename    Name of the file to save to.  See pncGetFilename().
 *========================================================================
 */
void 
pncSaveWPA( char *dest )
{
    FILE   *fd;
    GSList *wpa;
    GSList *item;
    int    idx;
    char   *filename;

    const gchar  *field;

    wpa = pncGetWPA();
    if ( wpa == NULL )
    {
        piboxLogger(LOG_ERROR, "No wireless configuration found.  That shouldn't happen!\n");
        return;
    }

    // Make sure the SSID an password are set.
    item = g_slist_nth(wpa, PNC_ID_WPA_SSID);
    if ( (item == NULL) || (item->data==NULL) || (strlen(item->data)==0) )
    {
        piboxLogger(LOG_ERROR, "Missing SSID.  WPA configuration will not be written to file.\n");
        return;
    }
    item = g_slist_nth(wpa, PNC_ID_WPA_PSK);
    if ( (item == NULL) || (item->data==NULL) || (strlen(item->data)==0) )
    {
        piboxLogger(LOG_ERROR, "Missing password.  WPA configuration will not be written to file.\n");
        return;
    }

    if ( getTestMode() )
        filename = g_strconcat( dest, ".bak", NULL );
    else
        filename = g_strdup( dest );

    // Open the named file
    piboxLogger(LOG_INFO, "pncSaveWPA: %s\n", filename);
    fd = fopen(filename, "w");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open wireless configuration file: %s - %s\n", filename, strerror(errno));
        g_free(filename);
        return;
    }
    g_free(filename);

    // Write a header 
    writeWPAHeader(fd);

    // Write the required data
    writeWPARequired(fd);

    fprintf(fd, "\n");

    // Write the WPA fields in order
    fprintf(fd, "network={\n");

    for(idx=0; idx<PNC_ID_WPA_MAX; idx++)
    {
        field = g_slist_nth_data(wpa, idx);
        if ( field == NULL )
            continue;
        switch(idx)
        {
            case PNC_ID_WPA_SSID:
            case PNC_ID_WPA_PSK:
                fprintf(fd, "    %s=\"%s\"\n", getWPAOption(idx), field);
                break;
            case PNC_ID_WPA_PROTO:
                fprintf(fd, "    %s=WPA RSN\n", getWPAOption(idx));
                break;
            case PNC_ID_WPA_KEYMGMT:
                if ( strcasestr((char *)field, "Personal") )
                    fprintf(fd, "    %s=WPA-PSK\n", getWPAOption(idx));
                else if ( strcasestr((char *)field, "Enterprise") )
                    fprintf(fd, "    %s=WPA-EAP\n", getWPAOption(idx));
                break;
            default:
                fprintf(fd, "    %s=%s\n", getWPAOption(idx), field);
                break;
        }
    }

    fprintf(fd, "}\n");

    // close the file.
    fclose(fd);
}

/*
 *========================================================================
 * Name:   pncSaveHostAP
 * Prototype:  void pncSaveHostAP( char *filename )
 *
 * Description:
 * Write out the HostAP configuration to the named file.
 *
 * Arguments:
 * char *filename    Name of the file to save to.  See pncGetFilename().
 *========================================================================
 */
void 
pncSaveHostAP( char *dest )
{
    FILE   *fd;
    GSList *hostap;
    GSList *item;
    int    idx;
    gchar  *passwordFile;
    GSList *interfaces;
    char   buf[16];
    char   *bufptr, *ptr;
    char   *netfilename;
    char   *filename;
    char   *ifname;

    PNC_INTERFACE_T *iface;
    const gchar  *field;

    hostap = pncGetHostAP();
    if ( hostap == NULL )
    {
        piboxLogger(LOG_ERROR, "No wireless configuration found.  That shouldn't happen!\n");
        return;
    }

    // Make sure the SSID an password are set.
    item = g_slist_nth(hostap, PNC_ID_HAP_SSID);
    if ( (item == NULL) || (item->data==NULL) || (strlen(item->data)==0) )
    {
        piboxLogger(LOG_ERROR, "Missing SSID.  HostAPD configuration will not be written to file.\n");
        return;
    }
    piboxLogger(LOG_INFO, "SSID is not NULL: %s\n", item->data);
    item = g_slist_nth(hostap, PNC_ID_HAP_WPA_PSK_FILE);
    if ( (item == NULL) || (item->data==NULL) || (strlen(item->data)==0) )
    {
        piboxLogger(LOG_ERROR, "Missing password file setting.  HostAPD password will not be written to file.\n");
        return;
    }
    passwordFile = item->data;
    piboxLogger(LOG_INFO, "passwordFile is not NULL: %s\n", item->data);

    // Make sure the base network address is set.
    item = g_slist_nth(hostap, PNC_ID_HAP_NET);
    if ( (item == NULL) || (item->data==NULL) || (strlen(item->data)==0) )
    {
        piboxLogger(LOG_ERROR, "Missing base network address.  HostAPD configuration will not be written to file.\n");
        return;
    }
    piboxLogger(LOG_INFO, "base address is not NULL: %s\n", item->data);

    if ( getTestMode() )
        filename = g_strconcat( dest, ".bak", NULL );
    else
        filename = g_strdup( dest );

    // Open the configuration file
    piboxLogger(LOG_INFO, "pncSaveHostAP: %s\n", filename);
    fd = fopen(filename, "w");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open HostAP configuration file: %s - %s\n", filename, strerror(errno));
        g_free(filename);
        return;
    }
    g_free(filename);

    // Write the HostAP fields in order
    for(idx=0; idx<PNC_ID_HOSTAP_MAX; idx++)
    {
        if ( ( idx == PNC_ID_HAP_PW ) || ( idx == PNC_ID_HAP_NET ) )
        {
            piboxLogger(LOG_INFO, "Skipping idx %d\n", idx);
            continue;
        }
        field = g_slist_nth_data(hostap, idx);
        if ( field == NULL )
        {
            piboxLogger(LOG_INFO, "Field is null %d\n", idx);
            continue;
        }
        piboxLogger(LOG_INFO, "%s=%s\n", getHostAPOption(idx), field);
        fprintf(fd, "%s=%s\n", getHostAPOption(idx), field);
    }

    // close the file.
    fclose(fd);

    field = g_slist_nth_data(hostap, PNC_ID_HAP_PW);
    if ( field != NULL )
    {
        // Open the password file
        piboxLogger(LOG_INFO, "pncSaveHostAP PW file: %s\n", passwordFile);
        fd = fopen(passwordFile, "w");
        if ( fd == NULL )
        {
            piboxLogger(LOG_ERROR, "Failed to open HostAP password file: %s - %s\n", passwordFile, strerror(errno));
            return;
        }

        fprintf(fd, "00:00:00:00:00:00 %s\n", field);

        // close the file.
        fclose(fd);
    }

    // Retrieve the interface name to use, or use the default.
    item = g_slist_nth(hostap, PNC_ID_HAP_INTERFACE);
    if ( (item != NULL) && (item->data!=NULL) && (strlen(item->data)!=0) )
        ifname = item->data;
    else
        ifname = "wlan0";
    piboxLogger(LOG_INFO, "interface: %s\n", ifname);

    // Find, then save, the interface entries.
    interfaces = pncGetInterfaces();
    if (interfaces == NULL )
    {
        piboxLogger(LOG_ERROR, "pncSaveHostAP: No interfaces found.  That shouldn't happen.\n");
        return;
    }
    item = g_slist_find_custom(interfaces, ifname, (GCompareFunc)pncFindInterface);
    if ( item == NULL )
    {
        piboxLogger(LOG_ERROR, 
			"pncSaveHostAP: Can't find interface: %s. Creating new interface.\n", ifname);
        pncNewInterface( ifname );
        pncSetInterfaceField( ifname, PNC_ID_IF_NETTYPE, "inet" );
        pncSetInterfaceField( ifname, PNC_ID_IF_ADDRTYPE, "static" );
        pncSetInterfaceState( ifname, 1 );
    	item = g_slist_find_custom(interfaces, ifname, (GCompareFunc)pncFindInterface);
    }
    iface = (PNC_INTERFACE_T *)item->data;
    if ( iface == NULL )
    {
        piboxLogger(LOG_ERROR, "pncSaveHostAP: missing device configuration for %s\n", ifname);
        return;
    }

    // Copy the first three octets of the address into a buffer.
    // The caller validated the address so there is no error checking its format here.
    // The last octet is always "1".
    item = g_slist_nth(hostap, PNC_ID_HAP_NET);
    memset(buf, 0, 16);
    for ( bufptr=buf, idx=0, ptr=item->data; (*ptr != '\0') && (idx < 3); )
    {
        if ( *ptr == '.' )
            idx++;
        memcpy(bufptr++, ptr++, 1);
    }
    strcat(bufptr, "1");
    piboxLogger(LOG_INFO, "pncSaveHostAP: %s IP will be %s\n", ifname, buf);
    
    // Update as static IP based on base network address
	if ( iface->addressType != NULL )
		free(iface->addressType);
    iface->addressType = g_strdup("static");
	if ( iface->address != NULL )
		free(iface->address);
    iface->address = g_strdup(buf);
	if ( iface->netmask != NULL )
		free(iface->netmask);
    iface->netmask = g_strdup("255.255.255.0");
	if ( iface->gateway != NULL )
		free(iface->gateway);
    iface->gateway = NULL;

    // Restore interface entries.
    netfilename = pncGetFilename( PNC_ID_INTERFACES );
    pncSaveInterface(netfilename);
    g_free(netfilename);
}

/*
 *========================================================================
 * Name:   pncSaveNetType
 * Prototype:  void pncSaveNetType( char *filename, int type )
 *
 * Arguments:
 * char *filename   The name of the file to write to.
 * int type         One of: N_NONE, N_WIRELESS, N_WAP
 *
 * Description:
 * Write out the NetType that says whether wireless client or hostAP is being used.
 *========================================================================
 */
void 
pncSaveNetType( char *dest, int type )
{
    FILE   *fd;
    GSList *hostap;
    GSList *item;
    int    idx;
    gchar  *passwordFile;
    char   *filename;

    const gchar  *field;

    if ( getTestMode() )
        filename = g_strconcat( dest, ".bak", NULL );
    else
        filename = g_strdup( dest );

    // Open the configuration file
    piboxLogger(LOG_INFO, "pncSaveNetType: %s\n", dest);
    fd = fopen(filename, "w");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open NetType file: %s - %s\n", filename, strerror(errno));
        g_free(filename);
        return;
    }
    g_free(filename);

    switch(type)
    {
        case PNC_ID_NONE    : fprintf(fd, "none\n"); break;
        case PNC_ID_WIRELESS: fprintf(fd, "client\n"); break;
        case PNC_ID_WAP     : fprintf(fd, "wap\n"); break;
    }

    // close the file.
    fclose(fd);
}
