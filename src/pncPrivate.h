/*******************************************************************************
 * pibox-network-config
 *
 * pncLibNetPrivate.h:  Non public components of Library for managing network files.
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef PNCLIBNETPRIVATE_H
#define PNCLIBNETPRIVATE_H

/*
 * ========================================================================
 * Type definitions and Data structures 
 * =======================================================================
 */

typedef struct __pnc_interface {
    char    *name;          // Device name, from /sys/class/net
    char    *networkType;   // inet or inet6 (we only support inet currently)
    char    *macAddress;    // MAC address
    char    *addressType;   // DHCP or static
    char    *address;       // static IP address
    char    *netmask;       // static netmask
    char    *gateway;       // static gateway
    int     enabled;        // 0: No, 1: Yes
} PNC_INTERFACE_T;

/*
 * ========================================================================
 * Prototypes
 * =======================================================================
 */
#ifndef LOAD_C
extern gchar *getWPAOption( gint );
extern gchar *getHostAPOption( gint idx );
extern gint  getTestMode( void );
#endif // LOAD_C

#ifndef MSGDIALOG_C
extern gint chooseDialog( char *msg, char *title, char *btn1, char *btn2 );
extern void errorDialog( char *msg );
#endif // MSGDIALOG_C

#endif
