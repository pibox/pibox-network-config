/*******************************************************************************
 * pibox-network-config
 *
 * wifiscan.c:  Manage background wifi scans
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define WIFISCAN_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <glib.h>
#include <pibox/log.h>

#include "pnc.h"
#include "callbacks.h"

/* Module prototypes */
void genWifiList( void );
void genBSSList( void );
void genChannelList( void );

static int mgrIsRunning = 0;
static int mgrThreadIsRunning = 0;
pthread_mutex_t mgrProcessorMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t mgrProcessorThread;

/* This really belongs somewhere else but awaits a rewrite of interface.c */
#define PROG "pibox-network-config"

static GHashTable *channels;

/*
 ******************************************************************************
 ******************************************************************************
 *
 * Thread functions
 *
 ******************************************************************************
 ******************************************************************************
 */

/*========================================================================
 * Name:   copyBSSList
 * Prototype:  gpointer copyBSSList( gconstpointer src, gpointer data )
 *
 * Description:
 * Copy an element of the BSS list.
 *========================================================================*/
static gpointer
copyBSSList( gconstpointer src, gpointer data )
{
    BSS_T *entry = (BSS_T *)src;
    BSS_T *new_entry;

    new_entry = (BSS_T *)calloc(1,sizeof(BSS_T));
    new_entry->channel = entry->channel;
    new_entry->signal  = entry->signal;
    new_entry->ssid    =  strdup(entry->ssid);
    return new_entry;
}

/*========================================================================
 * Name:   copyWifiDevices
 * Prototype:  gpointer copyWifiDevices( gconstpointer src, gpointer data )
 *
 * Description:
 * Copy an element of the wifi devices list.
 *========================================================================*/
static gpointer
copyWifiDevices( gconstpointer src, gpointer data )
{
    return strdup(src);
}

/*========================================================================
 * Name:   isMgrProcessorRunning
 * Prototype:  int isMgrProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of mgrIsRunning variable.
 *========================================================================*/
static int
isMgrProcessorRunning( void )
{
    int status;
    pthread_mutex_lock( &mgrProcessorMutex );
    status = mgrIsRunning;
    pthread_mutex_unlock( &mgrProcessorMutex );
    return status;
}

/*========================================================================
 * Name:   isMgrTheadProcessorRunning
 * Prototype:  int isMgrTheadProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of mgrIsRunning variable.
 *========================================================================*/
int
isMgrThreadRunning( void )
{
    int status;
    pthread_mutex_lock( &mgrProcessorMutex );
    status = mgrThreadIsRunning;
    pthread_mutex_unlock( &mgrProcessorMutex );
    return status;
}

/*========================================================================
 * Name:   setMgrProcessorRunning
 * Prototype:  int setMgrProcessorRunning( void )
 *
 * Description:
 * Thread-safe set of mgrIsRunning variable.
 *========================================================================*/
static void
setMgrProcessorRunning( int val )
{
    pthread_mutex_lock( &mgrProcessorMutex );
    mgrIsRunning = val;
    pthread_mutex_unlock( &mgrProcessorMutex );
}

/*========================================================================
 * Name:   setMgrThreadRunning
 * Prototype:  int setMgrThreadRunning( void )
 *
 * Description:
 * Thread-safe set of mgrTheadIsRunning variable.
 *========================================================================*/
static void
setMgrThreadRunning( int val )
{
    pthread_mutex_lock( &mgrProcessorMutex );
    mgrThreadIsRunning = val;
    pthread_mutex_unlock( &mgrProcessorMutex );
}

/*========================================================================
 * Name:   mgrProcessor
 * Prototype:  void mgrProcessor( void * )
 *
 * Description:
 * Peform and retrieve wifi scan data.
 *
 * Input Arguments:
 * void *arg    Not used, but required for threads.
 *========================================================================*/
static void *
mgrProcessor( void *arg )
{
    int cnt = 0;
    int doDraw = 0;

    setMgrProcessorRunning(1);
    setMgrThreadRunning(1);
    while ( isMgrProcessorRunning() )
    {
        /* 
        * Scan wifi channels
        */
        if ( cnt == 60 )
        {
            piboxLogger(LOG_INFO, "Generating channel list.\n");
            genChannelList();
            piboxLogger(LOG_INFO, "Generating Wifi device list.\n");
            genWifiList();
            cnt = 0;
        }
        else if ( cnt == 20 )
        {
            piboxLogger(LOG_INFO, "Generating BSS list.\n");
            genBSSList();
        }
        usleep(100000);
        cnt++;
    }

    piboxLogger(LOG_INFO, "Mgr thread is exiting.\n");
    setMgrThreadRunning(0);
    return(0);
}

/*========================================================================
 * Name:   startMgr
 * Prototype:  void startMgr( void )
 *
 * Description:
 * Setup thread to handle inbound messages.
 *
 * Notes:
 * Threads run until configs->serverEnabled = 0.  See shutdownMgrProcessor().
 *========================================================================*/
void
startMgr( void )
{
    int rc;

    // Prevent running two at once.
    if ( isMgrProcessorRunning() )
        return;

    /* Create a thread to expire streams. */
    rc = pthread_create(&mgrProcessorThread, NULL, &mgrProcessor, (void *)NULL);
    if (rc)
    {
        piboxLogger(LOG_ERROR, "%s: Failed to create mgr thread: %s\n", PROG, strerror(rc));
        exit(-1);
    }
    piboxLogger(LOG_INFO, "%s: Started mgr thread.\n", PROG);
    return;
}

/*========================================================================
 * Name:   shutdownMgrProcessor
 * Prototype:  void shutdownMgrProcessor( void )
 *
 * Description:
 * Shut down message processing thread.
 *========================================================================*/
void
shutdownMgr( void )
{
    int timeOut = 0;
    int isRunning = -1;

    setMgrProcessorRunning(0);
    if ( isMgrThreadRunning() )
    {
        while ( isMgrThreadRunning() )
        {
            sleep(1);
            timeOut++;
            if (timeOut == 5)
            {
                piboxLogger(LOG_ERROR, "Timed out waiting on mgr thread to shut down.\n");
                pthread_cancel(mgrProcessorThread);
                piboxLogger(LOG_INFO, "mgrProcessor forced shut down.\n");
                return;
            }
        }
        pthread_detach(mgrProcessorThread);
    }
    piboxLogger(LOG_INFO, "mgrProcessor shut down.\n");
}

/*========================================================================
 * Name:   genWifiList
 * Prototype:  void genWifiList( void )
 *
 * Description:
 * Thread-safe generation of the wifi device list.
 *========================================================================*/
void
genWifiList( void )
{
    if ( pthread_mutex_trylock( &mgrProcessorMutex ) != 0 )
        return;
    pncGenWifiList();
    pthread_mutex_unlock( &mgrProcessorMutex );
}

/*========================================================================
 * Name:   getWifiList
 * Prototype:  void getWifiList( void )
 *
 * Description:
 * Retrieve a copy of the current set of wifi devices available on the host.
 * Caller is responsible for clearing the list with g_slist_free_full().
 * This is the thread safe retrieval of the list.
 *
 * Returns:
 * A copy of the Wifi devices list or NULL if the lock cannot be acquired.
 *========================================================================*/
GSList *
getWifiList( void )
{
    GSList *devices = NULL;
    if ( pthread_mutex_trylock( &mgrProcessorMutex ) != 0 )
        return NULL;
    if ( pncGetWifiList() != NULL )
        devices = g_slist_copy_deep( pncGetWifiList(), copyWifiDevices, NULL );
    pthread_mutex_unlock( &mgrProcessorMutex );
    return devices;
}

/*========================================================================
 * Name:   freeWifiDevices
 * Prototype:  void freeWifiDevices( void )
 *
 * Description:
 * Frees up the storage allocated for the deep copy of wifi devices list.
 *========================================================================*/
void
freeWifiDevices( gpointer data )
{
    g_free(data);
}

/*========================================================================
 * Name:   genChannelList
 * Prototype:  void genChannelList( void )
 *
 * Description:
 * Thread-safe generation of the wifi channel list.
 *========================================================================*/
void
genChannelList( void )
{
    if ( pthread_mutex_trylock( &mgrProcessorMutex ) != 0 )
        return;
    if ( channels != NULL )
        g_hash_table_destroy(channels);
    channels = pncGetChannelList();
    pthread_mutex_unlock( &mgrProcessorMutex );
}

/*========================================================================
 * Name:   getChannelList
 * Prototype:  void getChannelList( void )
 *
 * Description:
 * Retrieve a copy of the current set of wifi channels in use by local networks.
 * Caller is responsible for clearing the list with g_hash_table_destroy().
 * This is the thread safe retrieval of the table.
 *
 * Returns:
 * A copy of the channel list or NULL if the lock cannot be acquired.
 *========================================================================*/
GHashTable *
getChannelList( void )
{
    GHashTable      *hash_copy;
    GHashTableIter  iter;
    gpointer        key, value;

    /* Do nothing if we have nothing. */
    if ( channels == NULL )
        return NULL;

    if ( pthread_mutex_trylock( &mgrProcessorMutex ) != 0 )
        return NULL;
    hash_copy = g_hash_table_new(NULL, NULL);
    g_hash_table_iter_init (&iter, channels);
    while (g_hash_table_iter_next (&iter, &key, &value))
    {
        g_hash_table_insert(hash_copy, GINT_TO_POINTER(key), GINT_TO_POINTER(value));
    }
    pthread_mutex_unlock( &mgrProcessorMutex );
    return hash_copy;
}

/*========================================================================
 * Name:   genBSSList
 * Prototype:  void genBSSList( void )
 *
 * Description:
 * Thread-safe generation of the BSS list.
 *========================================================================*/
void
genBSSList( void )
{
    if ( pthread_mutex_trylock( &mgrProcessorMutex ) != 0 )
        return;
    pncGenBSSList();
    pthread_mutex_unlock( &mgrProcessorMutex );
}

/*========================================================================
 * Name:   getBSSList
 * Prototype:  void getBSSList( void )
 *
 * Description:
 * Retrieve a copy of the current set of BSS entries.
 * Caller is responsible for clearing the list with g_slist_free_full().
 * This is the thread safe retrieval of the list.
 *
 * Returns:
 * A copy of the BSS entries list or NULL if the lock cannot be acquired.
 *========================================================================*/
GSList *
getBSSList( void )
{
    GSList *bss;
    if ( pthread_mutex_trylock( &mgrProcessorMutex ) != 0 )
        return NULL;
    bss = g_slist_copy_deep( pncGetBSSList(), copyBSSList, NULL );
    pthread_mutex_unlock( &mgrProcessorMutex );
    return bss;
}

/*========================================================================
 * Name:   freeBSSList
 * Prototype:  void freeBSSList( void )
 *
 * Description:
 * Frees up the storage allocated for the deep copy.
 *========================================================================*/
void
freeBSSList( gpointer data )
{
    g_free(data);
}

