#include <gtk/gtk.h>

const gchar *getAddress(void);
const gchar *getNetmask(void);
const gchar *getGateway(void);
const gchar *getDNS(gint idx);
const gchar *getSSID(void);
const gchar *getPW(void);
gchar *getAddressType(void);
gchar *getInterface(void);
void  setMapped(gint state);
void  setNetType (gint type);
void  netTypeSetup();
void  updateWPA();
int   updateHostAP();
guint getStatusBarContextID();
GtkWidget *getStatusBar();
void showIPInStatusBar( void );

void
on_addressTypeComboBox_changed         (GtkComboBox     *combobox,
                                        gpointer         user_data);

void
on_interfaceComboBox_changed           (GtkComboBox     *combobox,
                                        gpointer         user_data);

void
on_interfaceComboBox_realize           (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_combobox3_changed                   (GtkComboBox     *combobox,
                                        gpointer         user_data);

void
on_button2_clicked                     (GtkButton       *button,
                                        gpointer         user_data);

void
on_button1_clicked                     (GtkButton       *button,
                                        gpointer         user_data);

void
on_open1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_save1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_save_as1_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_quit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_about1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_addressTypeComboBox_realize         (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_combobox3_realize                   (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_showPasswordButton_clicked          (GtkButton       *button,
                                        gpointer         user_data);

void
on_about1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_aboutdialog1_response               (GtkDialog       *dialog,
                                        gint             response_id,
                                        gpointer         user_data);

void
on_passwordTextEntry_realize           (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_notebook2_realize                   (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_ipAddressText_realize               (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_netmaskText_realize                 (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_gatewayText_realize                 (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_dns1Text_realize                    (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_dns2_realize                        (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_dns3Text_realize                    (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_addressTypeComboBox_changed         (GtkComboBox     *combobox,
                                        gpointer         user_data);

void
on_notebook1_realize                   (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_entry11_realize                     (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_interfaceComboBox_updated           (GtkComboBox     *combobox,
                                        gpointer         user_data);

void
on_restart1_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_radiobutton1_clicked                (GtkButton       *button,
                                        gpointer         user_data);

void
on_radiobutton1_realize                (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_radiobutton2_realize                (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_accessPointSSIDEntry_realize        (GtkWidget       *widget,
                                        gpointer         user_data);
void
on_hostapNetEntry_realize              (GtkWidget       *widget,
                                        gpointer         user_data);
void
on_accessPointChannel_realize          (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_accessPointChannel_changed          (GtkComboBox     *combobox,
                                        gpointer         user_data);

void
on_accessPointPasswordEntry_realize    (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_accessPointShowPasswordButton_clicked
                                        (GtkButton       *button,
                                        gpointer         user_data);

void
on_vbox3_realize                       (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_vbox5_realize                       (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_wirelessButton_clicked              (GtkButton       *button,
                                        gpointer         user_data);

void
on_wirelessButton_realize              (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_accessPointButton_realize           (GtkWidget       *widget,
                                        gpointer         user_data);

gboolean
doShutdown                             (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_statusbar1_realize                  (GtkWidget       *widget,
                                        gpointer         user_data);

gboolean 
key_press                               (GtkWidget *widget,
                                         GdkEventKey *event, 
                                         gpointer user_data);

void
on_macAddrText_realize                 (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_ipAddrText_realize                  (GtkWidget       *widget,
                                        gpointer         user_data);

