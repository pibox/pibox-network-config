/*******************************************************************************
 * pibox-network-config
 *
 * msgDialog.c:  Functions for working with a message dialog.
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define MSGDIALOG_H

/*
 * ========================================================================
 * Type definitions and Data structures 
 * =======================================================================
 */

/*
 * ========================================================================
 * Prototypes
 * =======================================================================
 */
#ifndef MSGDIALOG_C
extern void errorDialog( char *msg );
extern void infoDialog( char *msg );
#endif

