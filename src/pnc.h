/*******************************************************************************
 * pibox-network-config
 *
 * pnc.h:  Library for reading/writing network configuration files
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef PNC_H
#define PNC_H

#include <glib.h>

/*
 * ========================================================================
 * Type definitions and Data structures 
 * =======================================================================
 */

// File identifiers
#define PNC_ID_INTERFACES       1
#define PNC_ID_WPASUPPLICANT    2
#define PNC_ID_RESOLV           3
#define PNC_ID_HOSTAPDCONF      4
#define PNC_ID_HOSTAPDPSK       5
#define PNC_ID_NETTYPE          6
#define PNC_ID_DHCP             7
#define PNC_ID_KEYSYMS          8
#define PNC_ID_NETIFACES        9

// Interface Fields
#define PNC_ID_IF_NAME          1
#define PNC_ID_IF_NETTYPE       2
#define PNC_ID_IF_MAC           3
#define PNC_ID_IF_ADDRTYPE      4
#define PNC_ID_IF_ADDR          5
#define PNC_ID_IF_MASK          6
#define PNC_ID_IF_GATEWAY       7

// WPA_SUPPLICANT Fields
#define PNC_ID_WPA_SSID         0
#define PNC_ID_WPA_SCANSSID     1
#define PNC_ID_WPA_PROTO        2
#define PNC_ID_WPA_KEYMGMT      3
#define PNC_ID_WPA_PSK          4
#define PNC_ID_WPA_PAIRWISE     5
#define PNC_ID_WPA_GROUP        6
#define PNC_ID_WPA_MAX          PNC_ID_WPA_GROUP+1

// hostapd.conf Fields
#define PNC_ID_HAP_INTERFACE              0
#define PNC_ID_HAP_DRIVER                 1
#define PNC_ID_HAP_SSID                   2
#define PNC_ID_HAP_CHANNEL                3
#define PNC_ID_HAP_AUTH_ALGS              4
#define PNC_ID_HAP_WPA                    5
#define PNC_ID_HAP_WPA_PSK_FILE           6
#define PNC_ID_HAP_WPA_KEY_MGMT           7
#define PNC_ID_HAP_RSN_PAIRWISE           8
#define PNC_ID_HAP_WPA_PAIRWISE           9
#define PNC_ID_HAP_MACADDR_ACL            10
#define PNC_ID_HAP_IGNORE_BROADCAST_SSID  11
#define PNC_ID_HAP_HW_MODE                12
#define PNC_ID_HAP_PW                     13
#define PNC_ID_HAP_NET                    14
#define PNC_ID_HOSTAP_MAX                 PNC_ID_HAP_NET+1

// Network types
#define PNC_ID_NONE     1
#define PNC_ID_WIRELESS 2
#define PNC_ID_WAP      3

typedef struct _bss_t {
    char    *ssid;
    int     signal;
    int     channel;
    // int     percent;
} BSS_T;

/*
 * ========================================================================
 * Prototypes
 * =======================================================================
 */
#ifndef PNC_C
extern void    pncInit( int, gchar * );
extern gchar  *pncGetFilename( gint );
extern gint    pncSetFilename( gint, gchar * );
extern void    pncPrintFilename( gint );
extern void    pncPrintFilenames( void );
extern void    pncPrintInterfaces( void );
extern gchar  *pncGetInterfaceField( gchar *, int );
extern void    pncSetInterfaceField( gchar *, int, const gchar * );
extern gint    pncGetInterfaceState( gchar * );
extern void    pncSetInterfaceState( gchar *, int );
extern gchar  *pncGetWPAField( gint );
extern void    pncSetWPAField( int , gchar * );
extern gchar  *pncGetHostAPField( int );
extern void    pncSetHostAPField( int , gchar * );
extern void         pncGenWifiList( void );
extern GSList      *pncGetWifiList( void );
extern GHashTable  *pncGetChannelList( void );
extern void         pncGenBSSList( void );
extern GSList      *pncGetBSSList( void );
#endif

#ifndef LOAD_C
// /etc/network/interfaces
extern void    pncLoadInterfaces( void );
extern void    pncClearInterfaces( void );
extern void    pncNewInterface( char * );
extern void    pncRemoveInterface( char * );
extern gint    pncFindInterface( gconstpointer item, gconstpointer user_data );
extern char   *pncGetMacAddress( char * );
extern GSList *pncGetInterfaces( void );
extern gchar  *pncGetInterface0( void );

// Physical interfaces
extern void    pncLoadNet( void );
extern void    pncClearNetInterfaces( void );
extern GSList *pncGetNetInterfaces( void );
extern gint    pncNetInterfaceValid( gchar * );

// DNS (etc/resolv.conf)
extern void    pncLoadResolv( void );
extern void    pncClearResolv( void );
extern void    pncSetResolvField( gchar *dns0, gchar *dns1, gchar *dns2 );
extern GSList *pncGetNameservers( void );
extern gchar  *pncGetNameserver( gint );

// WPA Supplicant
extern void    pncLoadWPA( void );
extern void    pncClearWPA( void );
extern GSList *pncGetWPA( void );

// hostapd
extern void    pncLoadHostAP( void );
extern void    pncClearHostAP( void );
extern GSList *pncGetHostAP( void );
extern gchar  *pncGetHostAPPassword( gchar * );
extern gint    pncReadNetType( char * );

// PiBox key mappings
extern void    pncLoadKeysyms( void );
extern void    pncClearKeysyms( void );
extern gchar  *pncGetHomeKey( void );

#endif // LOAD_C

#ifndef SAVE_C
extern void pncSaveInterface( char *filename );
extern void pncUpdateInterface( gchar *, const gchar *, const gchar *, const gchar *, const gchar *);
extern void pncSaveDNS( char *filename );
extern void pncRemoveDNS( char *dest );
extern void pncSaveHostAP( char *filename );
extern void pncSaveNetType( char *filename, gint type );
extern void pncSaveWPA( char *dest );
#endif // SAVE_C

#endif // PNC_H
