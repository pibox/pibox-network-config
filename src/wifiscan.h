/*******************************************************************************
 * pibox-network-config
 *
 * wifiscan.h:  Manage background wifi scans
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef WIFISCAN_H
#define WIFISCAN_H

/*========================================================================
 * Defined values
 *=======================================================================*/

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef WIFISCAN_C
extern pthread_mutex_t mgrProcessorMutex;

extern int          isMgrThreadRunning( void );
extern void         startMgr( void );
extern void         shutdownMgr( void );
extern GSList       *getWifiList( void );
extern GHashTable   *getChannelList( void );
extern void         freeWifiDevices( gpointer data );
extern GSList       *getBSSList( void );
extern void         freeBSSList( gpointer data );
#endif /* !WIFISCAN_C */
#endif /* !WIFISCAN_H */
