/*******************************************************************************
 * pibox-network-config
 *
 * cli.c:  Command line parsing
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define CLI_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
/*
#include <sys/time.h>
#include <sys/resource.h>
#include <sched.h>
#include <sys/mman.h>
#include <errno.h>
#include <signal.h>
*/
#include <pthread.h>

#include "cli.h"

static pthread_mutex_t cliOptionsMutex = PTHREAD_MUTEX_INITIALIZER;

/*========================================================================
 * Name:   parseArgs
 * Prototype:  void parseArgs( int, char ** )
 *
 * Description:
 * Parse the command line
 *
 * Input Arguments:
 * int argc         Number of command line arguments
 * char **argv     Command line arguments to parse
 *========================================================================*/
void
parseArgs(int argc, char **argv)
{
    int opt;

    memset(&cliOptions, 0, sizeof(CLI_T));

    /* Suppress error messages from getopt_long() for unrecognized options. */
    opterr = 0;

    /* Parse the command line. */
    while ( (opt = getopt(argc, argv, CLIARGS)) != -1 )
    {
        switch (opt)
        {
            /* Enable logging to local file. */
            case 'l':
                cliOptions.flags |= CLI_LOGTOFILE;
                cliOptions.logFile = optarg;
                break;

            /* Enable test mode. */
            case 'T':
                cliOptions.flags |= CLI_TEST;
                break;

            /* -v: Set verbosity level */
            case 'v':
                cliOptions.verbose = atoi(optarg);
                break;

            /* Enable embedded mode. */
            case 'e':
                cliOptions.flags |= CLI_EMBEDDED;
                break;

            default:
                printf("%s \nVersion: %s - %s\n", PROGNAME, VERSTR, VERDATE);
                printf(USAGE);
                exit(0);
                break;
        }
    }
}

/*========================================================================
 * Name:   isCLIFlagSet
 * Prototype:  void isCLIFlagSet( int )
 *
 * Description:
 * Checks to see if an option is set in cliOptions.flags using a thread lock.
 * 
 * Returns;
 * 0 if requested flag is not set.
 * 1 if requested flag is set.
 * 
 * Notes:
 * Thread safe.
 *========================================================================*/
int
isCLIFlagSet( int bits )
{
    int status = 0;

    pthread_mutex_lock( &cliOptionsMutex );
    if ( cliOptions.flags & bits )
        status = 1;
    pthread_mutex_unlock( &cliOptionsMutex );

    return status;
}

/*========================================================================
 * Name:   setCLIFlag
 * Prototype:  void setCLIFlag( int )
 *
 * Description:
 * Set options is in cliOptions.flags using a thread lock.
 *
 * Notes:
 * Thread safe.
 *========================================================================*/
void
setCLIFlag( int bits )
{
    pthread_mutex_lock( &cliOptionsMutex );
    cliOptions.flags |= bits;
    pthread_mutex_unlock( &cliOptionsMutex );
}

/*========================================================================
 * Name:   unsetCLIFlag
 * Prototype:  void unsetCLIFlag( int )
 *
 * Description:
 * Unset options is in cliOptions.flags using a thread lock.
 *
 * Notes:
 * Thread safe.
 *========================================================================*/
void
unsetCLIFlag( int bits )
{
    pthread_mutex_lock( &cliOptionsMutex );
    cliOptions.flags &= ~bits;
    pthread_mutex_unlock( &cliOptionsMutex );
}

