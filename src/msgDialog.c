/*******************************************************************************
 * pibox-network-config
 *
 * msgDialog.c:  Functions for working with a message dialog.
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define MSGDIALOG_C

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <gtk/gtk.h>
#include <pibox/utils.h>

#include "main.h"
#include "cli.h"
#include "pnc.h"
#include "callbacks.h"

/*
 * Display a generic error dialog
 */
void
errorDialog( char *msg )
{
    GtkWidget *mainWindow = NULL;
    GtkWidget *dialog = NULL;

    mainWindow = getMainWindow();
    dialog = gtk_message_dialog_new (GTK_WINDOW(mainWindow),
                                     GTK_DIALOG_DESTROY_WITH_PARENT,
                                     GTK_MESSAGE_ERROR,
                                     GTK_BUTTONS_CLOSE,
                                     msg);
    gtk_window_set_title( GTK_WINDOW(dialog), "Error");
    gtk_dialog_run (GTK_DIALOG (dialog));
    gtk_widget_destroy (dialog);
}

/*
 * Display a generic informational dialog
 */
void
infoDialog( char *msg )
{
    GtkWidget *mainWindow = NULL;
    GtkWidget *dialog = NULL;

    mainWindow = getMainWindow();
    dialog = gtk_message_dialog_new (GTK_WINDOW(mainWindow),
                                     GTK_DIALOG_DESTROY_WITH_PARENT,
                                     GTK_MESSAGE_INFO,
                                     GTK_BUTTONS_CLOSE,
                                     msg);
    gtk_window_set_title( GTK_WINDOW(dialog), "Info");
    gtk_dialog_run (GTK_DIALOG (dialog));
    gtk_widget_destroy (dialog);
}

/*
 * Display a dialog that allows selection between two options.
 */
gint
chooseDialog( char *msg, char *title, char *btn1, char *btn2 )
{
    GtkWidget *mainWindow = NULL;
    GtkWidget *dialog = NULL;
    int response_id = -1;

    mainWindow = getMainWindow();
    dialog = gtk_message_dialog_new (GTK_WINDOW(mainWindow),
                                     GTK_DIALOG_DESTROY_WITH_PARENT,
                                     GTK_MESSAGE_QUESTION,
                                     GTK_BUTTONS_NONE,
                                     msg);
    gtk_dialog_add_button( GTK_DIALOG(dialog), btn1, 1);
    gtk_dialog_add_button( GTK_DIALOG(dialog), btn2, 2);
    gtk_window_set_title( GTK_WINDOW(dialog), title);
    response_id = gtk_dialog_run (GTK_DIALOG (dialog));
    gtk_widget_destroy (dialog);
    return response_id;
}
