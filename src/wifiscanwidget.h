/*******************************************************************************
 * pibox-network-config
 *
 * wifiscanwidget.h:  custom widget for showing wifi scan data
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef __WIFISCANWIDGET_H
#define __WIFISCANWIDGET_H

#include <gtk/gtk.h>
#include <cairo.h>

G_BEGIN_DECLS

#define GTK_WIFISCAN(obj) GTK_CHECK_CAST(obj, gtk_wifiscan_get_type (), GtkWifiscan)
#define GTK_WIFISCAN_CLASS(klass) GTK_CHECK_CLASS_CAST(klass, gtk_wifiscan_get_type(), GtkWifiscanClass)
#define GTK_IS_WIFISCAN(obj) GTK_CHECK_TYPE(obj, gtk_wifiscan_get_type())

/* Strings used in the widget */
#define NO_WIFI_S           "No wifi available"
#define WIFI_DEVICES_S      "Wifi devices"
#define NO_CHANNEL_S        "No channel data"
#define WIFI_CHANNEL_S      "Channel usage"
#define NO_BSS_S            "No BSS data available"
#define SSID_S              "SSID Signal Strength"

/* Number of tick marks depends on number of channels */
#define MAX_2G_TICKS        41
#define MAX_5G_TICKS        TBD
#define MAX_DBM_TICKS       7

typedef struct _GtkWifiscan GtkWifiscan;
typedef struct _GtkWifiscanClass GtkWifiscanClass;

struct _GtkWifiscan {
  GtkDrawingArea parent;
  GdkPixmap *pixmap;
  GdkColor bg;
  GdkColor fg;
  GSList *wifi_devices;
  GSList *bss_list;
  GHashTable *channels;
  GHashTable *color_hash;
};

struct _GtkWifiscanClass {
  GtkDrawingAreaClass parent_class;
};

/* Prototypes */
GtkType gtk_wifiscan_get_type(void);
GtkWidget *gtk_wifiscan_new();
void gtk_wifiscan_update( GtkWifiscan *wifiscan, 
                          GSList *devices, 
                          GSList *bss, 
                          GHashTable *channels );

G_END_DECLS

#endif /* __WIFISCANWIDGET_H */
