/*******************************************************************************
 * pibox-network-config
 *
 * main.c:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define MAIN_C

/*
 * Initial main.c file generated by Glade. Edit as required.
 * Glade will not overwrite this file.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <gtk/gtk.h>
#include <unistd.h>
#include <pibox/log.h>

#include "wifiscanwidget.h"
#include "interface.h"
#include "callbacks.h"
#include "support.h"
#include "cli.h"
#include "pnc.h"
#include "wifiscan.h"

GtkWidget   *window1;
GSList      *wifiDevices = NULL;
GSList      *bssList = NULL;
GHashTable  *channels = NULL;

/*
 *========================================================================
 * Name:   updateScan
 * Prototype:  gboolean updateScan( gpointer )
 *
 * Description:
 * Called periodically from a glib timer to cause the scan area to be
 * updated.
 *========================================================================
 */
gboolean updateScan (gpointer data)
{
    GSList      *new_devices = NULL;
    GSList      *new_bss = NULL;
    GHashTable  *new_channels = NULL;
    char        found = 0;

    piboxLogger(LOG_TRACE2, "Getting wifi devices copy.\n");
    new_devices = getWifiList();
    if ( new_devices != NULL )
    {
        piboxLogger(LOG_TRACE2, "Found some wifi devices.\n");
        g_slist_free_full(wifiDevices, freeWifiDevices);
        wifiDevices = new_devices;
    }
    else
    {
        piboxLogger(LOG_TRACE2, "No wifi devices available.\n");
    }

    piboxLogger(LOG_TRACE2, "Getting channels copy.\n");
    new_channels = getChannelList();
    if ( new_channels != NULL )
    {
        piboxLogger(LOG_TRACE2, "Found some channels.\n");
        if ( channels != NULL )
            g_hash_table_destroy(channels);
        channels = new_channels;
    }
    else
    {
        piboxLogger(LOG_TRACE2, "No channels available.\n");
    }

    piboxLogger(LOG_TRACE2, "Getting BSS copy.\n");
    new_bss = getBSSList();
    if ( new_bss != NULL )
    {
        piboxLogger(LOG_TRACE2, "Found BSS data available.\n");
        g_slist_free_full(bssList, freeBSSList);
        bssList = new_bss;
    }
    else
    {
        piboxLogger(LOG_TRACE2, "No BSS data available.\n");
    }

    if ( ! gtk_widget_get_realized(wifiscan) || ! gtk_widget_get_visible(wifiscan) )
    {
        piboxLogger(LOG_INFO, "Scanner is not realized & visible.  Skipping display update.\n");
        return TRUE;
    }

    piboxLogger(LOG_TRACE2, "Updating wifiscan widget.\n");
    gtk_wifiscan_update(GTK_WIFISCAN(wifiscan), wifiDevices, bssList, channels);
    piboxLogger(LOG_TRACE2, "Queuing wifiscan update.\n");
    gtk_widget_queue_draw(GTK_WIDGET(wifiscan));
    return TRUE;
}

/*
 *========================================================================
 * Name:   main
 * Prototype:  int main ( int, char ** )
 *
 * Description:
 * Get it started...
 *========================================================================
 */
int
main (int argc, char *argv[])
{
    gchar       *filename = NULL;
    gint        netType = PNC_ID_NONE;
    struct stat stat_buf;
    GSList      *wifiDevices;

    // Read command line options
    parseArgs(argc, argv);
    piboxLoggerInit( cliOptions.logFile );
    piboxLoggerVerbosity( cliOptions.verbose );

    // Make GTK thread aware
    gdk_threads_init();
    gdk_threads_enter();

    // Read the data files we'll be editing.
    pncInit( isCLIFlagSet(CLI_TEST), NULL );
    pncLoadKeysyms();
    pncLoadNet();
    pncLoadInterfaces();
    pncLoadWPA();
    pncLoadResolv();
    pncLoadHostAP();
    pncGenWifiList();
    if ( pncGetWifiList() != NULL )
    {
        /* Start a thread for acquiring scan data. */
        startMgr();
    }

    filename = pncGetFilename( PNC_ID_NETTYPE );
    netType = pncReadNetType(filename);
    setNetType(netType);
    g_free( filename );

#ifdef ENABLE_NLS
    bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
    bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
    textdomain (GETTEXT_PACKAGE);
#endif

    gtk_set_locale ();
    gtk_init (&argc, &argv);
    if ( isCLIFlagSet( CLI_TEST) && (stat("data/gtkrc", &stat_buf) == 0) )
        gtk_rc_parse("data/gtkrc");
    add_pixmap_directory (PACKAGE_DATA_DIR "/" PACKAGE "/pixmaps");

    /*
     * Create the window.  If in embedded mode then display it in fullscreen.
     */
    window1 = create_window1 ();
    if ( isCLIFlagSet( CLI_EMBEDDED ) )
        gtk_window_fullscreen (GTK_WINDOW(window1));
    gtk_widget_show_all(window1);
    setMapped(1);
    netTypeSetup();

    piboxLogger(LOG_INFO, "nls: ========================================\n");
    pncPrintInterfaces();
    piboxLogger(LOG_INFO, "nls: ========================================\n");

    showIPInStatusBar();
    on_interfaceComboBox_changed(NULL, NULL);

    // DEBUG
    piboxLogger(LOG_INFO, "gtk_main: ========================================\n");
    pncPrintInterfaces();
    piboxLogger(LOG_INFO, "gtk_main: ========================================\n");

    /* Start a thread for updating the Cairo window */
    (void)g_timeout_add(2000, (GSourceFunc)updateScan, wifiscan);
    gtk_widget_set_app_paintable(wifiscan, TRUE);
    gtk_widget_set_double_buffered(wifiscan, FALSE);

    gtk_main ();
    shutdownMgr();
    piboxLoggerShutdown();
    return 0;
}

GtkWidget *
getMainWindow()
{
    return window1;
}

