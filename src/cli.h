/*******************************************************************************
 * pibox-network-config
 *
 * cli.h:  Command line parsing
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef CLI_H
#define CLI_H

/*========================================================================
 * Type definitions
 *=======================================================================*/
#define MAXBUF          4096
#define CLI_TEST        0x00001    // Enable test mode (read data files locally)
#define CLI_EMBEDDED    0x00002    // Enable embedded mode (changes UI layout)
#define CLI_LOGTOFILE   0x00008    // Enable log to file

typedef struct _cli_t {
    int     flags;      // Enable/disable features
    char    *logFile;   // Name of local file to write log to
    int     verbose;    // Sets the verbosity level for the application
} CLI_T;


/*========================================================================
 * Text strings 
 *=======================================================================*/

/* Program name for display purposes */
#define PROGNAME    "pibox-network-config"

/* Version information should be passed from the build */
#ifndef VERSTR
#define VERSTR      "No Version String"
#endif

#ifndef VERDATE
#define VERDATE     "No Version Date"
#endif

#define CLIARGS     "eTl:v:"
#define USAGE \
"\n\
pibox-network-config [ -eT | -l <filename> | -v <level> | -h? ]\n\
where\n\
\n\
    -l filename     Enable local logging to named file \n\
    -e              Enable embedded mode (modifies UI layout) \n\
    -T              Enable test mode (use local data files) \n\
    -v level        Enable verbose output: \n\
                    0: LOG_NONE  (default) \n\
                    1: LOG_INFO            \n\
                    2: LOG_WARN            \n\
                    3: LOG_ERROR           \n\
                    4: LOG_TRACE1          \n\
                    5: LOG_TRACE2          \n\
                    6: LOG_TRACE3          \n\
                    7: LOG_TRACE4          \n\
                    8: LOG_TRACE5          \n\
\n\
"

/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef CLI_C
CLI_T cliOptions;
#else
extern CLI_T cliOptions;
#endif /* CLI_C */

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifdef CLI_H
void parseArgs(int argc, char **argv);
int  isCLIFlagSet( int bits );
void setCLIFlag( int bits );
void unsetCLIFlag( int bits );
#else
extern void parseArgs(int argc, char **argv);
extern int  isCLIFlagSet( int bits );
extern void setCLIFlag( int bits );
extern void unsetCLIFlag( int bits );
#endif

#endif /* !CLI_H */
