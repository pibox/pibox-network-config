/*******************************************************************************
 * pibox-network-config
 *
 * main.h:  utilities supporting the main window.
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define MAIN_H

/*
 * ========================================================================
 * Type definitions and Data structures 
 * =======================================================================
 */
#define NO_WIFI_S       "No wifi available"
#define WIFI_DEVICES_S  "Wifi devices"
#define WIFI_CHANNEL_S  "Channel usage"
#define NO_BSS_S        "No BSS data available"

/*
 * ========================================================================
 * Prototypes
 * =======================================================================
 */
#ifndef MAIN_C
extern GtkWidget *getMainWindow();
#endif

