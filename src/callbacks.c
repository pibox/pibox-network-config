/*******************************************************************************
 * pibox-network-config
 *
 * callbacks.c:  Functions called from event handling 
 *               in widgets defined in interface.c
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define CALLBACKS_C

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <dirent.h>
#include <pthread.h>
#include <signal.h>
#include <libgen.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <pibox/pibox.h>
#include <pibox/log.h>
#include <pibox/utils.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"

#include "cli.h"
#include "pnc.h"
#include "pncPrivate.h"
#include "main.h"
#include "wifiscan.h"

#define RESTART_CMD "/etc/init.d/S40network"

GtkWidget   *statusBar           = NULL;
GtkWidget   *ssidText            = NULL;
GtkWidget   *pwField             = NULL;
GtkWidget   *mainNotebook        = NULL;
GtkWidget   *ipv4Notebook        = NULL;
GtkWidget   *interfaceComboBox   = NULL;
GtkWidget   *addressTypeComboBox = NULL;
GtkWidget   *securityComboBox    = NULL;
pthread_t   editThread           = (pthread_t)NULL;
int         editRunning          = 0;
gint        isMapped             = 0;
gint        skipEnable           = 0;
gchar       *currentName         = NULL;
gint        netType              = PNC_ID_NONE;
guint       statusBarContextID   = 0;

GtkWidget   *addressText         = NULL;
GtkWidget   *netmaskText         = NULL;
GtkWidget   *gatewayText         = NULL;
GtkWidget   *dns1Text            = NULL;
GtkWidget   *dns2Text            = NULL;
GtkWidget   *dns3Text            = NULL;
GtkWidget   *enableYesButton     = NULL;
GtkWidget   *enableNoButton      = NULL;
GtkWidget   *macAddrText         = NULL;
GtkWidget   *ipAddrText          = NULL;

GtkWidget   *accessPointSSIDEntry     = NULL;
GtkWidget   *accessPointChannelCombo  = NULL;
GtkWidget   *accessPointPasswordEntry = NULL;
GtkWidget   *hostapNetEntry           = NULL;

GtkWidget   *wirelessPage = NULL;
GtkWidget   *accessPointPage = NULL;

cairo_t     *cr = NULL;

/* Prevents multiple drawing operations. */
static int currently_drawing = 0;

pthread_mutex_t crMutex = PTHREAD_MUTEX_INITIALIZER;

/*
 *========================================================================
 * Setters
 *
 * Description:
 * Set state variables.
 *========================================================================
 */
void
setMapped(gint state)
{
    isMapped = state;
}

void
setNetType (gint type )
{
    netType = type;
}

/*
 * Set initial state of notebook tab's visibility.
 */
void
netTypeSetup()
{
    GtkWidget *clientPage = NULL;
    GtkWidget *wapPage = NULL;

    // Now set the visibility of the tab the widget is in.
    clientPage = gtk_notebook_get_nth_page(GTK_NOTEBOOK(mainNotebook), 1);
    wapPage = gtk_notebook_get_nth_page(GTK_NOTEBOOK(mainNotebook), 2);
    if( netType == PNC_ID_WAP )
    {
        gtk_widget_show(wapPage);
        gtk_widget_hide(clientPage);
    }
    else
    {
        gtk_widget_hide(wapPage);
        gtk_widget_show(clientPage);
    }
}

/*
 *========================================================================
 * Getters
 *
 * Description:
 * Return the contents of the specified field.
 *========================================================================
 */
const gchar *
getAddress(void)
{
    if ( isMapped && (addressText != NULL) )
        return gtk_entry_get_text(GTK_ENTRY(addressText));
    else
        return NULL;
}
const gchar *
getNetmask(void)
{
    if ( isMapped && (netmaskText != NULL) )
        return gtk_entry_get_text(GTK_ENTRY(netmaskText));
    else
        return NULL;
}
const gchar *
getGateway(void)
{
    if ( isMapped && (gatewayText != NULL) )
        return gtk_entry_get_text(GTK_ENTRY(gatewayText));
    else
        return NULL;
}
const gchar *
getDNS(gint idx)
{
    switch(idx)
    {
        case 0: return (dns1Text!=NULL)?gtk_entry_get_text(GTK_ENTRY(dns1Text)):NULL;
        case 1: return (dns2Text!=NULL)?gtk_entry_get_text(GTK_ENTRY(dns2Text)):NULL;
        case 2: return (dns3Text!=NULL)?gtk_entry_get_text(GTK_ENTRY(dns3Text)):NULL;
    }
}
gchar *
getAddressType(void)
{
    if ( isMapped && (addressTypeComboBox != NULL) )
        return gtk_combo_box_get_active_text( GTK_COMBO_BOX(addressTypeComboBox) );
    else
        return NULL;
}
gchar *
getInterface(void)
{
    if ( isMapped )
        return gtk_combo_box_get_active_text( GTK_COMBO_BOX(interfaceComboBox) );
    else
        return NULL;
}
const gchar *
getSSID(void)
{
    return gtk_entry_get_text(GTK_ENTRY(ssidText));
}
const gchar *
getPW(void)
{
    return gtk_entry_get_text(GTK_ENTRY(pwField));
}
GtkWidget *
getStatusBar(void)
{
    return statusBar;
}
guint
getStatusBarContextID(void)
{
    return statusBarContextID;
}


/*
 *========================================================================
 * Name:   editor
 * Prototype:  void *editor( void * )
 *
 * Description:
 * Thread function for managing an external process that manually
 * edits the specified file.
 *========================================================================
 */
void *
editor( void *arg )
{
    int status;
    int terminated = 0;
    pid_t childPid = -1;
    pid_t pid = 0;
    char buf[64];

    sprintf(buf, "/bin/vi %s", (char *)arg);
    char * args[] = {
        "xterm",        // arg[0] is program name
        "-e",
        buf,
        NULL
    };

    childPid = fork();
    if ( childPid == 0 )
    {
        // Child: Open specified file in xterm -e /bin/vi
        execvp("xterm", args);
        piboxLogger(LOG_ERROR, "Failed to launch xterm to edit %s\n", (char *)arg);
        abort();
    }

    // Parent: spin, waiting for child to exit or main thread to stop this thread.
    while (editRunning)
    {
        usleep(5000);
        pid = waitpid( childPid, &status, WNOHANG);
        if ( pid != 0 )
        {
            terminated = 1;
            break;
        }
    }

    // If child didn't terminate on its own, kill it.
    if ( !terminated )
    {
        // kill the child and wait for it to change state
        kill(childPid, SIGTERM);
        pid = waitpid( childPid, &status, 0);
    }

    // Clean up thread.
    editThread = (pthread_t)NULL;
    editRunning = 0;
    pthread_exit(NULL);
}

/*
 *========================================================================
 * Name:   populate
 * Prototype:  void populate( gint )
 *
 * Description:
 * Fill the fields of the ipv4 field based on which interface is selected.
 *========================================================================
 */
void
populate ( gint idx )
{
    char   *name;
    char   *macAddr;
    gchar  *value;

    name = gtk_combo_box_get_active_text( GTK_COMBO_BOX(interfaceComboBox) );
    if ( name == NULL )
    {
        piboxLogger(LOG_ERROR, "Missing name.\n");
        return;
    }
    if ( pncGetInterfaceState(name) == -1 )
    {
        piboxLogger(LOG_INFO, "No interfaces match for name: %s\n", name);

        // If this is the mac field we're trying to fill...
        if ( idx==6 )
        {
            // ...see if we can get its MAC address.
            macAddr = pncGetMacAddress(name);
            if ( macAddr != NULL )
            {
                piboxLogger(LOG_INFO, "Found mac address for: %s\n", name);
                gtk_entry_set_text(GTK_ENTRY(macAddrText), macAddr);
                g_free(macAddr);
            }
            else
            {
                piboxLogger(LOG_INFO, "No mac address for: %s\n", name);
                gtk_entry_set_text(GTK_ENTRY(macAddrText), "");
            }
        }

        g_free(name);
        return;
    }
    piboxLogger(LOG_INFO, "Populating field: %d\n", idx);
    switch(idx)
    {
        case 1:
            if (addressText != NULL)
            {
                value = pncGetInterfaceField(name, PNC_ID_IF_ADDR);
                if (value != NULL) 
                {
                    gtk_entry_set_text(GTK_ENTRY(addressText), value);
                    g_free( value );
                }
                else
                    gtk_entry_set_text(GTK_ENTRY(addressText), "");
            }
            break;

        case 2:
            if (netmaskText != NULL)
            {
                value = pncGetInterfaceField(name, PNC_ID_IF_MASK);
                if (value != NULL) 
                {
                    gtk_entry_set_text(GTK_ENTRY(netmaskText), value);
                    g_free( value );
                }
                else
                    gtk_entry_set_text(GTK_ENTRY(netmaskText), "");
            }
            break;

        case 3:
            if (gatewayText != NULL)
            {
                value = pncGetInterfaceField(name, PNC_ID_IF_GATEWAY);
                if (value != NULL) 
                {
                    gtk_entry_set_text(GTK_ENTRY(gatewayText), value);
                    g_free( value );
                }
                else
                    gtk_entry_set_text(GTK_ENTRY(gatewayText), "");
            }
            break;

        case 4:
            if (addressTypeComboBox != NULL)
            {
                value = pncGetInterfaceField(name, PNC_ID_IF_ADDRTYPE);
                if (value != NULL) 
                {
                    if (strcasecmp(value, "DHCP") == 0 )
                        gtk_combo_box_set_active(GTK_COMBO_BOX(addressTypeComboBox), 0);
                    else
                        gtk_combo_box_set_active(GTK_COMBO_BOX(addressTypeComboBox), 1);
                    g_free( value );
                }
            }
            break;

        case 5:
            piboxLogger(LOG_INFO, "Setting enable buttons.\n");
            if ( (enableYesButton != NULL) && (enableNoButton != NULL) )
            {
                skipEnable = 1;
                if ( pncGetInterfaceState(name) == 1)
                {
                    piboxLogger(LOG_INFO, "Setting yes buttons.\n");
                    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(enableYesButton), 1);
                    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(enableNoButton), 0);
                }
                else
                {
                    piboxLogger(LOG_INFO, "Setting no buttons.\n");
                    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(enableYesButton), 0);
                    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(enableNoButton), 1);
                }
                skipEnable = 0;
            }
            break;

        case 6:
            if (macAddrText != NULL)
            {
                value = pncGetInterfaceField(name, PNC_ID_IF_MAC);
                if (value != NULL) 
                {
                    piboxLogger(LOG_INFO, "Setting mac field: %s\n", value);
                    gtk_entry_set_text(GTK_ENTRY(macAddrText), value);
                    g_free( value );
                }
                else
                {
                    // ...see if we can get its MAC address.
                    macAddr = pncGetMacAddress(name);
                    if ( macAddr != NULL )
                    {
                        piboxLogger(LOG_INFO, "Found mac address for: %s\n", name);
                        gtk_entry_set_text(GTK_ENTRY(macAddrText), macAddr);
                        g_free(macAddr);
                    }
                    else
                    {
                        piboxLogger(LOG_INFO, "Clearing mac field.\n");
                        gtk_entry_set_text(GTK_ENTRY(macAddrText), "");
                    }
                }
            }
            break;
    }
    g_free(name);
}

/*
 *========================================================================
 * Name:   populateDNS
 * Prototype:  void populateDNS( gint )
 *
 * Description:
 * Fill the DNS fields.
 *========================================================================
 */
void
populateDNS ( gint idx )
{
    char   *name;

    name = pncGetNameserver(idx);
    if ( name == NULL )
    {
        piboxLogger(LOG_ERROR, "No nameserver for DNS%d\n", idx);
        return;
    }
    switch(idx)
    {
        case 0:
            gtk_entry_set_text(GTK_ENTRY(dns1Text), name);
            break;

        case 1:
            gtk_entry_set_text(GTK_ENTRY(dns2Text), name);
            break;

        case 2:
            gtk_entry_set_text(GTK_ENTRY(dns3Text), name);
            break;
    }
    g_free( name );
}

/*
 *========================================================================
 * Name:   addNetInterfaces
 * Prototype:  void addNetInterfaces(GtkComboBox *, gpointer)
 *
 * Description:
 * Add any network interfaces found that were not in /etc/network/interfaces
 * to our list of configurable interfaces.
 *========================================================================
 */
void 
addNetInterfaces( gpointer item, gpointer user_data )
{
    char *name = (char *)item;
    if ( name != NULL )
    {
        gtk_combo_box_append_text(GTK_COMBO_BOX(interfaceComboBox), name);
        if ( pncGetInterfaceState(name) == -1 )
        {
            piboxLogger(LOG_TRACE1, "%s(%d): Calling pncNewInterface\n", __FUNCTION__, __LINE__);
            pncNewInterface(name);
        }
    }
}

/*
 *========================================================================
 * Name:   on_addressTypeComboBox_changed
 * Prototype:  void n_addressTypeComboBox_changed(GtkComboBox *, gpointer)
 *
 * Description:
 * Handler for addressType combo box changes.
 *========================================================================
 */
void
on_addressTypeComboBox_changed         (GtkComboBox     *combobox,
                                        gpointer         user_data)
{
    char *name, *value[3];
    int  page = -1;
    int  i;

    if ( !isMapped )
        return;

    /* 
     * Set the visible page of the sub-notebook of the 
     * IPv4 page of the top level notebook.
     * Note: comboBox index == page index
     */
    if ( ipv4Notebook != NULL )
    {
        page = gtk_combo_box_get_active(combobox);
        if ( page != -1 )
            gtk_notebook_set_current_page(GTK_NOTEBOOK(ipv4Notebook), page);
    }
    if ( interfaceComboBox != NULL )
    {
        name = gtk_combo_box_get_active_text( GTK_COMBO_BOX(interfaceComboBox) );
        pncUpdateInterface(name, getAddress(), getAddressType(), getNetmask(), getGateway());
        g_free(name);
    }

    for (i=0; i<3; i++)
        value[i] = g_strdup(getDNS(i));
    pncSetResolvField( value[0], value[1], value[2] );
    for (i=0; i<3; i++)
        g_free(value[i]);
}

/*
 *========================================================================
 * Name:   on_interfaceComboBox_changed
 * Prototype:  void on_interfaceComboBox_changed(GtkComboBox *, gpointer)
 *
 * Description:
 * Set the addressTypeCombo based on interface seetings in 
 * /etc/network/interfaces
 *========================================================================
 */
void
on_interfaceComboBox_changed           (GtkComboBox     *combobox,
                                        gpointer         user_data)
{
    char *name, *value[3];
    int  i;

    if ( !isMapped )
    {
        piboxLogger(LOG_INFO, "Interface combo is not mapped yet.\n");
        return;
    }

    // save changes
    pncUpdateInterface(currentName, getAddress(), getAddressType(), getNetmask(), getGateway());
    for (i=0; i<3; i++)
        value[i] = g_strdup(getDNS(i));
    pncSetResolvField( value[0], value[1], value[2] );
    for (i=0; i<3; i++)
        g_free(value[i]);

    name = gtk_combo_box_get_active_text( GTK_COMBO_BOX(interfaceComboBox) );
    if ( currentName != NULL ) 
        g_free(currentName); 
    currentName = name;

    // Update IPV4 fields.
    piboxLogger(LOG_INFO, "Interface combo changer is updating fields.\n");
    populate(1);
    populate(2);
    populate(3);
    populate(4);
    populate(5);
    populate(6);
    showIPInStatusBar();
}

/*
 *========================================================================
 * Name:   on_interfaceComboBox_realize
 * Prototype:  void on_interfaceComboBox_realize(GtkWidget *, gpointer)
 *
 * Description:
 * Get the list of interfaces and add them to the calling combo widget
 *========================================================================
 */
void
on_interfaceComboBox_realize           (GtkWidget       *widget,
                                        gpointer         user_data)
{
    char   *name;
    GSList *interfaces    = NULL;
    GSList *item          = NULL;

    // Save widget for use by other callbacks.
    interfaceComboBox = widget;

    interfaces = pncGetNetInterfaces();
    if ( interfaces == NULL )
    {
        piboxLogger(LOG_ERROR, "No network devices found.\n");
        return;
    }
    g_slist_foreach(interfaces, addNetInterfaces, NULL);

    // Set the first entry as the active entry.
    gtk_combo_box_set_active(GTK_COMBO_BOX(widget), 0);

    name = gtk_combo_box_get_active_text( GTK_COMBO_BOX(interfaceComboBox) );
    currentName = g_strdup(name);
    g_free(name);
}

/*
 *========================================================================
 * Name:   on_combobox3_changed
 * Prototype:  void on_combobox3_changed( GtkComboBox *, gpointer )
 *
 * Description:
 * This is securityComboBox: update the WPA list on changes.
 *========================================================================
 */
void
on_combobox3_changed                   (GtkComboBox     *combobox,
                                        gpointer         user_data)
{
    gchar *value;
    value = gtk_combo_box_get_active_text( GTK_COMBO_BOX(combobox) );
    pncSetWPAField( PNC_ID_WPA_KEYMGMT, value );
    g_free( value );
}

/*
 *========================================================================
 * Name:   updateWPA
 * Prototype:  void updateWPA()
 *
 * Description:
 * Copy UI components for WPA configuration into a glist for processing.
 *========================================================================
 */
void 
updateWPA()
{
    gchar *value;

    value = g_strdup(gtk_entry_get_text(GTK_ENTRY(pwField)));
    pncSetWPAField( PNC_ID_WPA_PSK, value );
    g_free(value);

    value = g_strdup(gtk_entry_get_text(GTK_ENTRY(ssidText)));
    pncSetWPAField( PNC_ID_WPA_SSID, value );
    g_free(value);
}

/*
 *========================================================================
 * Name:   on_button1_clicked
 * Prototype:  void on_button1_clicked( GtkButton *, gpointer )
 *
 * Description:
 * Save Button handler.  Updates either /etc/network/interfaces,
 * /etc/wpa_supplicant.conf or hostapd configuration depending on 
 * which top level tab is in use.
 * 
 * Notes:
 * If test mode is enabled this function will not overwrite the test data
 * file.  It will write to a file with a .bak suffix to use for comparison.
 *========================================================================
 */
void
on_button1_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{
    char *name, *value[3];
    char *filename;
    int  i;

    if ( gtk_notebook_get_current_page(GTK_NOTEBOOK(mainNotebook)) == 0 )
    {
        // IPV4 Tab
        name = gtk_combo_box_get_active_text( GTK_COMBO_BOX(interfaceComboBox) );
        pncUpdateInterface(name, getAddress(), getAddressType(), getNetmask(), getGateway());
        g_free(name);
        for (i=0; i<3; i++)
            value[i] = g_strdup(getDNS(i));
        pncSetResolvField( value[0], value[1], value[2] );
        for (i=0; i<3; i++)
            g_free(value[i]);

        // Call save interfaces 
        filename = pncGetFilename( PNC_ID_INTERFACES );
        pncSaveInterface(filename);
        g_free(filename);

        // If Statis addressing is being used, save the DNS settings.
        filename = pncGetFilename( PNC_ID_RESOLV );
        if ( gtk_notebook_get_current_page(GTK_NOTEBOOK(ipv4Notebook)) == 1 )
            pncSaveDNS(filename);
        else
            pncRemoveDNS(filename);
        g_free( filename );
    }
    else if ( gtk_notebook_get_current_page(GTK_NOTEBOOK(mainNotebook)) == 1 )
    {
        // Wireless Tab
        filename = pncGetFilename( PNC_ID_WPASUPPLICANT );
        updateWPA();
        pncSaveWPA(filename);
        g_free(filename);
    }
    else if ( gtk_notebook_get_current_page(GTK_NOTEBOOK(mainNotebook)) == 2 )
    {
        // Access Point Tab
        filename = pncGetFilename( PNC_ID_HOSTAPDCONF );
        if ( updateHostAP() == 0 )
            piboxLogger(LOG_ERROR, "Failed to updateHostAP\n");
        else
            if ( updateHostAP() == 0 )
                piboxLogger(LOG_ERROR, "Failed to updateHostAP\n");
            else
                pncSaveHostAP(filename);
        g_free(filename);
    }

    // Save the marker that says whether wireless client or access point is being used.
    filename = pncGetFilename( PNC_ID_NETTYPE );
    pncSaveNetType(filename, netType);
    g_free( filename );

    // update displayed network address in status bar
    name = gtk_combo_box_get_active_text( GTK_COMBO_BOX(interfaceComboBox) );
    if ( pncGetInterfaceState(name) == 1 )
        showIPInStatusBar();
    g_free(name);
}

/*
 *========================================================================
 * Name:   on_open1_activate
 * Prototype:  void on_open1_activate ( GtkMenuItem *, gpointer )
 *
 * Description:
 * File->Open menu handler.  When selected, starts a thread that forks
 * a process to edit a file manually.
 * Using a thread allows us to more easily manage killing the process
 * directly from the pibox-network-config UI.
 *========================================================================
 */
void
on_open1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
    GtkWidget   *dialog;
    char        *filename;
    int         rc = 0; 
    int         response_id = 0; 
    struct stat stat_buf;

    if ( editRunning )
        return;
    editRunning = 1;

    // Chose a file to edit.
    if ( gtk_notebook_get_current_page(GTK_NOTEBOOK(mainNotebook)) == 0 )
        filename = pncGetFilename( PNC_ID_INTERFACES );
    else if ( gtk_notebook_get_current_page(GTK_NOTEBOOK(mainNotebook)) == 1 )
        filename = pncGetFilename( PNC_ID_WPASUPPLICANT );
    else if ( gtk_notebook_get_current_page(GTK_NOTEBOOK(mainNotebook)) == 2 )
    {
        response_id = chooseDialog("Which page do you want to edit", 
                                   "Access Point File Edit", 
                                   "hostapd.conf", 
                                   "dhcpd.conf");
        if ( response_id == 1 )
            filename = pncGetFilename( PNC_ID_HOSTAPDCONF );
        else
            filename = pncGetFilename( PNC_ID_DHCP );
    }

    // Make sure file exists
    if ( stat(filename, &stat_buf) != 0 )
    {
        dialog = gtk_message_dialog_new( GTK_WINDOW(lookup_widget(mainNotebook, "window1")),
                                 GTK_DIALOG_DESTROY_WITH_PARENT,
                                 GTK_MESSAGE_ERROR,
                                 GTK_BUTTONS_CLOSE,
                                 "No such file: %s", filename);
        gtk_dialog_run (GTK_DIALOG (dialog));
        gtk_widget_destroy (dialog);
        editRunning = 0;
        g_free(filename);
        return;
    }

    /* Create a thread to edit the file manually. */
    rc = pthread_create(&editThread, NULL, editor, filename);
    if (rc)
    {
        editRunning = 0;
        piboxLogger(LOG_ERROR, "Failed to create editor thread. \n");
        fflush(stderr);
    }
    g_free(filename);
}

/*
 *========================================================================
 * Name:   on_save1_activate
 * Prototype:  void on_save1_activate ( GtkMenuItem *, gpointer )
 *
 * Description:
 * Save button handler.  This is the same as on_button1_clicked().
 *========================================================================
 */
void
on_save1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
    on_button1_clicked(NULL, NULL);
}

/*
 *========================================================================
 * Name:   on_save_as1_activate
 * Prototype:  void on_save_as1_activate ( GtkMenuItem *, gpointer )
 *
 * Description:
 * Same as on_button1_clicked, but allows specifying an alternate filename.
 *========================================================================
 */
void
on_save_as1_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
    GtkWidget   *dialog;
    char        *filename;
    gchar       *path;
    char        srcPath[PATH_MAX];
    char        srcFile[PATH_MAX];
    struct stat stat_buf;

    if ( gtk_notebook_get_current_page(GTK_NOTEBOOK(mainNotebook)) == 0 )
    {
        // interfaces file
        path = pncGetFilename( PNC_ID_INTERFACES );
        sprintf(srcFile, "%s", basename(path));
        g_free(path);
    }
    else if ( gtk_notebook_get_current_page(GTK_NOTEBOOK(mainNotebook)) == 1 )
    {
        // wpa_supplicant.conf
        path = pncGetFilename( PNC_ID_RESOLV );
        sprintf(srcFile, "%s", basename(path));
        g_free(path);
    }
    else 
    {
        // HostAP
        path = pncGetFilename( PNC_ID_HOSTAPDCONF );
        sprintf(srcFile, "%s", basename(path));
        g_free(path);
    }

    // Get a filename using file selection dialog.
    dialog = gtk_file_chooser_dialog_new (
                "Save As",
                GTK_WINDOW(lookup_widget(mainNotebook, "window1")),
                GTK_FILE_CHOOSER_ACTION_SAVE,
                GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
                NULL);
    gtk_file_chooser_set_do_overwrite_confirmation (GTK_FILE_CHOOSER (dialog), TRUE);
    if ( stat(srcPath, &stat_buf) == 0 )
    {
        gtk_file_chooser_set_filename (GTK_FILE_CHOOSER (dialog), srcPath);
    }
    else
    {
        // If file is new we'll get GtkWarning on console, but we can ignore it.
        // Don't know how to get rid of that warning.
        gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (dialog), srcPath);
        gtk_file_chooser_set_current_name (GTK_FILE_CHOOSER (dialog), srcFile);
    }

    if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
    {
        char *filename;

        filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
        if ( gtk_notebook_get_current_page(GTK_NOTEBOOK(mainNotebook)) == 0 )
        {
            pncSaveInterface(filename);
            pncSaveDNS(filename);
        }
        else if ( gtk_notebook_get_current_page(GTK_NOTEBOOK(mainNotebook)) == 1 )
        {
            updateWPA();
            pncSaveWPA(filename);
        }
        else 
        {
            if ( updateHostAP() == 0 )
                piboxLogger(LOG_ERROR, "Failed to updateHostAP\n");
            else
                pncSaveHostAP(filename);
        }
        g_free (filename);
    }

    gtk_widget_destroy (dialog);
}

/*
 *========================================================================
 * Name:   on_quit1_activate
 * Prototype:  void on_quit1_activate ( GtkMenuItem *, gpointer )
 *
 * Description:
 * File->Quit menu handler.  This kills off any child processes and then
 * exits the application.
 *========================================================================
 */
void
on_quit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
    doShutdown(NULL, NULL, 0);
    gtk_main_quit();
}

/*
 *========================================================================
 * Name:   on_about1_activate
 * Prototype:  void on_about1_activate ( GtkMenuItem *, gpointer )
 *
 * Description:
 * Help->About menu handler. Displays the About dialog.
 *========================================================================
 */
void
on_about1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
    GtkWidget *dialog;

    // Show about dialog
    dialog = create_aboutdialog1 ();
    gtk_widget_show(dialog);
}

/*
 *========================================================================
 * Name:   on_addressTypeComboBox_realize
 * Prototype:  void on_addressTypeComboBox_realize ( GtkWidget *, gpointer )
 *
 * Description:
 * Sets the active item in the addressType ComboBox at startup.
 *========================================================================
 */
void
on_addressTypeComboBox_realize         (GtkWidget       *widget,
                                        gpointer         user_data)
{
    gchar *name;
    gchar *value;

    // Save widget for use by other callbacks.
    addressTypeComboBox = widget;

    name = pncGetInterface0();
    if ( name == NULL )
    {
        piboxLogger(LOG_ERROR, "IPV4 notebook: No network devices found.\n");
        return;
    }

    value = pncGetInterfaceField(name, PNC_ID_IF_ADDRTYPE );
    if ( value != NULL )
    {
        if ( strcasecmp(value, "DHCP") == 0 )
            gtk_combo_box_set_active(GTK_COMBO_BOX(widget), 0);
        else
            gtk_combo_box_set_active(GTK_COMBO_BOX(widget), 1);
        g_free( value );
    }
    else
    {
        if ( name != NULL )
            piboxLogger(LOG_ERROR, "addressTypeComboBox: no address type specified for configuration: %s\n", name);
        else
            piboxLogger(LOG_ERROR, "addressTypeComboBox: no address type specified for configuration: unknown\n");
    }

}

/*
 *========================================================================
 * Name:   on_combobox3_realize
 * Prototype:  void on_combobox3_realize ( GtkWidget *, gpointer )
 *
 * Description:
 * Sets the active item in the security ComboBox at startup.
 *========================================================================
 */
void
on_combobox3_realize                   (GtkWidget       *widget,
                                        gpointer         user_data)
{
    // securityComboBox realize 
    gtk_combo_box_set_active(GTK_COMBO_BOX(widget), 0);
}

/*
 *========================================================================
 * Name:   on_showPasswordButton_clicked
 * Prototype:  void on_showPasswordButton_clicked ( GtkButton *, gpointer )
 *
 * Description:
 * Toggle visibility of the password field 
 *========================================================================
 */
void
on_showPasswordButton_clicked          (GtkButton       *button,
                                        gpointer         user_data)
{
    if ( pwField != NULL )
        gtk_entry_set_visibility (GTK_ENTRY(pwField), 
            gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(button))
            );
}

/*
 *========================================================================
 * Name:   on_aboutdialog1_response
 * Prototype:  void on_aboutdialog1_response( GtkDialog *, gint, gpointer )
 *
 * Description:
 * About dialog event handler.  Destroy the About dialog.
 *========================================================================
 */
void
on_aboutdialog1_response               (GtkDialog       *dialog,
                                        gint             response_id,
                                        gpointer         user_data)
{
    gtk_widget_destroy(GTK_WIDGET(dialog));
}

/*
 *========================================================================
 * Name:   on_passwordTextEntry_realize
 * Prototype:  void on_passwordTextEntry_realize ( GtkWidget *, gpointer )
 *
 * Description:
 * When realized, Save the passwordText widget for use by this module.
 *========================================================================
 */
void
on_passwordTextEntry_realize           (GtkWidget       *widget,
                                        gpointer         user_data)
{
    gchar *value;

    if ( pwField == NULL )
        pwField = widget;

    value = pncGetWPAField( PNC_ID_WPA_PSK );
    if ( value != NULL )
    {
        gtk_entry_set_text(GTK_ENTRY(pwField), value);
        g_free( value );
    }
}

/*
 *========================================================================
 * Name:   on_notebook2_realize
 * Prototype:  void on_notebook2_realize ( GtkWidget *, gpointer )
 *
 * Description:
 * When realized, Save the sub-notebook in the IPv4 tab of the top level
 * notebook for use by this module.  Also set the first page of the 
 * sub-notebook to whatever the first device configuration is.
 *========================================================================
 */
void
on_notebook2_realize                   (GtkWidget       *widget,
                                        gpointer         user_data)
{
    gchar *value;
    gchar *name;

    if ( ipv4Notebook == NULL )
        ipv4Notebook = widget;

    name = pncGetInterface0();
    if ( name == NULL )
    {
        piboxLogger(LOG_ERROR, "IPV4 notebook: missing first device configuration.\n");
        return;
    }
    value = pncGetInterfaceField( name, PNC_ID_IF_ADDRTYPE );
    if ( value != NULL )
    {
        if ( strcasecmp(value, "DHCP") == 0 )
            gtk_notebook_set_current_page(GTK_NOTEBOOK(ipv4Notebook), 0);
        else
            gtk_notebook_set_current_page(GTK_NOTEBOOK(ipv4Notebook), 1);
        g_free( value );
    }
    else
        piboxLogger(LOG_ERROR, 
            "IPV4 notebook: no address type specified for configuration: %s\n", name);

    // Initialize the yes/no buttons.
    g_free(name);
    populate(5);
}

/*
 *========================================================================
 * Name:   shutdown
 * Prototype:  gboolean shutdown ( GtkWidget *, GdkEvent *, gpointer )
 *
 * Description:
 * On delete or quit events clean up any threads and processes.
 * Be sure to give them time to complete before trying to exit.
 *========================================================================
 */
gboolean
doShutdown                             (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
    editRunning = 0;
    usleep(50000);
    pncClearInterfaces();
    return FALSE;
}

/*
 *========================================================================
 * Name:   on_restart1_activate
 * Prototype:  gboolean on_restart1_activate ( GtkWidget *, GdkEvent *, gpointer )
 *
 * Description:
 * Restart the network configuration.
 *========================================================================
 */
void
on_restart1_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
    GtkWidget   *dialog;
    struct stat stat_buf;
    gchar       buf[128];

    if ( stat(RESTART_CMD, &stat_buf) == 0 )
    {
        sprintf(buf, "%s restart", RESTART_CMD);
        system(buf);
        showIPInStatusBar();
    }
    else
    {
        dialog = gtk_message_dialog_new( GTK_WINDOW(lookup_widget(mainNotebook, "window1")),
                                 GTK_DIALOG_DESTROY_WITH_PARENT,
                                 GTK_MESSAGE_ERROR,
                                 GTK_BUTTONS_CLOSE,
                                 "No such file: %s", RESTART_CMD);
        gtk_dialog_run (GTK_DIALOG (dialog));
        gtk_widget_destroy (dialog);
    }
}

/*
 *========================================================================
 * Name:   Realize methods for IPv# text input fields.
 *
 * Description:
 * When these fields are realized, look up their settings based on the
 * interface combo box settings.
 *========================================================================
 */
void
on_ipAddressText_realize               (GtkWidget       *widget,
                                        gpointer         user_data)
{
    addressText = widget;
    populate(1);
}

void
on_netmaskText_realize                 (GtkWidget       *widget,
                                        gpointer         user_data)
{
    netmaskText = widget;
    populate(2);
}

void
on_gatewayText_realize                 (GtkWidget       *widget,
                                        gpointer         user_data)
{
    gatewayText = widget;
    populate(3);
}

void
on_dns1Text_realize                    (GtkWidget       *widget,
                                        gpointer         user_data)
{
    dns1Text = widget;
    populateDNS(0);
}

void
on_dns2_realize                        (GtkWidget       *widget,
                                        gpointer         user_data)
{
    dns2Text = widget;
    populateDNS(1);
}

void
on_dns3Text_realize                    (GtkWidget       *widget,
                                        gpointer         user_data)
{
    dns3Text = widget;
    populateDNS(2);
}

void
on_notebook1_realize                   (GtkWidget       *widget,
                                        gpointer         user_data)
{
    mainNotebook = widget;
}


void
on_entry11_realize                     (GtkWidget       *widget,
                                        gpointer         user_data)
{
    gchar *value;

    ssidText = widget;
    value = pncGetWPAField( PNC_ID_WPA_SSID );
    if ( value != NULL )
    {
        gtk_entry_set_text(GTK_ENTRY(ssidText), value);
        g_free( value );
    }
}

/*
 *========================================================================
 * Name:   on_radiobutton1_clicked (re: enable buttons)
 *
 * Description:
 * Set the interface's enable field based on UI selection.
 *========================================================================
 */
void
on_radiobutton1_clicked                (GtkButton       *button,
                                        gpointer         user_data)
{
    gchar *name = NULL;

    if (skipEnable == 1)
        return;

    if (interfaceComboBox == NULL)
        return;

    // Find the currently selected interface.
    name = gtk_combo_box_get_active_text( GTK_COMBO_BOX(interfaceComboBox) );

    // Update it's "enabled" field.
    if( gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( button ) ) )
        pncSetInterfaceState(name, 1);
    else
        pncSetInterfaceState(name, 0);
}


/*
 *========================================================================
 * Name:   Realize methods for IPv# enable buttons.
 *
 * Description:
 * When these fields are realized, save their widget ids.
 *========================================================================
 */
void
on_radiobutton1_realize                (GtkWidget       *widget,
                                        gpointer         user_data)
{
    enableYesButton = widget;
}


void
on_radiobutton2_realize                (GtkWidget       *widget,
                                        gpointer         user_data)
{
    enableNoButton = widget;
}


/*
 *========================================================================
 * Name:   Realize methods for Access Point fields.
 *
 * Description:
 * When these fields are realized, save their widget ids.
 *========================================================================
 */
void
on_accessPointSSIDEntry_realize        (GtkWidget       *widget,
                                        gpointer         user_data)
{
    gchar *value;
    accessPointSSIDEntry = widget;

    // Find the pre-configured channel, if any.
    value = pncGetHostAPField( PNC_ID_HAP_SSID );
    if ( value != NULL )
    {
        gtk_entry_set_text(GTK_ENTRY(widget), value);
        g_free(value);
    }
}

void
on_accessPointChannel_realize          (GtkWidget       *widget,
                                        gpointer         user_data)
{
    int channel = 0;
    gchar *value;

    accessPointChannelCombo = widget;

    // Find the pre-configured channel, if any.
    value = pncGetHostAPField( PNC_ID_HAP_CHANNEL );
    if ( value == NULL )
    {
        gtk_combo_box_set_active(GTK_COMBO_BOX(widget), channel);
        return;
    }
    channel = atoi(value);
    switch (channel)
    {
        case 1 : gtk_combo_box_set_active(GTK_COMBO_BOX(widget), 0); break;
        case 5 : gtk_combo_box_set_active(GTK_COMBO_BOX(widget), 1); break;
        case 9 : gtk_combo_box_set_active(GTK_COMBO_BOX(widget), 2); break;
        case 13: gtk_combo_box_set_active(GTK_COMBO_BOX(widget), 3); break;
        default: gtk_combo_box_set_active(GTK_COMBO_BOX(widget), 0); break;
    }
    g_free(value);
}

void
on_accessPointPasswordEntry_realize    (GtkWidget       *widget,
                                        gpointer         user_data)
{
    gchar  *password;
    gchar  *value;

    if ( accessPointPasswordEntry == NULL )
        accessPointPasswordEntry = widget;

    // Load the password file to get the password, then stuff it into
    // the password field.
    value = pncGetHostAPField( PNC_ID_HAP_WPA_PSK_FILE );
    if ( value != NULL )
    {
        piboxLogger(LOG_ERROR, "HostAP PSK File: %s\n", value);
        password = pncGetHostAPPassword( value );
        if ( password != NULL )
        {
            piboxLogger(LOG_ERROR, "pw is not null: %s\n", password);
            gtk_entry_set_text(GTK_ENTRY(accessPointPasswordEntry), password);
            g_free(password);
        }
        g_free( value );
    }
}

void
on_hostapNetEntry_realize              (GtkWidget       *widget,
                                        gpointer         user_data)
{
    gchar *value1;
    gchar *value2;

    hostapNetEntry = widget;

    value1 = pncGetHostAPField( PNC_ID_HAP_INTERFACE );
    if (value1 != NULL)
    {
        value2 = pncGetInterfaceField(value1, PNC_ID_IF_ADDR);
        piboxLogger(LOG_INFO, "Base network address for %s is ...\n", value1);
        g_free( value1 );
    }
    else
    {
        value2 = pncGetInterfaceField("wlan0", PNC_ID_IF_ADDR);
        piboxLogger(LOG_INFO, "Base network address for default (wlan0) is ...\n");
    }

    if (value2 != NULL)
    {
        gtk_entry_set_text(GTK_ENTRY(widget), value2);
        piboxLogger(LOG_INFO, "%s\n", value2);
        g_free( value2 );
    }
    else
        piboxLogger(LOG_INFO, "No base network address available\n");
}

/*
 *========================================================================
 * Name:  on_accessPointChannel_changed
 *
 * Description:
 * Grab the newly selected channel for the Access Point.
 *========================================================================
 */
void
on_accessPointChannel_changed          (GtkComboBox     *combobox,
                                        gpointer         user_data)
{
    gchar *value;

    // Find the pre-configured channel, if any.
    value = g_strdup(gtk_combo_box_get_active_text( GTK_COMBO_BOX(accessPointChannelCombo)));
    pncSetHostAPField( PNC_ID_HAP_CHANNEL, value );
    g_free( value );
}

/*
 *========================================================================
 * Name:  on_accessPointShowPasswordButton_clicked
 *
 * Description:
 * When set, display the password field for the Access Point.
 *========================================================================
 */
void
on_accessPointShowPasswordButton_clicked
                                        (GtkButton       *button,
                                        gpointer         user_data)
{
    if ( accessPointPasswordEntry != NULL )
        gtk_entry_set_visibility (GTK_ENTRY(accessPointPasswordEntry), 
            gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(button))
            );
}

/*
 *========================================================================
 * Name:   updateHostAP
 * Prototype:  int updateHostAP()
 *
 * Description:
 * Copy UI components for HostAP configuration into a glist for processing.
 *
 * Return:
 * 0 if update fails.
 * 1 if update succeeds.
 *========================================================================
 */
int 
updateHostAP()
{
    gchar *value;

    // Only save the password if there is something in it.
    if ( (strlen(gtk_entry_get_text(GTK_ENTRY(accessPointPasswordEntry))) > 7) &&
         (strlen(gtk_entry_get_text(GTK_ENTRY(accessPointPasswordEntry))) < 65) )
    {
        value = g_strdup(gtk_entry_get_text(GTK_ENTRY(accessPointPasswordEntry)));
        pncSetHostAPField( PNC_ID_HAP_PW, value );
        g_free(value);
    }
    else
    {
        // Post an error dialog and don't save.
        errorDialog("Password must be between\n8 and 64 characters, inclusive.");
        return 0;
    }

    // Only save the SSID if there is something in it.
    if ( strlen(gtk_entry_get_text(GTK_ENTRY(accessPointSSIDEntry))) > 0 )
    {
        value = g_strdup(gtk_entry_get_text(GTK_ENTRY(accessPointSSIDEntry)));
        pncSetHostAPField( PNC_ID_HAP_SSID, value );
        piboxLogger(LOG_INFO, "updateHostAP: SSID=*%s*\n", value);
        g_free(value);
    }
    else
    {
        // Post an error dialog and don't save.
        errorDialog("SSID is required.");
        return 0;
    }

    // Only save the base network address if there is something in it.
    if ( strlen(gtk_entry_get_text(GTK_ENTRY(hostapNetEntry))) > 0 )
    {
        value = g_strdup(gtk_entry_get_text(GTK_ENTRY(hostapNetEntry)));
        if ( piboxValidIPAddress( value ) )
        {
            g_free(value);
            value = g_strdup(gtk_entry_get_text(GTK_ENTRY(hostapNetEntry)));
            pncSetHostAPField( PNC_ID_HAP_NET, value );
            piboxLogger(LOG_INFO, "updateHostAP: base network address=*%s*\n", value);
            g_free(value);
        }
        else
        {
            // Post an error dialog and don't save.
            g_free(value);
            errorDialog("Invalid base network address.");
            return 0;
        }
    }
    else
    {
        // Post an error dialog and don't save.
        errorDialog("Base network address is required.");
        return 0;
    }
    return 1;
}

/*
 *========================================================================
 * Name:   on_vbox3_realize, on_vbox5_realize
 *
 * Description:
 * Show/hide tabs in the main notebook on startup.
 *========================================================================
 */
void
on_vbox3_realize                       (GtkWidget       *widget,
                                        gpointer         user_data)
{
    // Save the vbox widget (though we don't use it yet).
    wirelessPage = widget;
}

void
on_vbox5_realize                       (GtkWidget       *widget,
                                        gpointer         user_data)
{
    // Save the vbox widget (though we don't use it yet).
    accessPointPage = widget;
}

/*
 *========================================================================
 * Name:   on_wirelessButton_clicked
 *
 * Description:
 * Show/hide tabs in the main notebook when the user selects either the
 * wireless or access point radio buttons.
 *========================================================================
 */
void
on_wirelessButton_clicked              (GtkButton       *button,
                                        gpointer         user_data)
{
    GtkWidget *page = NULL;
    int pageID = -1;

    if ( !isMapped )
        return;

    if( gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( button ) ) )
    {
        // This shows the wireless tab
        pageID = 1;
        page = gtk_notebook_get_nth_page(GTK_NOTEBOOK(mainNotebook), 1);
        gtk_widget_show(page);
        page = gtk_notebook_get_nth_page(GTK_NOTEBOOK(mainNotebook), 2);
        gtk_widget_hide(page);
        netType = PNC_ID_WIRELESS;
    }
    else
    {
        // This shows the access point tab
        pageID = 2;
        page = gtk_notebook_get_nth_page(GTK_NOTEBOOK(mainNotebook), 1);
        gtk_widget_hide(page);
        page = gtk_notebook_get_nth_page(GTK_NOTEBOOK(mainNotebook), 2);
        gtk_widget_show(page);
        netType = PNC_ID_WAP;
    }
    // printf("netType: %d\n", netType);

    // Let the new widget show, so we can change to the appropriate page.
    gtk_main_iteration_do(FALSE);
    gtk_notebook_set_current_page(GTK_NOTEBOOK(mainNotebook), pageID);
}

/*
 *========================================================================
 * Name:   Realize methods for net type buttons.
 *
 * Description:
 * Based on the netType marker file content, set the initial state of the 
 * wireless and access point radio buttons.
 *========================================================================
 */
void
on_wirelessButton_realize              (GtkWidget       *widget,
                                        gpointer         user_data)
{
    if( netType == PNC_ID_WIRELESS )
        gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), TRUE );
    else
        gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), FALSE );
}


void
on_accessPointButton_realize           (GtkWidget       *widget,
                                        gpointer         user_data)
{
    if( netType == PNC_ID_WAP )
        gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), TRUE );
    else
        gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), FALSE );
}


void
on_statusbar1_realize                  (GtkWidget       *widget,
                                        gpointer         user_data)
{
    statusBar = widget;
    statusBarContextID = gtk_statusbar_get_context_id(GTK_STATUSBAR(statusBar), "IPAddress");
}


/*
 *========================================================================
 * Name:   key_press
 * Prototype:  void key_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Handles exiting the application via keystrokes.
 *========================================================================
 */
gboolean 
key_press(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{      
    guint homeKey;

    switch(event->keyval) 
    {
        case GDK_KEY_Q:
        case GDK_KEY_q:
            if (event->state & GDK_CONTROL_MASK)
            {
                piboxLogger(LOG_INFO, "Ctrl-Q key\n");
                gtk_main_quit();
                return(TRUE);
            }
            break;

        case GDK_KEY_Home:
            piboxLogger(LOG_INFO, "Home key\n");
            gtk_main_quit();
            return(TRUE);
            break;

        default:
            homeKey = gdk_keyval_from_name( pncGetHomeKey() );
            if ( event->keyval == homeKey )
            {
                piboxLogger(LOG_INFO, "Keysym configured home key\n");
                gtk_main_quit();
                return(TRUE);
            }
            break;
    }
    return(FALSE);
}

/*
 *========================================================================
 * Name:   on_macAddrText_realize
 * Prototype:  void on_macAddrText_realize(GtkWidget *, gpointer)
 *
 * Description:
 * Save a refernece to the macAddrText widget.
 *========================================================================
 */
void
on_macAddrText_realize                 (GtkWidget       *widget,
                                        gpointer         user_data)
{
    // Save widget for use by other callbacks.
    macAddrText = widget;
}

/*
 *========================================================================
 * Name:   on_ipAddrText_realize
 * Prototype:  void on_ipAddrText_realize(GtkWidget *, gpointer)
 *
 * Description:
 * Save a refernece to the ipAddrText widget.
 *========================================================================
 */
void
on_ipAddrText_realize                 (GtkWidget       *widget,
                                        gpointer         user_data)
{
    // Save widget for use by other callbacks.
    ipAddrText = widget;
}

/*
 *========================================================================
 * Name:   showIPInStatusBar
 * Prototype:  void showIPInStatusBar( void )
 *
 * Description:
 * Sets the current IP address in the status bar for the currently selected
 * interface.
 *
 * Note:
 * This doesn't belong in load - it should be in callbacks.
 *========================================================================
 */
void 
showIPInStatusBar( void )
{
    int          fd;
    struct ifreq ifr;
    char         ipaddr[64];
    char         buffer[64];
    char         *name;
    GtkWidget    *statusBar;
    guint        statusBarContextID;

    // Need to get statusBar info
    statusBar = getStatusBar();
    statusBarContextID = getStatusBarContextID();

    // Clear the status bar.
    gtk_statusbar_pop( GTK_STATUSBAR(statusBar), statusBarContextID );

    // Find out which interface is being displayed
    name = getInterface();
    if ( name == NULL )
    {
        piboxLogger(LOG_INFO, "No interface name available.");
        return;
    }
    piboxLogger(LOG_INFO, "showIP: Interface name %s\n", name);

    // If not found or not enabled, skip it.
    if ( pncGetInterfaceState(name) != 1 )
    {
        g_free(name);
        return;
    }

    // setup a query of the interface
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name, name, IFNAMSIZ-1);

    // query if the interface is up.
    ioctl(fd, SIOCGIFFLAGS, &ifr);
    if ( (ifr.ifr_flags & ( IFF_UP | IFF_RUNNING )) != ( IFF_UP | IFF_RUNNING ) )
    {
        piboxLogger(LOG_INFO,"Interface is not running: %s\n", name);
        g_free(name);
        close(fd);
        return;
    }

    // Grab the IP address.
    ioctl(fd, SIOCGIFADDR, &ifr);
    close(fd);

    memset(ipaddr, 0, 64);
    memset(buffer, 0, 64);

    // Get result 
    sprintf(ipaddr, "%s", inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
    piboxLogger(LOG_INFO, "retrieved ip: %s\n", ipaddr);
    if ( (strcmp( ipaddr, "0.0.0.0" ) == 0) ||
         (strcmp( ipaddr, "255.127.0.0" ) == 0) )
        sprintf(buffer, "IP Address: not set");
    else
        sprintf(buffer, "IP Address: %s", ipaddr);
    piboxLogger(LOG_INFO, "%s ipaddr = %s\n", name, buffer);
    g_free(name);

    // Show IP in the status bar.
    // gtk_statusbar_push( GTK_STATUSBAR(statusBar), statusBarContextID, buffer );

    // Show IP in the text entry field.
    if ( (strcmp( ipaddr, "0.0.0.0" ) == 0) ||
         (strcmp( ipaddr, "255.127.0.0" ) == 0) )
        sprintf(buffer, "Not set");
    else
        sprintf(buffer, "%s", ipaddr);
    gtk_entry_set_text(GTK_ENTRY(ipAddrText), buffer);
}

