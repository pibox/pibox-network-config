/*******************************************************************************
 * pibox-network-config library
 *
 * pnc.c:  Library initialization and get/set functions
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define PNC_C

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <linux/limits.h>
#include <libgen.h>
#include <errno.h>
#include <glib.h>
#include <pibox/log.h>
#include <pibox/utils.h>
#include "pnc.h"
#include "pncPrivate.h"

/*
 * ========================================================================
 * Type definitions and Data structures 
 * =======================================================================
 */

/* Assumption: WIRELESS_CMD returns only a list of wireless device names. */
#define WIRELESS_CMD "iw dev | awk '$1==\"Interface\"{print $2}'"
#define SCAN_CMD  "iwlist %s scan|grep \"\(Channel\"|sed 's/ \\+/ /g'|cut -f5 -d\" \"|sed 's/)//'"
#define BSS_CMD "iw dev %s scan"

#define BSS_S       "BSS"
#define SIGNAL_S    "signal:"
#define SSID_S      "SSID:"
#define DSPARAM_S   "DS Parameter set:"

/* Files have a path and a filename. */
typedef struct _pnc_file {
    int     id;             // Unique id
    char    *name;          // Filename
    char    *path;          // Path where you can find that file.
} PNC_FILE_T;

/* 
 * The initialized set of data files
 * These can be changed inside get functions based on command line 
 * options.
 */
#define FILE_DEFAULT_MAX 9
static PNC_FILE_T file_default[] = {
    {PNC_ID_INTERFACES,    "interfaces",          "/etc/network/"},
    {PNC_ID_WPASUPPLICANT, "wpa_supplicant.conf", "/etc/"},
    {PNC_ID_RESOLV,        "resolv.conf",         "/etc/"},
    {PNC_ID_HOSTAPDCONF,   "hostapd.conf",        "/etc/"},
    {PNC_ID_HOSTAPDPSK,    "hostapd-psk",         "/etc/"},
    {PNC_ID_NETTYPE,       "nettype",             "/etc/network/"},
    {PNC_ID_DHCP,          "dhcpd.conf",          "/etc/"},
    {PNC_ID_KEYSYMS,       "pibox-keysyms",       "/etc/"},
    {PNC_ID_NETIFACES,     "net",                 "/sys/class/"}
};

/*
 * The link list of file paths for configuation files managed by this library.
 */
static GSList  *_pncFiles = NULL;

/* 
 * Keep track of whether we're running in test mode or not.  
 * Save functions will append ".bak" to filenames if we are.
 */
static int testMode = 0;

/* The list of wifi devices */
GSList *wifi_devices = NULL;

/* List of BSS entries */
GSList *bss_list = NULL;

/*
 *========================================================================
 *========================================================================
 * Static functions
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   bss_list_free
 * Prototype:  gint bss_list_free( gpointer )
 *
 * Description:
 * Free the list of BSS entries.
 *========================================================================
 */
static void 
bss_list_free( gpointer user_data )
{
    BSS_T   *bss = (BSS_T *)user_data;
    if ( bss != NULL )
    {
        g_free( bss->ssid );
        g_free( bss );
    }
}

/*
 *========================================================================
 * Name:   wifi_devices_free
 * Prototype:  gint wifi_devices_free( gpointer )
 *
 * Description:
 * Free the list of wifi devices.
 *========================================================================
 */
static void 
wifi_devices_free( gpointer user_data )
{
    if ( user_data != NULL )
        g_free(user_data);
}

/*
 *========================================================================
 * Name:   findFilename
 * Prototype:  gint findFilename( gconstpointer )
 *
 * Description:
 * Find the filename associated with the specified id.
 *========================================================================
 */
static gint 
findFilename( gconstpointer item, gconstpointer user_data )
{
    PNC_FILE_T *entry = (PNC_FILE_T *)item;
    gint id = GPOINTER_TO_INT(user_data);

    if ( entry->id == id )
        return 0;
    return 1;
}

/*
 *========================================================================
 *========================================================================
 * Library FUNCTIONS (not part of API)
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   getTestMode
 * Prototype:  gint getTestMode( void )
 *
 * Description:
 * Returns the test mode state.  If set to 1, then we're running in test mode.
 * Test mode is set by the application in its call to pncInit().
 *========================================================================
 */
gint 
getTestMode( void )
{
    return testMode;
}

/*
 *========================================================================
 * Name:   printInterface
 * Prototype:  void printInterface( void )
 *
 * Description:
 * g_slist_foreach() callback.  Print the interfaces read from file.
 *========================================================================
 */
void 
printInterface( gpointer item, gpointer user_data )
{
    PNC_INTERFACE_T *iface = (PNC_INTERFACE_T *)item;
    piboxLogger(LOG_INFO, "%s: Printing interface: \n", __FUNCTION__);
    if ( iface->name        != NULL ) piboxLogger(LOG_INFO, "%s \n", iface->name);
    if ( iface->macAddress  != NULL ) piboxLogger(LOG_INFO, "    MAC    : %s\n", iface->macAddress);
    if ( iface->networkType != NULL ) piboxLogger(LOG_INFO, "    Network: %s\n", iface->networkType);
    if ( iface->addressType != NULL ) piboxLogger(LOG_INFO, "    Type   : %s\n", iface->addressType);
    if ( iface->address     != NULL ) piboxLogger(LOG_INFO, "    Address: %s\n", iface->address);
    if ( iface->netmask     != NULL ) piboxLogger(LOG_INFO, "    Netmask: %s\n", iface->netmask);
    if ( iface->gateway     != NULL ) piboxLogger(LOG_INFO, "    Gateway: %s\n", iface->gateway);
}

/*
 *========================================================================
 *========================================================================
 * Public API functions
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   pncInit
 * Prototype:  void pncInit( int testmode, char *datadir )
 *
 * Description:
 * Initializes list of data files.  These can be overridden with associated
 * set functions.
 *
 * Arguments:
 * testmode    0: no, 1: yes (use test data directory instead of real directory)
 * datadir     If not NULL, specifies the test directory to use. 
 *             Defaults to "data" under current directory.
 *========================================================================
 */
void 
pncInit( int testmode, gchar *datadir )
{
    int         idx;
    PNC_FILE_T  *pncfile;
    char        buf[PATH_MAX];
    char        *testdir;

    piboxLogger(LOG_INFO, "Entered pncInit\n");
    testMode = testmode;
    if ( datadir != NULL )
        testdir = datadir;
    else
        testdir = "data/";

    for(idx=0; idx<FILE_DEFAULT_MAX; idx++)
    {
        pncfile = g_new(PNC_FILE_T, 1);
        pncfile->id = file_default[idx].id;
        pncfile->name = file_default[idx].name;

        if ( testmode )
            pncfile->path = g_strdup(testdir);
        else
            pncfile->path = g_strdup(file_default[idx].path);

        _pncFiles = g_slist_append(_pncFiles, pncfile);
    }
}

/*
 *========================================================================
 * Name:   pncGetFilename
 * Prototype:  gchar *pncGetFilename( gint id )
 *
 * Description:
 * Retrieve the fully qualified path and filename for the specified config
 * file.
 *
 * Arguments:
 * id          The id of the path/filename to retrieve.  See pncLibNet.h
 *
 * Returns:
 * Pointer to string containing the fully qualified path to the file.
 * Null if the id is invalid.
 * Caller is responsible for freeing this pointer.
 *========================================================================
 */
gchar * 
pncGetFilename( gint id )
{
    GSList     *item;
    PNC_FILE_T *entry;
    char       buf[PATH_MAX];

    item = g_slist_find_custom(_pncFiles, GINT_TO_POINTER(id), (GCompareFunc)findFilename);
    if ( item != NULL )
    {
        entry = (PNC_FILE_T *)item->data;
        sprintf(buf, "%s%s", entry->path, entry->name);
        return g_strdup(buf);
    }
    return NULL;
}

/*
 *========================================================================
 * Name:   pncSetFilename
 * Prototype:  gint pncSetFilename( gint id, gchar *path )
 *
 * Description:
 * Set the path for one of the supported configuration files.
 * This can be used to override the default configurations.
 *
 * Arguments:
 * id          The id of the path/filename to retrieve.  See pncLibNet.h
 * path        The updated, fully qualified, path.
 *
 * Returns:
 * 0 on success.
 * 1 if the path could not be updated.
 *========================================================================
 */
gint
pncSetFilename( gint id, gchar *path )
{
    gchar       *ptr;
    GSList      *item;
    PNC_FILE_T  *entry;
    PNC_FILE_T  *pncfile;

    item = g_slist_find_custom(_pncFiles, GINT_TO_POINTER(id), (GCompareFunc)findFilename);
    if ( item != NULL )
    {
        if ( item->data != NULL )
        {
            entry = item->data;
            pncfile = g_new(PNC_FILE_T, 1);
            pncfile->id = entry->id;
            g_free(entry);
            item->data = pncfile;

            ptr = g_strdup(path);
            pncfile->path = g_strdup(dirname(ptr));
            strcat(pncfile->path,"/");
            g_free(ptr);

            ptr = g_strdup(path);
            pncfile->name = g_strdup(basename(ptr));
            g_free(ptr);

            return 0;
        }
    }
    return 1;
}

/*
 *========================================================================
 * Name:   pncPrintFilename
 * Prototype:  void pncPrintFilename( gint id )
 *
 * Description:
 * Print the configuration for the specified configuration filename.
 *
 * Arguments:
 * id          The id of the path/filename to retrieve.  See pncLibNet.h
 *========================================================================
 */
void
pncPrintFilename( gint id )
{
    gchar       *ptr;
    GSList      *item;
    PNC_FILE_T  *entry;
    PNC_FILE_T  *pncfile;
    char        buf[PATH_MAX];

    item = g_slist_find_custom(_pncFiles, GINT_TO_POINTER(id), (GCompareFunc)findFilename);
    if ( item != NULL )
    {
        entry = item->data;
        sprintf(buf, "%s%s\n", entry->path, entry->name);
        fprintf(stderr, buf);
    }
}

/*
 *========================================================================
 * Name:   pncPrintFilenames
 * Prototype:  void pncPrintFilenames()
 *
 * Description:
 * Print the configuration for all the specified configuration filenames.
 *========================================================================
 */
void
pncPrintFilenames()
{
    int idx;
    for ( idx=0; idx<FILE_DEFAULT_MAX; idx++)
        pncPrintFilename(idx);
}

/*
 *========================================================================
 * Name:   pncPrintInterfaces
 * Prototype:  void pncPrintInterfaces( void )
 *
 * Description:
 * Print the list of interfaces currently configured.
 *========================================================================
 */
void
pncPrintInterfaces()
{
    GSList *interfaces  = NULL;
    interfaces = pncGetInterfaces();
    g_slist_foreach(interfaces, printInterface, NULL);
}

/*
 *========================================================================
 * Name:   pncGetInterfaceField
 * Prototype:  gchar *pncGetInterfaceField( gchar *name, int id )
 *
 * Description:
 * Retrieve the specified field from an interface object via GSList item.
 *
 * Arguments:
 * name        The name of the interface to update.
 * id          Field identifier from pncLibNet.h.
 *
 * Returns:
 * A gchar buffer or NULL if the field is empty or the id is invalid.
 * Caller is responsible for freeing the returned buffer.
 *========================================================================
 */
gchar *
pncGetInterfaceField( gchar *name, int id )
{
    GSList *interfaces  = NULL;
    GSList *item        = NULL;
    PNC_INTERFACE_T *iface  = NULL;
    gchar *ptr = NULL;

    interfaces = pncGetInterfaces();
    if ( interfaces == NULL )
        return NULL;
    item = g_slist_find_custom(interfaces, name, (GCompareFunc)pncFindInterface);
    if ( item == NULL )
        return NULL;

    iface = (PNC_INTERFACE_T *)item->data;
    switch (id )
    {
        case PNC_ID_IF_NAME:     ptr = iface->name; break;
        case PNC_ID_IF_NETTYPE:  ptr = iface->networkType; break;
        case PNC_ID_IF_MAC:      ptr = iface->macAddress; break;
        case PNC_ID_IF_ADDRTYPE: ptr = iface->addressType; break;
        case PNC_ID_IF_ADDR:     ptr = iface->address; break;
        case PNC_ID_IF_MASK:     ptr = iface->netmask; break;
        case PNC_ID_IF_GATEWAY:  ptr = iface->gateway; break;
    }
    if (ptr == NULL)
        return NULL;
    else
        return g_strdup(ptr);
}

/*
 *========================================================================
 * Name:   pncSetInterfaceField
 * Prototype:  gchar *pncSetInterfaceField( gchar *, int id, const gchar *value )
 *
 * Description:
 * Set the specified field from an interface object via GSList item.
 * If the specified interface doesn't exist it is created before the field is updated.
 *
 * Arguments:
 * name        The name of the interface to update.
 * id          Field identifier from pncLibNet.h.
 * value       The new value.  May be NULL, which clears the field.
 *
 * Returns:
 * A gchar buffer or NULL if the field is empty or the id is invalid.
 * Caller is responsible for freeing the returned buffer.
 *========================================================================
 */
void
pncSetInterfaceField( gchar *name, int id, const gchar *value )
{
    GSList *interfaces  = NULL;
    GSList *item        = NULL;
    PNC_INTERFACE_T *iface  = NULL;
    gchar  *ptr;

    if ( (value == NULL) || (strlen(value) == 0) )
    {
        piboxLogger(LOG_ERROR, "%s: Missing value for field.\n", __FUNCTION__);
        return;
    }

    interfaces = pncGetInterfaces();
    if ( interfaces == NULL )
    {
        piboxLogger(LOG_TRACE1, "%s(%d): Calling pncNewInterface\n", __FUNCTION__, __LINE__);
        pncNewInterface(name);
        interfaces = pncGetInterfaces();
    }
    item = g_slist_find_custom(interfaces, name, (GCompareFunc)pncFindInterface);
    if ( item == NULL )
    {
        piboxLogger(LOG_TRACE1, "%s(%d): Calling pncNewInterface\n", __FUNCTION__, __LINE__);
        pncNewInterface(name);
        item = g_slist_find_custom(interfaces, name, (GCompareFunc)pncFindInterface);
    }
    iface = (PNC_INTERFACE_T *)item->data;
    switch (id )
    {
        case PNC_ID_IF_NAME:     
            if (iface->name!=NULL) g_free(iface->name); 
            iface->name = g_strdup(value);
            break;
        case PNC_ID_IF_NETTYPE:  
            if (iface->networkType!=NULL) g_free(iface->networkType); 
            iface->networkType = g_strdup(value);
            break;
        case PNC_ID_IF_MAC:      
            if (iface->macAddress!=NULL) g_free(iface->macAddress); 
            iface->macAddress = g_strdup(value);
            break;
        case PNC_ID_IF_ADDRTYPE: 
            if (iface->addressType!=NULL) g_free(iface->addressType); 
            iface->addressType = g_strdup(value);
            ptr = iface->addressType;
            for( ; *ptr; ++ptr)
                *ptr = tolower(*ptr);
            break;
        case PNC_ID_IF_ADDR:     
            if (iface->address!=NULL) g_free(iface->address); 
            iface->address = g_strdup(value);
            break;
        case PNC_ID_IF_MASK:     
            if (iface->netmask!=NULL) g_free(iface->netmask); 
            iface->netmask = g_strdup(value);
            break;
        case PNC_ID_IF_GATEWAY:  
            if (iface->gateway!=NULL) g_free(iface->gateway); 
            iface->gateway = g_strdup(value);
            break;
    }
}

/*
 *========================================================================
 * Name:   pncGetInterfaceState
 * Prototype:  gint pncGetInterfaceState( gchar * )
 *
 * Description:
 * Retrieve the state of the specified interface.
 *
 * Arguments:
 * name        The name of the interface.
 *
 * Returns:
 * 0 if the interface is disabled or 1 if it is enabled or -1 if no such
 * interface exists.
 *========================================================================
 */
gint
pncGetInterfaceState( gchar *name )
{
    GSList *interfaces  = NULL;
    GSList *item        = NULL;
    PNC_INTERFACE_T *iface  = NULL;

    interfaces = pncGetInterfaces();
    item = g_slist_find_custom(interfaces, name, (GCompareFunc)pncFindInterface);
    if ( item == NULL )
        return -1;
    iface = (PNC_INTERFACE_T *)item->data;
    return (iface->enabled);
}

/*
 *========================================================================
 * Name:   pncSetInterfaceState
 * Prototype:  void pncSetInterfaceState( gchar *name, int state )
 *
 * Description:
 * Retrieve the state of the specified interface.
 * 
 * Arguments:
 * name        Interface name
 * state       0 for disabled, 1 for enabled.
 * 
 * Notes:
 * Does not change state if state is neither 0 nor 1.
 *========================================================================
 */
void
pncSetInterfaceState( gchar *name, int state )
{
    GSList *interfaces      = NULL;
    GSList *item            = NULL;
    PNC_INTERFACE_T *iface  = NULL;

    interfaces = pncGetInterfaces();
    item = g_slist_find_custom(interfaces, name, (GCompareFunc)pncFindInterface);
    if ( item == NULL )
        return;
    if ( (state < 0) || (state > 1) )
        return;

    iface = (PNC_INTERFACE_T *)item->data;
    iface->enabled = state;
}

/*
 *========================================================================
 * Name:   pncGetWPAField
 * Prototype:  gchar *pncGetWPAField( GSList *, int id )
 *
 * Description:
 * Retrieve the specified field from a WPA object via GSList item.
 *
 * Arguments:
 * item        An entry from pncGetWPA.
 * id          Field identifier from pncLibNet.h.
 *
 * Returns:
 * A gchar buffer or NULL if the field is empty or the id is invalid.
 * Caller is responsible for freeing the returned buffer.
 *========================================================================
 */
gchar *
pncGetWPAField( int id )
{
    GSList *list, *item;

    list = pncGetWPA();
    item = g_slist_nth(list, id);
    if ( item->data != NULL )
        return g_strdup(item->data);
    else
        return NULL;
}

/*
 *========================================================================
 * Name:   pncSetWPAField
 * Prototype:  gchar *pncGetWPAField( int id, gchar *value )
 *
 * Description:
 * Replace the specified field in the WPA object with the specified value.
 *
 * Arguments:
 * id          Field identifier from pncLibNet.h.
 * value       The new value for that field.
 *========================================================================
 */
void
pncSetWPAField( int id, gchar *value )
{
    GSList *list, *item;

    list = pncGetWPA();
    item = g_slist_nth(list, id);
    if ( item->data != NULL )
        g_free(item->data);
    item->data = g_strdup(value);
}

/*
 *========================================================================
 * Name:   pncGetHostAPField
 * Prototype:  gchar *pncGetHostAPField( int id )
 *
 * Description:
 * Retrieve the specified field from a HAP object via GSList item.
 *
 * Arguments:
 * id          Field identifier from pncLibNet.h.
 *
 * Returns:
 * A gchar buffer or NULL if the field is empty or the id is invalid.
 * Caller is responsible for freeing the returned buffer.
 *========================================================================
 */
gchar *
pncGetHostAPField( int id )
{
    GSList *list, *item;

    list = pncGetHostAP();
    item = g_slist_nth(list, id);
    if ( item == NULL )
        return NULL;
    if ( item->data != NULL )
        return g_strdup(item->data);
    else
        return NULL;
}

/*
 *========================================================================
 * Name:   pncSetHostAPField
 * Prototype:  gchar *pncSetHostAPField( int id, gchar *value )
 *
 * Description:
 * Replace the specified field in the HostAP object with the specified value.
 *
 * Arguments:
 * id          Field identifier from pncLibNet.h.
 * value       The new value for that field.
 *========================================================================
 */
void
pncSetHostAPField( int id, gchar *value )
{
    GSList *list, *item;

    list = pncGetHostAP();
    item = g_slist_nth(list, id);
    if ( item == NULL )
        return;
    if ( item->data != NULL )
        g_free(item->data);
    if ( value != NULL )
        item->data = g_strdup(value);
    else
        item->data = g_strdup("");
}

/*
 *========================================================================
 * Name:   pncGenWifiList
 * Prototype:  gchar *pncGenWifiList()
 *
 * Description:
 * Generate a list of available wifi devices.
 *
 * Arguments:
 * id          Field identifier from pncLibNet.h.
 * value       The new value for that field.
 *========================================================================
 */
void
pncGenWifiList( void )
{
    FILE    *pd;
    char    buf[256];
    int     found=0;

    if ( wifi_devices != NULL )
    {
        g_slist_free_full (wifi_devices, wifi_devices_free);
        wifi_devices = NULL;
    }

    /* Assumption: WIRELESS_CMD returns only a list of wireless device names. */
    sprintf(buf, WIRELESS_CMD);
    piboxLogger(LOG_INFO, "Command to retrieve wireless interface names: %s\n", buf);
    pd = popen(buf, "r");
    if ( pd == NULL )
    {
        piboxLogger(LOG_INFO, "Failed to run command: reason: %s\n", 
                strerror(errno));
        return;
    }
    while (fgets(buf, 255, pd) != NULL )
    { 
        piboxStripNewline(buf);
        found=1;
        piboxLogger(LOG_INFO, "Found wireless device: %s\n", buf);
        wifi_devices = g_slist_append(wifi_devices, strdup(buf));
    }
    pclose(pd);

    if ( !found )
    {
        piboxLogger(LOG_INFO, "No wireless devices found.\n");
    }
}

/*
 *========================================================================
 * Name:   pncGetWifiList
 * Prototype:  GSList *pncGetWifiList( void )
 *
 * Description:
 * Returns the list of wifi devices found on the system as a set of GSList
 * items.
 *
 * Notes:
 * Caller should copy the entries and not modify them.
 *========================================================================
 */
GSList *
pncGetWifiList( void )
{
    return ( wifi_devices );
}

/*
 *========================================================================
 * Name:   pncGetChannelList
 * Prototype:  GHashTable *pncGetChannelList( void )
 *
 * Description:
 * Scans the local network for which channels are in use.
 *
 * Returns:
 * A pointer to a glib hash table.
 *
 * Notes:
 * Uses the first device found in wifi_devices (see pncGetWifiList).
 * Caller should copy the entries and not modify them.
 *========================================================================
 */
GHashTable *
pncGetChannelList( void )
{
    char        buf[256];
    char        *ch;
    FILE        *pd;
    GHashTable  *hash;
    int         key;
    int         value;
    gpointer    ptr;

    /* Look for wifi devices. Must have called pncGetWifiList() first. */
    if ( wifi_devices == NULL )
    {
        return(NULL);
    }

    /* Create a hash for the channels.  We'll count each entry. */
    hash = g_hash_table_new(NULL, NULL);

    /* Run the command to get the set of channels that are currently in use. */
    sprintf(buf, SCAN_CMD, (char *)wifi_devices->data);
    piboxLogger(LOG_INFO, "Scan command: %s\n", buf);
    pd = popen(buf, "r");
    if ( pd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open cmd (%s) for reading: reason: %s\n", 
                buf, strerror(errno));
        return(NULL);
    }

    /* Iterate over the list of channels and count how many wifi networks are using each. */
    while (fgets(buf, 64, pd) != NULL )
    { 
        piboxLogger(LOG_INFO, "Found channel: %s\n", buf);
        key = atoi(buf);

        /* Lookup value.  If it's not there, the return value is zero. */
        piboxLogger(LOG_INFO, "Lookup channel: hash 0x%08x, key %d\n", hash, key);
        ptr = g_hash_table_lookup(hash, GINT_TO_POINTER(key));
        value = GPOINTER_TO_INT( ptr );

        /* Increment counter. */
        piboxLogger(LOG_INFO, "Increment channel count.\n");
        value++;

        /* Insert the new value */
        piboxLogger(LOG_INFO, "Insert new channel count.\n");
        g_hash_table_insert(hash, GINT_TO_POINTER(key), GINT_TO_POINTER(value));
    }
    pclose(pd);
    piboxLogger(LOG_INFO, "Done scanning channels; hash size %d\n", g_hash_table_size(hash));

    /* Return the hash table of channels and counts */
    return(hash);
}

/*
 *========================================================================
 * Name:   pncGenBSSList
 * Prototype:  void pncGenBSSList( void )
 *
 * Description:
 * Scans the local network for which SSIDs, signal strength and channel info.
 *
 * Notes:
 * Uses the first device found in wifi_devices (see pncGetWifiList).
 * Use pncGetBSSList() to get access to the list.
 *========================================================================
 */
void
pncGenBSSList( void )
{
    char        buf[512];
    FILE        *pd;
    char        *ptr;
    char        *token;
    char        *save;
    BSS_T       *entry = NULL;

    piboxLogger(LOG_INFO, "Entered\n");

    /* Look for wifi devices. Must have called pncGetWifiList() first. */
    if ( wifi_devices == NULL )
    {
        return;
    }

    /* If we've been called previously, clean up first. */
    if ( bss_list != NULL )
    {
        /* free up the existing list. */
        g_slist_free_full (bss_list, bss_list_free);
        bss_list = NULL;
    }

    /* Run the command (on the first wifi device) to get the set of BSS entries. */
    sprintf(buf, BSS_CMD, (char *)wifi_devices->data);
    piboxLogger(LOG_INFO, "BSS command: %s\n", buf);
    pd = popen(buf, "r");
    if ( pd == NULL )
    {
        piboxLogger(LOG_INFO, "Failed to open cmd (%s) for reading: reason: %s\n", 
                buf, strerror(errno));
        return;
    }

    /* 
     * Iterate over the set of BSS entries and extract interesting information.
     * Currently, that is:
     * 1. signal strength
     * 2. SSID
     * 3. Channel
     */
    while (fgets(buf, 128, pd) != NULL )
    { 
        if ( strncmp(buf, BSS_S, strlen(BSS_S)) == 0 )
        {
            if (entry != NULL )
            {
                /* Add to gslist */
                piboxLogger(LOG_TRACE1, 
                        "Appending BSS entry to list:\n"
                        "ssid: %s\n"
                        "signal: %d\n"
                        "channel: %d\n",
                        entry->ssid, entry->signal, entry->channel
                        );
                bss_list = g_slist_append(bss_list, entry);

            }
            piboxLogger(LOG_TRACE1, "Allocating BSS entry\n");
            entry = (BSS_T *)calloc(1,sizeof(BSS_T));
        }
        if ( entry == NULL )
        {
            /* We're not in an entry yet. */
            continue;
        }

        /* Strip leading whitespace */
        ptr = piboxTrim(buf);
        piboxStripNewline(ptr);
        piboxLogger(LOG_TRACE1, "BSS line: %s\n", ptr);

        if ( strncmp(ptr, SIGNAL_S, strlen(SIGNAL_S)) == 0 )
        {
            token = strtok_r(ptr, " ", &save);
            token = strtok_r(NULL, " ", &save);
            entry->signal = atoi(token);
            // entry->percent = 2 * (entry->signal + 100);
            piboxLogger(LOG_TRACE1, "Adding signal to BSS entry: (%s) %d, %d\n", 
                    token, atoi(token));
            continue;
        }
        else if ( strncmp(ptr, SSID_S, strlen(SSID_S)) == 0 )
        {
            token = strtok_r(ptr, " ", &save);
            token = strtok_r(NULL, " ", &save);
            if ( token == NULL )
            {
                piboxLogger(LOG_TRACE1, "Adding SSID to BSS entry: Unknown\n");
                entry->ssid = strdup("Unknown");
            }
            else
            {
                piboxLogger(LOG_TRACE1, "Adding SSID to BSS entry: %s\n", token);
                entry->ssid = strdup(token);
            }
            continue;
        }
        else if ( strncmp(ptr, DSPARAM_S, strlen(DSPARAM_S)) == 0 )
        {
            token = strtok_r(ptr, " ", &save);
            token = strtok_r(NULL, " ", &save);
            token = strtok_r(NULL, " ", &save);
            token = strtok_r(NULL, " ", &save);
            token = strtok_r(NULL, " ", &save);
            piboxLogger(LOG_TRACE1, "Adding channel to BSS entry: %d\n", atoi(token));
            entry->channel = atoi(token);
            continue;
        }
    }
    pclose(pd);

    /* Add the last entry - it wasn't added in the loop. */
    if ( entry != NULL )
    {
        piboxLogger(LOG_TRACE1, "Appending last BSS entry to list\n");
        bss_list = g_slist_append(bss_list, entry);
    }

    /* Return the hash table of channels and counts */
    return;
}

/*
 *========================================================================
 * Name:   pncGetBSSList
 * Prototype:  GSList *pncGetBSSList( void )
 *
 * Description:
 * Returns the list of BSS entries found on the system as a set of GSList
 * items.
 *
 * Notes:
 * Caller should copy the entries and not modify them.
 *========================================================================
 */
GSList *
pncGetBSSList( void )
{
    piboxLogger(LOG_INFO, "Entered\n");
    return ( bss_list );
}

