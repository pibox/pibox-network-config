/*
 * This file is no longer compatible with GLADE.
 * All GLADE elements are slowly being removed.
 */
#define INTERACE_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"
#include "cli.h"
#include "wifiscanwidget.h"

GtkWidget *wifiscan;

/*
#define GLADE_HOOKUP_OBJECT(component,widget,name) \
  g_object_set_data_full (G_OBJECT (component), name, \
    gtk_widget_ref (widget), (GDestroyNotify) gtk_widget_unref)

#define GLADE_HOOKUP_OBJECT_NO_REF(component,widget,name) \
  g_object_set_data (G_OBJECT (component), name, widget)
*/

GtkWidget*
create_window1 (void)
{
  GtkWidget *mainVbox;
  GtkWidget *mainHbox;
  GtkWidget *window1;
  GtkWidget *vbox1;
  GtkWidget *menubar1;
  GtkWidget *menuitem4;
  GtkWidget *menuitem4_menu;
  GtkWidget *open1;
  GtkWidget *save1;
  GtkWidget *save_as1;
  GtkWidget *restart1;
  GtkWidget *separatormenuitem1;
  GtkWidget *quit1;
  GtkWidget *menuitem7;
  GtkWidget *menuitem7_menu;
  GtkWidget *about1;
  GtkWidget *notebook1;
  GtkWidget *vbox2;
  GtkWidget *table1;
  GtkWidget *interfaceLabel;
  GtkWidget *addressTypeLabel;
  GtkWidget *addressTypeComboBox;
  GtkWidget *interfaceComboBox;
  GtkWidget *label18;
  GtkWidget *label;
  GtkWidget *macAddrText;
  GtkWidget *ipAddrText;
  GtkWidget *hbox4;
  GtkWidget *radiobutton1;
  GSList *radiobutton1_group = NULL;
  GtkWidget *radiobutton2;
  GtkWidget *notebook2;
  GtkWidget *empty_notebook_page;
  GtkWidget *dhcpTabLabel;
  GtkWidget *table2;
  GtkWidget *label6;
  GtkWidget *label5;
  GtkWidget *label7;
  GtkWidget *label8;
  GtkWidget *label9;
  GtkWidget *label10;
  GtkWidget *ipAddressText;
  GtkWidget *netmaskText;
  GtkWidget *gatewayText;
  GtkWidget *dns1Text;
  GtkWidget *dns2;
  GtkWidget *dns3Text;
  GtkWidget *staticTabLabel;
  GtkWidget *ipv4TabLabel;
  GtkWidget *vbox3;
  GtkWidget *hbox3;
  GtkWidget *label17;
  GtkWidget *entry11;
  GtkWidget *hbox2;
  GtkWidget *securityLabel;
  GtkWidget *securityComboBox;
  GtkWidget *notebook3;
  GtkWidget *table4;
  GtkWidget *passwordLabel;
  GtkWidget *passwordTextEntry;
  GtkWidget *showPasswordButton;
  GtkWidget *wpaPersonalTabLabel;
  GtkWidget *wirelessTabLabel;
  GtkWidget *vbox5;
  GtkWidget *hbox5;
  GtkWidget *hbox5b;
  GtkWidget *ssidLabel;
  GtkWidget *accessPointSSIDEntry;
  GtkWidget *hostapNetLabel;
  GtkWidget *hostapNetEntry;
  GtkWidget *hbox6;
  GtkWidget *channelLabel;
  GtkWidget *combobox1;
  GtkWidget *table5;
  GtkWidget *label22;
  GtkWidget *accessPointPasswordEntry;
  GtkWidget *accessPointShowPasswordButton;
  GtkWidget *label19;
  GtkWidget *hbox1;
  GtkWidget *statusbar1;
  GtkWidget *wirelessButton;
  GSList *wirelessButton_group = NULL;
  GtkWidget *accessPointButton;
  GtkWidget *saveButton;
  GtkWidget *restartButton;
  GtkWidget *homeButton;
  GtkAccelGroup *accel_group;

  accel_group = gtk_accel_group_new ();

  window1 = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window1), _("Network Configuration"));
  GTK_WIDGET_SET_FLAGS(window1, GTK_CAN_FOCUS );
  gtk_widget_add_events(window1, GDK_KEY_PRESS_MASK);
  g_signal_connect(G_OBJECT(window1),
                "key_press_event",
                G_CALLBACK(key_press),
                NULL);

  mainVbox = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (mainVbox);
  gtk_container_add (GTK_CONTAINER (window1), mainVbox);

  menubar1 = gtk_menu_bar_new ();
  gtk_widget_show (menubar1);
  if ( !isCLIFlagSet( CLI_EMBEDDED ) )
  {
      gtk_box_pack_start (GTK_BOX (mainVbox), menubar1, FALSE, FALSE, 0);
  }

  mainHbox = gtk_hbox_new (FALSE, 0);
  gtk_widget_show (mainHbox);
  gtk_container_add (GTK_CONTAINER (mainVbox), mainHbox);

  vbox1 = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox1);
  gtk_container_add (GTK_CONTAINER (mainHbox), vbox1);

  menuitem4 = gtk_menu_item_new_with_mnemonic (_("_File"));
  gtk_widget_show (menuitem4);
  gtk_container_add (GTK_CONTAINER (menubar1), menuitem4);

  menuitem4_menu = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem4), menuitem4_menu);

  open1 = gtk_image_menu_item_new_from_stock ("gtk-open", accel_group);
  gtk_widget_show (open1);
  gtk_container_add (GTK_CONTAINER (menuitem4_menu), open1);

  save1 = gtk_image_menu_item_new_from_stock ("gtk-save", accel_group);
  gtk_widget_show (save1);
  gtk_container_add (GTK_CONTAINER (menuitem4_menu), save1);

  save_as1 = gtk_image_menu_item_new_from_stock ("gtk-save-as", accel_group);
  gtk_widget_show (save_as1);
  gtk_container_add (GTK_CONTAINER (menuitem4_menu), save_as1);

  restart1 = gtk_menu_item_new_with_mnemonic (_("Restart"));
  gtk_widget_show (restart1);
  gtk_container_add (GTK_CONTAINER (menuitem4_menu), restart1);

  separatormenuitem1 = gtk_separator_menu_item_new ();
  gtk_widget_show (separatormenuitem1);
  gtk_container_add (GTK_CONTAINER (menuitem4_menu), separatormenuitem1);
  gtk_widget_set_sensitive (separatormenuitem1, FALSE);

  quit1 = gtk_image_menu_item_new_from_stock ("gtk-quit", accel_group);
  gtk_widget_show (quit1);
  gtk_container_add (GTK_CONTAINER (menuitem4_menu), quit1);

  menuitem7 = gtk_menu_item_new_with_mnemonic (_("_Help"));
  gtk_widget_show (menuitem7);
  gtk_container_add (GTK_CONTAINER (menubar1), menuitem7);

  menuitem7_menu = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem7), menuitem7_menu);

  about1 = gtk_menu_item_new_with_mnemonic (_("_About"));
  gtk_widget_show (about1);
  gtk_container_add (GTK_CONTAINER (menuitem7_menu), about1);

  notebook1 = gtk_notebook_new ();
  gtk_widget_show (notebook1);
  gtk_box_pack_start (GTK_BOX (vbox1), notebook1, TRUE, TRUE, 0);

  vbox2 = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox2);
  gtk_container_add (GTK_CONTAINER (notebook1), vbox2);

    /*
     * Table with Interface, Address Type, Enable options and MAC address.
     */
    table1 = gtk_table_new (4, 2, TRUE);
    gtk_widget_show (table1);
    gtk_box_pack_start (GTK_BOX (vbox2), table1, TRUE, TRUE, 0);
    gtk_container_set_border_width (GTK_CONTAINER (table1), 10);
    gtk_table_set_col_spacings (GTK_TABLE (table1), 15);

    /*
     * Interface (eth0, wlan0, etc.)
     */
    interfaceLabel = gtk_label_new (_("Interface"));
    gtk_widget_show (interfaceLabel);
    gtk_table_attach (GTK_TABLE (table1), interfaceLabel, 0, 1, 0, 1,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
    gtk_misc_set_alignment (GTK_MISC (interfaceLabel), 0, 0.5);

    interfaceComboBox = gtk_combo_box_new_text ();
    gtk_widget_show (interfaceComboBox);
    gtk_table_attach (GTK_TABLE (table1), interfaceComboBox, 1, 2, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);

    /*
     * Address Type (DHCP or Static)
     */
    addressTypeLabel = gtk_label_new (_("Address Type"));
    gtk_widget_show (addressTypeLabel);
    gtk_table_attach (GTK_TABLE (table1), addressTypeLabel, 0, 1, 1, 2,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
    gtk_misc_set_alignment (GTK_MISC (addressTypeLabel), 0, 0.5);

    addressTypeComboBox = gtk_combo_box_new_text ();
    gtk_widget_show (addressTypeComboBox);
    gtk_table_attach (GTK_TABLE (table1), addressTypeComboBox, 1, 2, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);
    gtk_combo_box_append_text (GTK_COMBO_BOX (addressTypeComboBox), _("DHCP"));
    gtk_combo_box_append_text (GTK_COMBO_BOX (addressTypeComboBox), _("Static"));

    /*
     * Enable interface (Yes or No)
     */
    label18 = gtk_label_new (_("Enable"));
    gtk_widget_show (label18);
    gtk_table_attach (GTK_TABLE (table1), label18, 0, 1, 2, 3,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
    gtk_label_set_justify (GTK_LABEL (label18), GTK_JUSTIFY_CENTER);
    gtk_misc_set_alignment (GTK_MISC (label18), 0, 0.5);

    hbox4 = gtk_hbox_new (FALSE, 15);
    gtk_widget_show (hbox4);
    gtk_table_attach (GTK_TABLE (table1), hbox4, 1, 2, 2, 3,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);

    radiobutton1 = gtk_radio_button_new_with_mnemonic (NULL, _("Yes"));
    gtk_widget_show (radiobutton1);
    gtk_box_pack_start (GTK_BOX (hbox4), radiobutton1, FALSE, FALSE, 0);
    gtk_radio_button_set_group (GTK_RADIO_BUTTON (radiobutton1), radiobutton1_group);
    radiobutton1_group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (radiobutton1));
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (radiobutton1), TRUE);

    radiobutton2 = gtk_radio_button_new_with_mnemonic (NULL, _("No"));
    gtk_widget_show (radiobutton2);
    gtk_box_pack_start (GTK_BOX (hbox4), radiobutton2, FALSE, FALSE, 0);
    gtk_radio_button_set_group (GTK_RADIO_BUTTON (radiobutton2), radiobutton1_group);
    radiobutton1_group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (radiobutton2));

    /*
     * Mac address for selected interface.
     */
    label = gtk_label_new (_("MAC Address"));
    gtk_widget_show (label);
    gtk_table_attach (GTK_TABLE (table1), label, 0, 1, 3, 4,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
    macAddrText = gtk_entry_new ();
    gtk_widget_show (macAddrText);
    gtk_table_attach (GTK_TABLE (table1), macAddrText, 1, 2, 3, 4,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
    gtk_entry_set_has_frame ( GTK_ENTRY(macAddrText), FALSE);
    gtk_editable_set_editable ( GTK_EDITABLE(macAddrText), FALSE);
    GTK_WIDGET_UNSET_FLAGS(macAddrText, GTK_CAN_FOCUS);
    g_signal_connect ((gpointer) macAddrText, "realize",
                    G_CALLBACK (on_macAddrText_realize),
                    NULL);

    /*
     * IP address for selected interface.
     */
    label = gtk_label_new (_("IP Address"));
    gtk_widget_show (label);
    gtk_table_attach (GTK_TABLE (table1), label, 0, 1, 4, 5,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
    ipAddrText = gtk_entry_new ();
    gtk_widget_show (ipAddrText);
    gtk_table_attach (GTK_TABLE (table1), ipAddrText, 1, 2, 4, 5,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
    gtk_entry_set_has_frame ( GTK_ENTRY(ipAddrText), FALSE);
    gtk_editable_set_editable ( GTK_EDITABLE(ipAddrText), FALSE);
    GTK_WIDGET_UNSET_FLAGS(ipAddrText, GTK_CAN_FOCUS);
    g_signal_connect ((gpointer) ipAddrText, "realize",
                    G_CALLBACK (on_ipAddrText_realize),
                    NULL);

    /*
     * Other UI features....
     */
  notebook2 = gtk_notebook_new ();
  gtk_widget_show (notebook2);
  gtk_box_pack_start (GTK_BOX (vbox2), notebook2, TRUE, TRUE, 0);
  GTK_WIDGET_UNSET_FLAGS (notebook2, GTK_CAN_FOCUS);
  gtk_notebook_set_show_tabs (GTK_NOTEBOOK (notebook2), FALSE);

  empty_notebook_page = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (empty_notebook_page);
  gtk_container_add (GTK_CONTAINER (notebook2), empty_notebook_page);

  dhcpTabLabel = gtk_label_new (_("label3"));
  gtk_widget_show (dhcpTabLabel);
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook2), gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook2), 0), dhcpTabLabel);

  table2 = gtk_table_new (6, 2, TRUE);
  gtk_widget_show (table2);
  gtk_container_add (GTK_CONTAINER (notebook2), table2);
  gtk_container_set_border_width (GTK_CONTAINER (table2), 10);

  label6 = gtk_label_new (_("Netmask"));
  gtk_widget_show (label6);
  gtk_table_attach (GTK_TABLE (table2), label6, 0, 1, 1, 2,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label6), 0, 0.5);

  label5 = gtk_label_new (_("IP Address"));
  gtk_widget_show (label5);
  gtk_table_attach (GTK_TABLE (table2), label5, 0, 1, 0, 1,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (label5), GTK_JUSTIFY_CENTER);
  gtk_misc_set_alignment (GTK_MISC (label5), 0, 0.5);

  label7 = gtk_label_new (_("Gateway"));
  gtk_widget_show (label7);
  gtk_table_attach (GTK_TABLE (table2), label7, 0, 1, 2, 3,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label7), 0, 0.5);

  label8 = gtk_label_new (_("DNS 1"));
  gtk_widget_show (label8);
  gtk_table_attach (GTK_TABLE (table2), label8, 0, 1, 3, 4,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label8), 0, 0.5);

  label9 = gtk_label_new (_("DNS 2"));
  gtk_widget_show (label9);
  gtk_table_attach (GTK_TABLE (table2), label9, 0, 1, 4, 5,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label9), 0, 0.5);

  label10 = gtk_label_new (_("DNS 3"));
  gtk_widget_show (label10);
  gtk_table_attach (GTK_TABLE (table2), label10, 0, 1, 5, 6,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label10), 0, 0.5);

  ipAddressText = gtk_entry_new ();
  gtk_widget_show (ipAddressText);
  gtk_table_attach (GTK_TABLE (table2), ipAddressText, 1, 2, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_entry_set_invisible_char (GTK_ENTRY (ipAddressText), 8226);

  netmaskText = gtk_entry_new ();
  gtk_widget_show (netmaskText);
  gtk_table_attach (GTK_TABLE (table2), netmaskText, 1, 2, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_entry_set_invisible_char (GTK_ENTRY (netmaskText), 8226);

  gatewayText = gtk_entry_new ();
  gtk_widget_show (gatewayText);
  gtk_table_attach (GTK_TABLE (table2), gatewayText, 1, 2, 2, 3,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_entry_set_invisible_char (GTK_ENTRY (gatewayText), 8226);

  dns1Text = gtk_entry_new ();
  gtk_widget_show (dns1Text);
  gtk_table_attach (GTK_TABLE (table2), dns1Text, 1, 2, 3, 4,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_entry_set_invisible_char (GTK_ENTRY (dns1Text), 8226);

  dns2 = gtk_entry_new ();
  gtk_widget_show (dns2);
  gtk_table_attach (GTK_TABLE (table2), dns2, 1, 2, 4, 5,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_entry_set_invisible_char (GTK_ENTRY (dns2), 8226);

  dns3Text = gtk_entry_new ();
  gtk_widget_show (dns3Text);
  gtk_table_attach (GTK_TABLE (table2), dns3Text, 1, 2, 5, 6,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_entry_set_invisible_char (GTK_ENTRY (dns3Text), 8226);

  staticTabLabel = gtk_label_new (_("label4"));
  gtk_widget_show (staticTabLabel);
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook2), gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook2), 1), staticTabLabel);

  ipv4TabLabel = gtk_label_new (_("IPv4"));
  gtk_widget_show (ipv4TabLabel);
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook1), gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook1), 0), ipv4TabLabel);

  vbox3 = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox3);
  gtk_container_add (GTK_CONTAINER (notebook1), vbox3);

  hbox3 = gtk_hbox_new (TRUE, 0);
  gtk_widget_show (hbox3);
  gtk_box_pack_start (GTK_BOX (vbox3), hbox3, FALSE, FALSE, 0);

  label17 = gtk_label_new (_("SSID"));
  gtk_widget_show (label17);
  gtk_box_pack_start (GTK_BOX (hbox3), label17, FALSE, FALSE, 0);

  entry11 = gtk_entry_new ();
  gtk_widget_show (entry11);
  gtk_box_pack_start (GTK_BOX (hbox3), entry11, TRUE, TRUE, 0);
  gtk_entry_set_invisible_char (GTK_ENTRY (entry11), 8226);

  hbox2 = gtk_hbox_new (TRUE, 0);
  gtk_widget_show (hbox2);
  gtk_box_pack_start (GTK_BOX (vbox3), hbox2, FALSE, TRUE, 0);

  securityLabel = gtk_label_new (_("Security"));
  gtk_widget_show (securityLabel);
  gtk_box_pack_start (GTK_BOX (hbox2), securityLabel, FALSE, FALSE, 0);

  securityComboBox = gtk_combo_box_new_text ();
  gtk_widget_show (securityComboBox);
  gtk_box_pack_start (GTK_BOX (hbox2), securityComboBox, TRUE, TRUE, 0);
  gtk_combo_box_append_text (GTK_COMBO_BOX (securityComboBox), _("WPA & WPA2 Personal"));
  gtk_combo_box_append_text (GTK_COMBO_BOX (securityComboBox), _("WPA & WPA2 Enterprise"));

  notebook3 = gtk_notebook_new ();
  gtk_widget_show (notebook3);
  gtk_box_pack_start (GTK_BOX (vbox3), notebook3, FALSE, FALSE, 0);
  GTK_WIDGET_UNSET_FLAGS (notebook3, GTK_CAN_FOCUS);
  gtk_notebook_set_show_tabs (GTK_NOTEBOOK (notebook3), FALSE);
  gtk_notebook_set_show_border (GTK_NOTEBOOK (notebook3), FALSE);

  table4 = gtk_table_new (2, 2, TRUE);
  gtk_widget_show (table4);
  gtk_container_add (GTK_CONTAINER (notebook3), table4);

  passwordLabel = gtk_label_new (_("Password"));
  gtk_widget_show (passwordLabel);
  gtk_table_attach (GTK_TABLE (table4), passwordLabel, 0, 1, 0, 1,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (passwordLabel), 0, 0.5);

  passwordTextEntry = gtk_entry_new ();
  gtk_widget_show (passwordTextEntry);
  gtk_table_attach (GTK_TABLE (table4), passwordTextEntry, 1, 2, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_entry_set_visibility (GTK_ENTRY (passwordTextEntry), FALSE);
  gtk_entry_set_invisible_char (GTK_ENTRY (passwordTextEntry), 8226);

  showPasswordButton = gtk_check_button_new_with_mnemonic (_("Show Password"));
  gtk_widget_show (showPasswordButton);
  gtk_table_attach (GTK_TABLE (table4), showPasswordButton, 0, 2, 1, 2,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);

  wpaPersonalTabLabel = gtk_label_new (_("label13"));
  gtk_widget_show (wpaPersonalTabLabel);
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook3), gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook3), 0), wpaPersonalTabLabel);

  wirelessTabLabel = gtk_label_new (_("Wireless"));
  gtk_widget_show (wirelessTabLabel);
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook1), gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook1), 1), wirelessTabLabel);

  vbox5 = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox5);
  gtk_container_add (GTK_CONTAINER (notebook1), vbox5);

  hbox5 = gtk_hbox_new (TRUE, 0);
  gtk_widget_show (hbox5);
  gtk_box_pack_start (GTK_BOX (vbox5), hbox5, FALSE, FALSE, 0);

  ssidLabel = gtk_label_new (_("SSID"));
  gtk_widget_show (ssidLabel);
  gtk_box_pack_start (GTK_BOX (hbox5), ssidLabel, FALSE, FALSE, 0);

  accessPointSSIDEntry = gtk_entry_new ();
  gtk_widget_show (accessPointSSIDEntry);
  gtk_box_pack_start (GTK_BOX (hbox5), accessPointSSIDEntry, TRUE, TRUE, 0);
  gtk_entry_set_invisible_char (GTK_ENTRY (accessPointSSIDEntry), 8226);

  hbox6 = gtk_hbox_new (TRUE, 0);
  gtk_widget_show (hbox6);
  gtk_box_pack_start (GTK_BOX (vbox5), hbox6, FALSE, FALSE, 0);

  channelLabel = gtk_label_new (_("Channel"));
  gtk_widget_show (channelLabel);
  gtk_box_pack_start (GTK_BOX (hbox6), channelLabel, FALSE, FALSE, 0);

  combobox1 = gtk_combo_box_new_text ();
  gtk_widget_show (combobox1);
  gtk_box_pack_start (GTK_BOX (hbox6), combobox1, TRUE, TRUE, 0);
  gtk_combo_box_append_text (GTK_COMBO_BOX (combobox1), _("1"));
  gtk_combo_box_append_text (GTK_COMBO_BOX (combobox1), _("2"));
  gtk_combo_box_append_text (GTK_COMBO_BOX (combobox1), _("3"));
  gtk_combo_box_append_text (GTK_COMBO_BOX (combobox1), _("4"));
  gtk_combo_box_append_text (GTK_COMBO_BOX (combobox1), _("5"));
  gtk_combo_box_append_text (GTK_COMBO_BOX (combobox1), _("6"));
  gtk_combo_box_append_text (GTK_COMBO_BOX (combobox1), _("7"));
  gtk_combo_box_append_text (GTK_COMBO_BOX (combobox1), _("8"));
  gtk_combo_box_append_text (GTK_COMBO_BOX (combobox1), _("9"));
  gtk_combo_box_append_text (GTK_COMBO_BOX (combobox1), _("10"));
  gtk_combo_box_append_text (GTK_COMBO_BOX (combobox1), _("11"));
  gtk_combo_box_append_text (GTK_COMBO_BOX (combobox1), _("12"));
  gtk_combo_box_append_text (GTK_COMBO_BOX (combobox1), _("13"));
  gtk_combo_box_append_text (GTK_COMBO_BOX (combobox1), _("14"));

// --------------------------------
  hbox5b = gtk_hbox_new (TRUE, 0);
  gtk_widget_show (hbox5b);
  gtk_box_pack_start (GTK_BOX (vbox5), hbox5b, FALSE, FALSE, 0);

  hostapNetLabel = gtk_label_new (_("Base Network Address"));
  gtk_widget_show (hostapNetLabel);
  gtk_box_pack_start (GTK_BOX (hbox5b), hostapNetLabel, FALSE, FALSE, 0);

  hostapNetEntry = gtk_entry_new ();
  gtk_widget_show (hostapNetEntry);
  gtk_box_pack_start (GTK_BOX (hbox5b), hostapNetEntry, TRUE, TRUE, 0);
  gtk_entry_set_invisible_char (GTK_ENTRY (hostapNetEntry), 8226);
// --------------------------------

  table5 = gtk_table_new (2, 2, TRUE);
  gtk_widget_show (table5);
  gtk_box_pack_start (GTK_BOX (vbox5), table5, FALSE, FALSE, 0);

  label22 = gtk_label_new (_("Password"));
  gtk_widget_show (label22);
  gtk_table_attach (GTK_TABLE (table5), label22, 0, 1, 0, 1,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label22), 0, 0.5);

  accessPointPasswordEntry = gtk_entry_new ();
  gtk_widget_show (accessPointPasswordEntry);
  gtk_table_attach (GTK_TABLE (table5), accessPointPasswordEntry, 1, 2, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_entry_set_max_length (GTK_ENTRY (accessPointPasswordEntry), 64);
  gtk_entry_set_visibility (GTK_ENTRY (accessPointPasswordEntry), FALSE);
  gtk_entry_set_invisible_char (GTK_ENTRY (accessPointPasswordEntry), 8226);

  accessPointShowPasswordButton = gtk_check_button_new_with_mnemonic (_("Show Password"));
  gtk_widget_show (accessPointShowPasswordButton);
  gtk_table_attach (GTK_TABLE (table5), accessPointShowPasswordButton, 0, 2, 1, 2,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);

  label19 = gtk_label_new (_("Access Point"));
  gtk_widget_show (label19);
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook1), gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook1), 2), label19);

/* Add the scan widget. */
{
    GtkWidget *scanVbox;
    GtkWidget *scanLabel;
    GtkWidget *alignment;

    // scanVbox = gtk_vbox_new (FALSE, 0);
    // gtk_widget_show (scanVbox);
    // gtk_container_add (GTK_CONTAINER (mainHbox), scanVbox);
    // gtk_container_add (GTK_CONTAINER (notebook1), scanVbox);

    alignment = gtk_alignment_new(0,0,1.0,1.0);

    /*
     * The graphing area
     */
    wifiscan = gtk_wifiscan_new();
    gtk_widget_set_size_request( wifiscan, 300, 600);
    gtk_widget_show(wifiscan);
    // gtk_box_pack_start (GTK_BOX (scanVbox), wifiscan, TRUE, TRUE, 0);
    gtk_container_add (GTK_CONTAINER (alignment), wifiscan);
    gtk_container_add (GTK_CONTAINER (notebook1), alignment);

    scanLabel = gtk_label_new (_("Scanner"));
    gtk_widget_show (scanLabel);
    gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook1), gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook1), 3), scanLabel);
}

  hbox1 = gtk_hbox_new (FALSE, 10);
  gtk_widget_show (hbox1);
  gtk_box_pack_start (GTK_BOX (vbox1), hbox1, FALSE, FALSE, 0);

  statusbar1 = gtk_statusbar_new ();
  gtk_widget_show (statusbar1);
  gtk_box_pack_start (GTK_BOX (hbox1), statusbar1, TRUE, TRUE, 0);
  // gtk_widget_set_size_request (statusbar1, 250, -1);
  // gtk_container_set_border_width (GTK_CONTAINER (statusbar1), 5);

  wirelessButton = gtk_radio_button_new_with_mnemonic (NULL, _("Wireless"));
  gtk_widget_show (wirelessButton);
  gtk_box_pack_start (GTK_BOX (hbox1), wirelessButton, FALSE, FALSE, 0);
  gtk_radio_button_set_group (GTK_RADIO_BUTTON (wirelessButton), wirelessButton_group);
  wirelessButton_group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (wirelessButton));

  accessPointButton = gtk_radio_button_new_with_mnemonic (NULL, _("Access Point"));
  gtk_widget_show (accessPointButton);
  gtk_box_pack_start (GTK_BOX (hbox1), accessPointButton, FALSE, FALSE, 5);
  gtk_radio_button_set_group (GTK_RADIO_BUTTON (accessPointButton), wirelessButton_group);
  wirelessButton_group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (accessPointButton));

  saveButton = gtk_button_new_from_stock ("gtk-save");
  if ( isCLIFlagSet( CLI_EMBEDDED ) )
      gtk_button_set_relief (GTK_BUTTON(saveButton), GTK_RELIEF_NONE);
  gtk_widget_show (saveButton);
  gtk_box_pack_start (GTK_BOX (hbox1), saveButton, FALSE, FALSE, 0);

  if ( isCLIFlagSet( CLI_EMBEDDED ) )
  {
      restartButton = gtk_button_new_from_stock ("gtk-apply");
      gtk_button_set_relief (GTK_BUTTON(restartButton), GTK_RELIEF_NONE);
      gtk_widget_show (restartButton);
      gtk_box_pack_start (GTK_BOX (hbox1), restartButton, FALSE, FALSE, 0);
      g_signal_connect ((gpointer) restartButton, "activate",
                    G_CALLBACK (on_restart1_activate),
                    NULL);

      homeButton = gtk_button_new_from_stock ("gtk-home");
      gtk_button_set_relief (GTK_BUTTON(homeButton), GTK_RELIEF_NONE);
      gtk_widget_show (homeButton);
      gtk_box_pack_start (GTK_BOX (hbox1), homeButton, FALSE, FALSE, 0);
      g_signal_connect ((gpointer) homeButton, "activate",
                    G_CALLBACK (on_quit1_activate),
                    NULL);
  }

  g_signal_connect ((gpointer) window1, "delete_event",
                    G_CALLBACK (doShutdown),
                    NULL);
  g_signal_connect ((gpointer) window1, "delete_event",
                    G_CALLBACK (gtk_main_quit),
                    NULL);
  g_signal_connect ((gpointer) open1, "activate",
                    G_CALLBACK (on_open1_activate),
                    NULL);
  g_signal_connect ((gpointer) save1, "activate",
                    G_CALLBACK (on_save1_activate),
                    NULL);
  g_signal_connect ((gpointer) save_as1, "activate",
                    G_CALLBACK (on_save_as1_activate),
                    NULL);
  g_signal_connect ((gpointer) restart1, "activate",
                    G_CALLBACK (on_restart1_activate),
                    NULL);
  g_signal_connect ((gpointer) quit1, "activate",
                    G_CALLBACK (on_quit1_activate),
                    NULL);
  g_signal_connect ((gpointer) about1, "activate",
                    G_CALLBACK (on_about1_activate),
                    NULL);
  g_signal_connect ((gpointer) notebook1, "realize",
                    G_CALLBACK (on_notebook1_realize),
                    NULL);
  g_signal_connect ((gpointer) addressTypeComboBox, "realize",
                    G_CALLBACK (on_addressTypeComboBox_realize),
                    NULL);
  g_signal_connect ((gpointer) addressTypeComboBox, "changed",
                    G_CALLBACK (on_addressTypeComboBox_changed),
                    NULL);
  g_signal_connect ((gpointer) interfaceComboBox, "changed",
                    G_CALLBACK (on_interfaceComboBox_changed),
                    NULL);
  g_signal_connect ((gpointer) interfaceComboBox, "realize",
                    G_CALLBACK (on_interfaceComboBox_realize),
                    NULL);
  g_signal_connect ((gpointer) radiobutton1, "clicked",
                    G_CALLBACK (on_radiobutton1_clicked),
                    NULL);
  g_signal_connect ((gpointer) radiobutton1, "realize",
                    G_CALLBACK (on_radiobutton1_realize),
                    NULL);
  g_signal_connect ((gpointer) radiobutton2, "realize",
                    G_CALLBACK (on_radiobutton2_realize),
                    NULL);
  g_signal_connect ((gpointer) notebook2, "realize",
                    G_CALLBACK (on_notebook2_realize),
                    NULL);
  g_signal_connect ((gpointer) ipAddressText, "realize",
                    G_CALLBACK (on_ipAddressText_realize),
                    NULL);
  g_signal_connect ((gpointer) netmaskText, "realize",
                    G_CALLBACK (on_netmaskText_realize),
                    NULL);
  g_signal_connect ((gpointer) gatewayText, "realize",
                    G_CALLBACK (on_gatewayText_realize),
                    NULL);
  g_signal_connect ((gpointer) dns1Text, "realize",
                    G_CALLBACK (on_dns1Text_realize),
                    NULL);
  g_signal_connect ((gpointer) dns2, "realize",
                    G_CALLBACK (on_dns2_realize),
                    NULL);
  g_signal_connect ((gpointer) dns3Text, "realize",
                    G_CALLBACK (on_dns3Text_realize),
                    NULL);
  g_signal_connect ((gpointer) vbox3, "realize",
                    G_CALLBACK (on_vbox3_realize),
                    NULL);
  g_signal_connect ((gpointer) entry11, "realize",
                    G_CALLBACK (on_entry11_realize),
                    NULL);
  g_signal_connect ((gpointer) securityComboBox, "changed",
                    G_CALLBACK (on_combobox3_changed),
                    NULL);
  g_signal_connect ((gpointer) securityComboBox, "realize",
                    G_CALLBACK (on_combobox3_realize),
                    NULL);
  g_signal_connect ((gpointer) passwordTextEntry, "realize",
                    G_CALLBACK (on_passwordTextEntry_realize),
                    NULL);
  g_signal_connect ((gpointer) showPasswordButton, "clicked",
                    G_CALLBACK (on_showPasswordButton_clicked),
                    NULL);
  g_signal_connect ((gpointer) vbox5, "realize",
                    G_CALLBACK (on_vbox5_realize),
                    NULL);
  g_signal_connect ((gpointer) accessPointSSIDEntry, "realize",
                    G_CALLBACK (on_accessPointSSIDEntry_realize),
                    NULL);
  g_signal_connect ((gpointer) hostapNetEntry, "realize",
                    G_CALLBACK (on_hostapNetEntry_realize),
                    NULL);
  g_signal_connect ((gpointer) combobox1, "realize",
                    G_CALLBACK (on_accessPointChannel_realize),
                    NULL);
  g_signal_connect ((gpointer) combobox1, "changed",
                    G_CALLBACK (on_accessPointChannel_changed),
                    NULL);
  g_signal_connect ((gpointer) accessPointPasswordEntry, "realize",
                    G_CALLBACK (on_accessPointPasswordEntry_realize),
                    NULL);
  g_signal_connect ((gpointer) accessPointShowPasswordButton, "clicked",
                    G_CALLBACK (on_accessPointShowPasswordButton_clicked),
                    NULL);
  g_signal_connect ((gpointer) statusbar1, "realize",
                    G_CALLBACK (on_statusbar1_realize),
                    NULL);
  g_signal_connect ((gpointer) wirelessButton, "clicked",
                    G_CALLBACK (on_wirelessButton_clicked),
                    NULL);
  g_signal_connect ((gpointer) wirelessButton, "realize",
                    G_CALLBACK (on_wirelessButton_realize),
                    NULL);
  g_signal_connect ((gpointer) accessPointButton, "realize",
                    G_CALLBACK (on_accessPointButton_realize),
                    NULL);
  g_signal_connect ((gpointer) saveButton, "clicked",
                    G_CALLBACK (on_button1_clicked),
                    NULL);

  /* Store pointers to all widgets, for use by lookup_widget(). */
/*
  GLADE_HOOKUP_OBJECT_NO_REF (window1, window1, "window1");
  GLADE_HOOKUP_OBJECT (window1, vbox1, "vbox1");
  GLADE_HOOKUP_OBJECT (window1, menubar1, "menubar1");
  GLADE_HOOKUP_OBJECT (window1, menuitem4, "menuitem4");
  GLADE_HOOKUP_OBJECT (window1, menuitem4_menu, "menuitem4_menu");
  GLADE_HOOKUP_OBJECT (window1, open1, "open1");
  GLADE_HOOKUP_OBJECT (window1, save1, "save1");
  GLADE_HOOKUP_OBJECT (window1, save_as1, "save_as1");
  GLADE_HOOKUP_OBJECT (window1, restart1, "restart1");
  GLADE_HOOKUP_OBJECT (window1, separatormenuitem1, "separatormenuitem1");
  GLADE_HOOKUP_OBJECT (window1, quit1, "quit1");
  GLADE_HOOKUP_OBJECT (window1, menuitem7, "menuitem7");
  GLADE_HOOKUP_OBJECT (window1, menuitem7_menu, "menuitem7_menu");
  GLADE_HOOKUP_OBJECT (window1, about1, "about1");
  GLADE_HOOKUP_OBJECT (window1, notebook1, "notebook1");
  GLADE_HOOKUP_OBJECT (window1, vbox2, "vbox2");
  GLADE_HOOKUP_OBJECT (window1, table1, "table1");
  GLADE_HOOKUP_OBJECT (window1, interfaceLabel, "interfaceLabel");
  GLADE_HOOKUP_OBJECT (window1, addressTypeLabel, "addressTypeLabel");
  GLADE_HOOKUP_OBJECT (window1, addressTypeComboBox, "addressTypeComboBox");
  GLADE_HOOKUP_OBJECT (window1, interfaceComboBox, "interfaceComboBox");
  GLADE_HOOKUP_OBJECT (window1, label18, "label18");
  GLADE_HOOKUP_OBJECT (window1, hbox4, "hbox4");
  GLADE_HOOKUP_OBJECT (window1, radiobutton1, "radiobutton1");
  GLADE_HOOKUP_OBJECT (window1, radiobutton2, "radiobutton2");
  GLADE_HOOKUP_OBJECT (window1, notebook2, "notebook2");
  GLADE_HOOKUP_OBJECT (window1, dhcpTabLabel, "dhcpTabLabel");
  GLADE_HOOKUP_OBJECT (window1, table2, "table2");
  GLADE_HOOKUP_OBJECT (window1, label6, "label6");
  GLADE_HOOKUP_OBJECT (window1, label5, "label5");
  GLADE_HOOKUP_OBJECT (window1, label7, "label7");
  GLADE_HOOKUP_OBJECT (window1, label8, "label8");
  GLADE_HOOKUP_OBJECT (window1, label9, "label9");
  GLADE_HOOKUP_OBJECT (window1, label10, "label10");
  GLADE_HOOKUP_OBJECT (window1, ipAddressText, "ipAddressText");
  GLADE_HOOKUP_OBJECT (window1, netmaskText, "netmaskText");
  GLADE_HOOKUP_OBJECT (window1, gatewayText, "gatewayText");
  GLADE_HOOKUP_OBJECT (window1, dns1Text, "dns1Text");
  GLADE_HOOKUP_OBJECT (window1, dns2, "dns2");
  GLADE_HOOKUP_OBJECT (window1, dns3Text, "dns3Text");
  GLADE_HOOKUP_OBJECT (window1, staticTabLabel, "staticTabLabel");
  GLADE_HOOKUP_OBJECT (window1, ipv4TabLabel, "ipv4TabLabel");
  GLADE_HOOKUP_OBJECT (window1, vbox3, "vbox3");
  GLADE_HOOKUP_OBJECT (window1, hbox3, "hbox3");
  GLADE_HOOKUP_OBJECT (window1, label17, "label17");
  GLADE_HOOKUP_OBJECT (window1, entry11, "entry11");
  GLADE_HOOKUP_OBJECT (window1, hbox2, "hbox2");
  GLADE_HOOKUP_OBJECT (window1, securityLabel, "securityLabel");
  GLADE_HOOKUP_OBJECT (window1, securityComboBox, "securityComboBox");
  GLADE_HOOKUP_OBJECT (window1, notebook3, "notebook3");
  GLADE_HOOKUP_OBJECT (window1, table4, "table4");
  GLADE_HOOKUP_OBJECT (window1, passwordLabel, "passwordLabel");
  GLADE_HOOKUP_OBJECT (window1, passwordTextEntry, "passwordTextEntry");
  GLADE_HOOKUP_OBJECT (window1, showPasswordButton, "showPasswordButton");
  GLADE_HOOKUP_OBJECT (window1, wpaPersonalTabLabel, "wpaPersonalTabLabel");
  GLADE_HOOKUP_OBJECT (window1, wirelessTabLabel, "wirelessTabLabel");
  GLADE_HOOKUP_OBJECT (window1, vbox5, "vbox5");
  GLADE_HOOKUP_OBJECT (window1, hbox5, "hbox5");
  GLADE_HOOKUP_OBJECT (window1, hbox5b, "hbox5b");
  GLADE_HOOKUP_OBJECT (window1, ssidLabel, "ssidLabel");
  GLADE_HOOKUP_OBJECT (window1, hostapNetLabel, "hostapNetLabel");
  GLADE_HOOKUP_OBJECT (window1, hostapNetEntry, "hostapNetEntry");
  GLADE_HOOKUP_OBJECT (window1, accessPointSSIDEntry, "accessPointSSIDEntry");
  GLADE_HOOKUP_OBJECT (window1, hbox6, "hbox6");
  GLADE_HOOKUP_OBJECT (window1, channelLabel, "channelLabel");
  GLADE_HOOKUP_OBJECT (window1, combobox1, "combobox1");
  GLADE_HOOKUP_OBJECT (window1, table5, "table5");
  GLADE_HOOKUP_OBJECT (window1, label22, "label22");
  GLADE_HOOKUP_OBJECT (window1, accessPointPasswordEntry, "accessPointPasswordEntry");
  GLADE_HOOKUP_OBJECT (window1, accessPointShowPasswordButton, "accessPointShowPasswordButton");
  GLADE_HOOKUP_OBJECT (window1, label19, "label19");
  GLADE_HOOKUP_OBJECT (window1, hbox1, "hbox1");
  GLADE_HOOKUP_OBJECT (window1, statusbar1, "statusbar1");
  GLADE_HOOKUP_OBJECT (window1, wirelessButton, "wirelessButton");
  GLADE_HOOKUP_OBJECT (window1, accessPointButton, "accessPointButton");
  GLADE_HOOKUP_OBJECT (window1, saveButton, "saveButton");
*/

  gtk_window_add_accel_group (GTK_WINDOW (window1), accel_group);

  return window1;
}

GtkWidget*
create_aboutdialog1 (void)
{
  GtkWidget *aboutdialog1;
  const gchar *authors[] = {
    "Michael J. Hammel",
    NULL
  };
  /* TRANSLATORS: Replace this string with your names, one name per line. */
  gchar *translators = _("translator-credits");

  aboutdialog1 = gtk_about_dialog_new ();
  gtk_container_set_border_width (GTK_CONTAINER (aboutdialog1), 5);
  gtk_window_set_destroy_with_parent (GTK_WINDOW (aboutdialog1), TRUE);
  gtk_about_dialog_set_version (GTK_ABOUT_DIALOG (aboutdialog1), VERSION);
  gtk_about_dialog_set_name (GTK_ABOUT_DIALOG (aboutdialog1), _("pibox-network-config"));
  gtk_about_dialog_set_license (GTK_ABOUT_DIALOG (aboutdialog1), _("MIT LIcense"));
  gtk_about_dialog_set_website (GTK_ABOUT_DIALOG (aboutdialog1), "http://www.graphics-muse.org/wiki/pmwiki.php?action=home");
  gtk_about_dialog_set_website_label (GTK_ABOUT_DIALOG (aboutdialog1), _("Graphics Muse Wiki"));
  gtk_about_dialog_set_authors (GTK_ABOUT_DIALOG (aboutdialog1), authors);
  gtk_about_dialog_set_translator_credits (GTK_ABOUT_DIALOG (aboutdialog1), translators);

  g_signal_connect ((gpointer) aboutdialog1, "close",
                    G_CALLBACK (gtk_widget_destroy),
                    NULL);
  g_signal_connect ((gpointer) aboutdialog1, "response",
                    G_CALLBACK (on_aboutdialog1_response),
                    NULL);

  /* Store pointers to all widgets, for use by lookup_widget(). */
  // GLADE_HOOKUP_OBJECT_NO_REF (aboutdialog1, aboutdialog1, "aboutdialog1");

  return aboutdialog1;
}

