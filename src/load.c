/*******************************************************************************
 * pibox-network-config
 *
 * load.c:  Functions for reading data files.
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define LOAD_C

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <glib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <pibox/utils.h>
#include <pibox/log.h>

#include "cli.h"
#include "callbacks.h"

// Library API
#include "pnc.h"
#include "pncPrivate.h"

/*
 * ========================================================================
 * Type definitions and Data structures 
 * =======================================================================
 */
#define LOOPBACK      "lo"

#define N_AUTO      0
#define N_IFACE     1
#define N_ADDRESS   2
#define N_NETMASK   3
#define N_GATEWAY   4
char *INTERFACE_OPTIONS[] = {
    "auto", 
    "iface", 
    "address", 
    "netmask", 
    "gateway", 
    NULL
};

#define N_INET      0
#define N_INET6     1
char *NETWORK_TYPES[] = {
    "inet", 
    "inet6", 
    NULL
};

#define N_DHCP      0
#define N_STATIC    1
char *ADDRESS_TYPES[] = {
    "dhcp", 
    "static", 
    NULL
};

#define N_NAMESERVER 0
char *RESOLV_OPTIONS[] = {
    "nameserver", 
    NULL
};

#define WPA_NETWORK "network"
static char *WPA_OPTIONS[] = {
    "ssid", 
    "scan_ssid", 
    "proto", 
    "key_mgmt", 
    "psk", 
    "pairwise", 
    "group", 
    NULL
};

static char *HOSTAP_OPTIONS[] = {
    "interface", 
    "driver", 
    "ssid", 
    "channel", 
    "auth_algs", 
    "wpa", 
    "wpa_psk_file", 
    "wpa_key_mgmt", 
    "rsn_pairwise", 
    "wpa_pairwise", 
    "macaddr_acl", 
    "ignore_broadcast_ssid", 
    "hw_mode", 
    NULL
};

// The link list of interfaces and their configurations
GSList  *interfaces = NULL;
// The link list of network devices from /sys/class/net
GSList  *netInterfaces = NULL;
// The link list of nameservers from /etc/resolv.conf
GSList  *nameservers = NULL;
// The link list of wpa data from /etc/wpa_supplicant.conf
GSList  *wpa = NULL;
// The link list of hostapd data from /etc/hostapd.conf
GSList  *hostap = NULL;
// The home key is the key that exits the app.
static char *homeKey = NULL;

/*
 *========================================================================
 *========================================================================
 * STATIC FUNCTIONS (not part of API)
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   clearInterface
 * Prototype:  void clearInterface( void )
 *
 * Description:
 * g_slist_foreach() callback.  Free up interface definition from the stored link list, if any.
 *========================================================================
 */
static void 
clearInterface( gpointer item, gpointer user_data )
{
    PNC_INTERFACE_T *iface = (PNC_INTERFACE_T *)item;
    if ( iface == NULL )
        return;
    if ( iface->name != NULL ) free(iface->name);
    if ( iface->macAddress != NULL ) free(iface->macAddress);
    if ( iface->networkType != NULL ) free(iface->networkType);
    if ( iface->addressType != NULL ) free(iface->addressType);
    if ( iface->address != NULL ) free(iface->address);
    if ( iface->netmask != NULL ) free(iface->netmask);
    if ( iface->gateway != NULL ) free(iface->gateway);
    free(iface);
}

/*
 *========================================================================
 * Name:   clearNetInterface
 * Prototype:  void clearNetInterface( void )
 *
 * Description:
 * g_slist_foreach() callback.  Free up netInterface definition from the stored link list, if any.
 *========================================================================
 */
static void 
clearNetInterface( gpointer item, gpointer user_data )
{
    if ( item != NULL )
        g_free(item);
}

/*
 *========================================================================
 * Name:   clearWPA
 * Prototype:  void clearWPA( void )
 *
 * Description:
 * g_slist_foreach() callback.  Free up WPA definition from the stored link list, if any.
 *========================================================================
 */
static void 
clearWPA( gpointer item, gpointer user_data )
{
    if ( item != NULL )
        g_free(item);
}

/*
 *========================================================================
 * Name:   clearHostAP
 * Prototype:  void clearHostAP( void )
 *
 * Description:
 * g_slist_foreach() callback.  Free up HostAP definition from the stored link list, if any.
 *========================================================================
 */
static void 
clearHostAP( gpointer item, gpointer user_data )
{
    if ( item != NULL )
        g_free(item);
}

/*
 *========================================================================
 * Name:   clearNameserver
 * Prototype:  void clearNameserver( void )
 *
 * Description:
 * g_slist_foreach() callback.  Free up nameserver definition from the stored link list, if any.
 *========================================================================
 */
static void 
clearNameserver( gpointer item, gpointer user_data )
{
    if ( item != NULL )
        g_free(item);
}

/*
 *========================================================================
 * Name:   findNetInterface
 * Prototype:  gint findNetInterface( gconstpointer )
 *
 * Description:
 * Find a node in the list based on a the name.
 *========================================================================
 */
static gint 
findNetInterface( gconstpointer item, gconstpointer user_data )
{
    char *devName = (char *)item;
    char *name = (char *)user_data;
    if ( devName == NULL )
    {
        piboxLogger(LOG_ERROR, "%s: devName is null\n", __FUNCTION__);
        return 0;
    }
    if ( name == NULL )
    {
        piboxLogger(LOG_ERROR, "%s: name (via user_data) is null\n", __FUNCTION__);
        return 0;
    }
    piboxLogger(LOG_INFO, "devName/name: %s, %s\n", devName, name);
    if ( strcmp(name, devName) == 0 )
        return 0;
    return 1;
}

/*
 *========================================================================
 * Name:   findNameserver
 * Prototype:  gint findNameserver( gconstpointer )
 *
 * Description:
 * Find a node in the list based on a the address.
 *========================================================================
 */
static gint 
findNameserver( gconstpointer item, gconstpointer user_data )
{
    gchar *address = (gchar *)item;
    char *name = (char *)user_data;
    if ( address == NULL )
    {
        piboxLogger(LOG_ERROR, "%s: address is null\n", __FUNCTION__);
        return 0;
    }
    if ( name == NULL )
    {
        piboxLogger(LOG_ERROR, "%s: name is null\n", __FUNCTION__);
        return 0;
    }
    if ( strcmp(name, address) == 0 )
        return 0;
    return 1;
}

/*
 *========================================================================
 *========================================================================
 * PRIVAE LIBRARY FUNCTIONS 
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   getWPAOption
 * Prototype:  gchar *getWPAOption( gint idx )
 *
 * Description:
 * Retrieve a field from the WPA_OPTIONS array.
 *
 * Arguments:
 * idx     index into the array.  See pnc.h:PNC_ID_WPA_xxx values.
 *========================================================================
 */
gchar *
getWPAOption( gint idx )
{
    return WPA_OPTIONS[idx];
}

/*
 *========================================================================
 * Name:   getHostAPOption
 * Prototype:  gchar *getHostAPOption( gint idx )
 *
 * Description:
 * Retrieve a field from the HOSTAP_OPTIONS array.
 *
 * Arguments:
 * idx     index into the array.  See pnc.h:PNC_ID_HAP_xxx values.
 *========================================================================
 */
gchar *
getHostAPOption( gint idx )
{
    return HOSTAP_OPTIONS[idx];
}

/*
 *========================================================================
 *========================================================================
 * PUBLIC API FUNCTIONS 
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   pncClearKeysym
 * Prototype:  void pncClearKeysym( void )
 *
 * Description:
 * Clean up keysym settings.
 *========================================================================
 */
void 
pncClearKeysym( void )
{
    homeKey = NULL;
}

/*
 *========================================================================
 * Name:   pncLoadKeysysm
 * Prototype:  void pncLoadKeysysm( void )
 *
 * Description:
 * Read in the keysym file so we know how the platform wants us to behave.
 *
 * Notes:
 * Format is KEYSYM NAME:ACTION
 *========================================================================
 */
void 
pncLoadKeysyms( void )
{
    char        *tsave = NULL;
    struct stat stat_buf;
    char        *keysym;
    char        *action;
    char        *ptr;
    FILE        *fd;
    char        buf[128];
    char        *path;

    path = pncGetFilename( PNC_ID_KEYSYMS );

    // Read in /etc/pibox-keysysm
    if ( stat(path, &stat_buf) != 0 )
    {
        piboxLogger(LOG_INFO, "No keysym file: %s\n", path);
        g_free( path );
        return;
    }

    fd = fopen(path, "r");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open keysyms file: %s - %s\n", path, strerror(errno));
        g_free( path );
        return;
    }
    g_free( path );

    memset(buf, 0, 128);
    while( fgets(buf, 127, fd) != NULL )
    {
        // Ignore comments
        if ( buf[0] == '#' )
            continue;

        // Strip leading white space
        ptr = buf;
        ptr = piboxTrim(ptr);

        // Ignore blank lines
        if ( strlen(ptr) == 0 )
            continue;

        // Strip newline
        piboxStripNewline(ptr);

        // Grab first token
        keysym = strtok_r(ptr, ":", &tsave);
        if ( keysym == NULL )
            continue;

        // Grab second token
        action = strtok_r(NULL, ":", &tsave);
        if ( action == NULL )
            continue;

        // Set the home key
        if ( strncasecmp("home", action, 4) == 0 )
        {
            piboxLogger(LOG_INFO, "keysym/homeKey = %s\n", keysym);
            homeKey = g_strdup(keysym);
        }
    }
}

/*
 *========================================================================
 * Name:   pncFindInterface
 * Prototype:  gint pncFindInterface( gconstpointer )
 *
 * Description:
 * Find a node in the list based on a the name.
 *========================================================================
 */
gint 
pncFindInterface( gconstpointer item, gconstpointer user_data )
{
    PNC_INTERFACE_T *iface = (PNC_INTERFACE_T *)item;
    char *name = (char *)user_data;
    if ( iface == NULL )
    {
        piboxLogger(LOG_ERROR, "iface is null\n");
        return 0;
    }
    piboxLogger(LOG_TRACE3, "have %s, want %s\n", iface->name, name);
    if ( strcmp(name, iface->name) == 0 )
        return 0;
    return 1;
}

/*
 *========================================================================
 * Name:   pncClearInterfaces
 * Prototype:  void pncClearInterfaces( void )
 *
 * Description:
 * Free up all interface definition from the stored link list, if any.
 *========================================================================
 */
void 
pncClearInterfaces( void )
{
    if ( interfaces == NULL )
        return;

    // Clear any existing list.
    g_slist_foreach(interfaces, clearInterface, NULL);
    g_slist_free(interfaces);
    interfaces = NULL;
}

/*
 *========================================================================
 * Name:   pncNewInterface
 * Prototype:  void pncNewInterface( char * )
 *
 * Description:
 * Add a network interface configuration.
 *========================================================================
 */
void 
pncNewInterface( char *name )
{
    PNC_INTERFACE_T *iface;
    const gchar *field;

    // Create interface definition
    piboxLogger(LOG_INFO, "%s: ================ Adding new configuration for %s\n", __FUNCTION__, name);
    iface = g_new(PNC_INTERFACE_T, 1);
    memset(iface, 0, sizeof(PNC_INTERFACE_T));
    iface->name = g_strdup(name);
    iface->macAddress  = NULL;
    iface->networkType = g_strdup("inet");
    iface->addressType = g_strdup("dhcp");
    iface->address     = NULL;
    iface->enabled     = 0;

    // Append it to the linked list
    interfaces = g_slist_append(interfaces, iface);
}

/*
 *========================================================================
 * Name:   pncRemoveInterface
 * Prototype:  void pncRemoveInterface( char * )
 *
 * Description:
 * Remove a network interface configuration internally.
 * 
 * Notes:
 * Caller should call pncSaveInterface() immediately after this to update
 * the associated file.
 *========================================================================
 */
void 
pncRemoveInterface( char *name )
{
    PNC_INTERFACE_T *iface;
    GSList          *item = NULL;
    const gchar     *field;

    item = g_slist_find_custom(interfaces, name, (GCompareFunc)pncFindInterface);
    if ( item != NULL )
    {   
        // Remove item from list.
        interfaces = g_slist_remove_link(interfaces, item);

        // Free up item removed from list
        iface = (PNC_INTERFACE_T *)item->data;
        if ( iface->name != NULL )
            g_free(iface->name);
        if ( iface->networkType != NULL )
            g_free(iface->networkType);
        if ( iface->addressType != NULL )
            g_free(iface->addressType);
        if ( iface->address != NULL )
            g_free(iface->address);
        if ( iface->netmask != NULL )
            g_free(iface->netmask);
        if ( iface->gateway != NULL )
            g_free(iface->gateway);
        g_free(iface);
        g_slist_free(item);
    }
}

/*
 *========================================================================
 * Name:   pncGetMacAddress
 * Prototype:  char *pncGetMacAddress( char *name )
 *
 * Description:
 * Try to find the mac address for the named interface.
 *
 * Notes:
 * Caller must free the returned string, if not null.
 *========================================================================
 */
char * 
pncGetMacAddress( char *name )
{
    struct ifreq ifr;;
    int sock, j, k;
    char *p, mac[32];

    // Find mac address
    memset(mac, 0, 32);
    sock = socket(PF_INET, SOCK_STREAM, 0);
    if ( sock == -1 )
        piboxLogger(LOG_ERROR, "Couldn't get socket for mac address.\n");
    else
    {
        strncpy(ifr.ifr_name, name, sizeof(ifr.ifr_name)-1);
        ifr.ifr_name[ sizeof(ifr.ifr_name)-1 ] = '\0';
        if ( ioctl(sock, SIOCGIFHWADDR, &ifr) == -1 )
            piboxLogger(LOG_ERROR, "Failed ioctl to get mac address.\n");
        else
        {
            for (j=0, k=0; j<6; j++) 
            {
                k+=snprintf(mac+k, sizeof(mac)-k-1, j ? ":%02X" : "%02X",
                    (int)(unsigned int)(unsigned char)ifr.ifr_hwaddr.sa_data[j]);
            }
            mac[sizeof(mac)-1]='\0';
        }
    }
    if ( strlen(mac) > 0 )
        return g_strdup(mac);
    return NULL;
}


/*
 *========================================================================
 * Name:   pncLoadInterfaces
 * Prototype:  void pncLoadInterfaces( void )
 *
 * Description:
 * Read the file /etc/network/interfaces into a public data structure.
 *
 * Notes:
 * /etc/network/interfaces can be complex to parse.  See 
 * http://git.busybox.net/busybox/tree/networking/ifupdown.c for an example.
 * However, on the Pi or BeagleBoard we know that we only have two interfaces
 * to deal with: wired (eth0) and wireless (wlan0).  This allows us to build
 * a simple state machine for parsing the content of the interfaces file.
 *
 * Other limitations placed on our version of the interfaces file:
 * 1. No continuation characters.  They shouldn't be needed.
 * 2. No allow- stanzas.
 * 3. No mapping stanzas.
 * 4. No source stanzas.
 *
 * Interfaces file format based on
 * http://www.unix.com/man-page/Linux/5/interfaces/
 *
 * This function is thread-safe.
 *========================================================================
 */
void 
pncLoadInterfaces( void )
{
    FILE *fd;
    char buf[128];
    char *tsave     = NULL;
    char *tok       = NULL;
    char *ptr       = NULL;
    char *name      = NULL;
    char *netType   = NULL;
    char *addrType  = NULL;
    char *value     = NULL;
    char *filename  = NULL;
    char *mac       = NULL;

    PNC_INTERFACE_T *iface = NULL;
    GSList          *item  = NULL;
    PNC_INTERFACE_T *entry = NULL;

    // Clear any existing list.
    pncClearInterfaces();

    // Open the file for reading.
    filename = pncGetFilename( PNC_ID_INTERFACES );
    piboxLogger(LOG_INFO, "loadInterface: %s\n", filename);
    fd = fopen(filename, "r");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open interfaces file: %s - %s\n", filename, strerror(errno));
        g_free( filename );
        return;
    }
    g_free( filename );

    // Parse the file into a link list of interface definitions
    memset(buf, 0, 128);
    while( fgets(buf, 127, fd) != NULL )
    {
        // Ignore comments
        if ( buf[0] == '#' )
            continue;

        // Strip leading white space
        ptr = buf;
        ptr = piboxTrim(ptr);

        // Ignore blank lines
        if ( strlen(ptr) == 0 )
            continue;

        // Strip newline
        piboxStripNewline(ptr);

        // Grab first token
        piboxLogger(LOG_TRACE1, "line: %s\n", ptr);
        tok = strtok_r(ptr, " ", &tsave);
        piboxLogger(LOG_TRACE1, "tok: %s\n", tok);

        // Is this an auto statement?
        if ( strcmp(tok, INTERFACE_OPTIONS[N_AUTO]) == 0 )
        {
            piboxLogger(LOG_TRACE1, "Found AUTO statement.\n");
            // Do nothing - all configured interfaces are auto up.
            continue;
        }
        
        // Is this an iface stanza?
        else if ( strcmp(tok, INTERFACE_OPTIONS[N_IFACE]) == 0 )
        {
            piboxLogger(LOG_TRACE1, "Found IFACE statement.\n");

            // Parse options: interface name, network type, address type
            if ( name != NULL )
            {
                g_free(name);
                name = NULL;
            }
            name = g_strdup(strtok_r(NULL, " ", &tsave));
            netType = strtok_r(NULL, " ", &tsave);
            addrType = strtok_r(NULL, " ", &tsave);
            piboxLogger(LOG_INFO, "name/netType/addrType: %s, %s, %s\n", name, netType, addrType);

            // Ignore loopback - it's not configurable
            if ( strcmp(name, LOOPBACK) == 0 )
            {
                g_free(name);
                name = NULL;
                continue;
            }

            // Make sure it's one of the devices configured in the system.
            item = g_slist_find_custom(netInterfaces, name, (GCompareFunc)findNetInterface);
            if ( item == NULL )
            {
                piboxLogger(LOG_INFO, "%s: %s is not a real interface; skipping\n", __FUNCTION__, name);
                g_free(name);
                name = NULL;
                continue;
            }
            piboxLogger(LOG_INFO, "%s: %s is a valid interface; configuring.\n", __FUNCTION__, name);

            // Find mac address
            mac = pncGetMacAddress(name);

            // Create interface definition
            piboxLogger(LOG_TRACE1, "%s(%d): Calling pncNewInterface\n", __FUNCTION__, __LINE__);
            pncNewInterface( name );
            pncSetInterfaceField( name, PNC_ID_IF_NETTYPE, netType );
            pncSetInterfaceField( name, PNC_ID_IF_ADDRTYPE, addrType );
            pncSetInterfaceState( name, 1 );
            if ( mac != NULL )
            {
                pncSetInterfaceField( name, PNC_ID_IF_MAC, mac );
                g_free(mac);
            }
        }
        
        // Is this an address option?
        else if ( strcmp(tok, INTERFACE_OPTIONS[N_ADDRESS]) == 0 )
        {
            piboxLogger(LOG_TRACE1, "Found ADDRESS statement for ifname=%s\n", name);
            value = strtok_r(NULL, " ", &tsave);
            if ( name != NULL )
                pncSetInterfaceField( name, PNC_ID_IF_ADDR, value );
            else
                piboxLogger(LOG_TRACE1, 
                    "Address option outside of valid interface stanza (ignoring): %s\n", tok);
        }
        
        // Is this an netmask option?
        else if ( strcmp(tok, INTERFACE_OPTIONS[N_NETMASK]) == 0 )
        {
            piboxLogger(LOG_TRACE1, "Found NETMASK statement for ifname=%s.\n", name);
            value = strtok_r(NULL, " ", &tsave);
            if ( name != NULL )
                pncSetInterfaceField( name, PNC_ID_IF_MASK, value );
            else
                piboxLogger(LOG_TRACE1, 
                    "Netmask option outside of valid interface stanza (ignoring): %s\n", tok);
        }
        
        // Is this an gateway option?
        else if ( strcmp(tok, INTERFACE_OPTIONS[N_GATEWAY]) == 0 )
        {
            piboxLogger(LOG_TRACE1, "Found GATEWAY statement for ifname=%s.\n", name);
            value = strtok_r(NULL, " ", &tsave);
            if ( name != NULL )
                pncSetInterfaceField( name, PNC_ID_IF_GATEWAY, value );
            else
                piboxLogger(LOG_TRACE1, 
                    "Gateway option outside of valid interface stanza (ignoring): %s\n", tok);
        }

        // All other stanza and options are silently ignored.
        
    }

    // Close the file
    fclose(fd);

    // DEBUG
    pncPrintInterfaces();
}

/*
 *========================================================================
 * Name:   pncGetInterfaces
 * Prototype:  GSList *pncGetInterfaces()
 *
 * Returns:
 * A pointer to the GSList holding interface configurations.
 *
 * Description:
 * Retrieve the list of interfaces found in /etc/network/interfaces.
 *========================================================================
 */
GSList * 
pncGetInterfaces( void )
{
    return interfaces;
}

/*
 *========================================================================
 * Name:   pncGetInterface0
 * Prototype:  gchar *pncGetInterface0()
 *
 * Description:
 * Retrieve the first interface in the list of network interfaces.
 *
 * Returns:
 * A buffer containing the name of the first interface found in the list of interfaces.
 * Caller is responsible for freeing the returned buffer.
 *========================================================================
 */
gchar * 
pncGetInterface0( void )
{
    GSList          *item  = NULL;
    PNC_INTERFACE_T *entry = NULL;

    if ( interfaces == NULL )
        return NULL;
    entry = (PNC_INTERFACE_T *)g_slist_nth_data(interfaces, 0);
    if ( entry == NULL )
        return NULL;
    return g_strdup(entry->name);
}

/*
 *========================================================================
 * Name:   pncClearNetInterfaces
 * Prototype:  void pncClearNetInterfaces( void )
 *
 * Description:
 * Free up all interface definition from the stored link list, if any.
 *========================================================================
 */
void 
pncClearNetInterfaces( void )
{
    if ( netInterfaces == NULL )
        return;

    // Clear any existing list.
    g_slist_foreach(netInterfaces, clearNetInterface, NULL);
    g_slist_free(netInterfaces);
    netInterfaces = NULL;
}

/*
 *========================================================================
 * Name:   pncLoadNet
 * Prototype:  void pncLoadNet( void )
 *
 * Description:
 * Read in available network interfaces into a GSList structure.
 *========================================================================
 */
void 
pncLoadNet( void )
{
    DIR *pdir = NULL;
    struct dirent *pent = NULL;
    int count = 0;
    char *dirname;

    pncClearNetInterfaces();

    dirname = pncGetFilename( PNC_ID_NETIFACES );
    piboxLogger(LOG_INFO, "pncLoadNet: %s\n", dirname);
    pdir = opendir( dirname );
    if (pdir == NULL)
        return;
    while( (pent = readdir(pdir)) != NULL )
    {
        // Skip entries that aren't wired or wireless
        if ( strncmp(pent->d_name, ".", 1) == 0 )
            continue;
        if ( strncmp(pent->d_name, "lo", 2) == 0 )
            continue;
        if ( strncmp(pent->d_name, "vir", 3) == 0 )
            continue;
        netInterfaces = g_slist_append(netInterfaces, g_strdup(pent->d_name));
    }
    closedir( pdir );
}

/*
 *========================================================================
 * Name:   pncGetNetInterfaces
 * Prototype:  GSList *pncGetNetInterfaces()
 *
 * Returns:
 * A pointer to the GSList holding network device names.
 *
 * Description:
 * Retrieve the list of network devices found in /sys/class/net.
 *========================================================================
 */
GSList * 
pncGetNetInterfaces( void )
{
    return netInterfaces;
}

/*
 *========================================================================
 * Name:   pncNetInterfaceValid
 * Prototype:  GSList *pncNetInterfaceValid( gchar *name )
 *
 * Description:
 * Determine if the named interface exists.
 *
 * Arguments:
 * name   Character name of the network interface to validate.
 *
 * Returns:
 * 0 if the interface does not exist in the kernel.
 * 1 if the interface does exist in the kernel.
 *========================================================================
 */
gint
pncNetInterfaceValid( gchar *name )
{
    GSList *item;
    if ( netInterfaces == NULL )
        return 0;
    if ( name == NULL )
        return 0;
    item = g_slist_find_custom(netInterfaces, name, (GCompareFunc)findNetInterface);
    if ( item == NULL )
        return 0;
    return 1;
}

/*
 *========================================================================
 * Name:   pncClearWPA
 * Prototype:  void pncClearWPA( void )
 *
 * Description:
 * Free up all interface definition from the stored link list, if any.
 *========================================================================
 */
void 
pncClearWPA( void )
{
    if ( wpa == NULL )
        return;

    // Clear any existing list.
    g_slist_foreach(wpa, clearWPA, NULL);
    g_slist_free(wpa);
    wpa = NULL;
}

/*
 *========================================================================
 * Name:   pncLoadWPA
 * Prototype:  void pncLoadWPA( void )
 *
 * Description:
 * Read the file /etc/wpa_supplicant.conf into a public data structure.
 *========================================================================
 */
void 
pncLoadWPA( void )
{
    FILE *fd;
    char buf[128];
    char *filename;
    char *tsave     = NULL;
    char *tok       = NULL;
    char *ptr       = NULL;
    char *ptrStart  = NULL;
    char *field     = NULL;
    int  inNetwork  = 0;
    int  i;

    GSList *item;

    // Clear the list before loading it.
    pncClearWPA();

    // Create the list entries - we pre-allocate them.
    for(i=0; i<PNC_ID_WPA_MAX; i++)
    {
        wpa = g_slist_append(wpa, NULL);
        item = g_slist_nth(wpa, i);
        item->data = NULL;
    }

    filename = pncGetFilename( PNC_ID_WPASUPPLICANT );
    piboxLogger(LOG_INFO, "pncLoadWPA: %s\n", filename);
    fd = fopen(filename, "r");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open wpa_supplicant file: %s - %s\n", filename, strerror(errno));
        goto skipWPARead;
    }

    memset(buf, 0, 128);
    while( fgets(buf, 127, fd) != NULL )
    {
        // Ignore comments
        if ( buf[0] == '#' )
            continue;

        // Strip leading white space
        ptr = buf;
        ptr = piboxTrim(ptr);

        // Ignore blank lines
        if ( strlen(ptr) == 0 )
            continue;

        // Strip newline
        piboxStripNewline(ptr);

        if (strncmp(ptr, "}", 1) == 0 )
        {
            inNetwork = 0;
            continue;
        }

        // Grab first token
        tok = strtok_r(ptr, "=", &tsave);

        // Is this a nameserver statement?
        if ( strcasecmp(tok, WPA_NETWORK) == 0 )
        {
            inNetwork = 1;
        }
        else if ( inNetwork && (strcasecmp(tok, WPA_OPTIONS[PNC_ID_WPA_SSID]) == 0) )
        {
            /* This one requires stripping surrounding quotes. */
            field = strtok_r(NULL, "=", &tsave);
            ptr = field + strlen(field) - 1;
            if ( *ptr == '\"' ) *ptr = '\0';
            ptr = field;
            if ( *ptr == '\"' ) ptr++;
            item = g_slist_nth(wpa, PNC_ID_WPA_SSID);
            item->data = g_strdup(ptr);
        }
        else if ( inNetwork && (strcasecmp(tok, WPA_OPTIONS[PNC_ID_WPA_SCANSSID]) == 0) )
        {
            field = strtok_r(NULL, "=", &tsave);
            item = g_slist_nth(wpa, PNC_ID_WPA_SCANSSID);
            item->data = g_strdup(field);
        }
        else if ( inNetwork && (strcasecmp(tok, WPA_OPTIONS[PNC_ID_WPA_PROTO]) == 0) )
        {
            field = strtok_r(NULL, "=", &tsave);
            item = g_slist_nth(wpa, PNC_ID_WPA_PROTO);
            item->data = g_strdup(field);
        }
        else if ( inNetwork && (strcasecmp(tok, WPA_OPTIONS[PNC_ID_WPA_KEYMGMT]) == 0) )
        {
            field = strtok_r(NULL, "=", &tsave);
            item = g_slist_nth(wpa, PNC_ID_WPA_KEYMGMT);
            if ( strcasecmp(field, "WPA-PSK") == 0 )
                item->data = g_strdup( "WPA & WPA2 Personal" );
            else if ( strcasecmp(field, "WPA-EAP") == 0 )
                item->data = g_strdup( "WPA & WPA2 Enterprise" );
        }
        else if ( inNetwork && (strcasecmp(tok, WPA_OPTIONS[PNC_ID_WPA_PSK]) == 0) )
        {
            /* This one requires stripping surrounding quotes. */
            field = strtok_r(NULL, "=", &tsave);
            ptr = field + strlen(field) - 1;
            if ( *ptr == '\"' ) *ptr = '\0';
            ptr = field;
            if ( *ptr == '\"' ) ptr++;
            item = g_slist_nth(wpa, PNC_ID_WPA_PSK);
            item->data = g_strdup(ptr);
        }
        else if ( inNetwork && (strcasecmp(tok, WPA_OPTIONS[PNC_ID_WPA_PAIRWISE]) == 0) )
        {
            field = strtok_r(NULL, "=", &tsave);
            item = g_slist_nth(wpa, PNC_ID_WPA_PAIRWISE);
            item->data = g_strdup(field);
        }
        else if ( inNetwork && (strcasecmp(tok, WPA_OPTIONS[PNC_ID_WPA_GROUP]) == 0) )
        {
            field = strtok_r(NULL, "=", &tsave);
            item = g_slist_nth(wpa, PNC_ID_WPA_GROUP);
            item->data = g_strdup(field);
        }
    }
    fclose(fd);

skipWPARead:
    g_free( filename );

    // Fill in fields that should be there, if they aren't already.
    item = g_slist_nth(wpa, PNC_ID_WPA_SCANSSID);
    if ( item->data == NULL )
        item->data = g_strdup("1");
    item = g_slist_nth(wpa, PNC_ID_WPA_PROTO);
    if ( item->data == NULL )
        item->data = g_strdup("WPA2");
    item = g_slist_nth(wpa, PNC_ID_WPA_PAIRWISE);
    if ( item->data == NULL )
        item->data = g_strdup("CCMP");
    item = g_slist_nth(wpa, PNC_ID_WPA_GROUP);
    if ( item->data == NULL )
        item->data = g_strdup("CCMP");
}

/*
 *========================================================================
 * Name:   pncGetWPA
 * Prototype:  GSList *pncGetWPA()
 *
 * Returns:
 * A pointer to the GSList holding wpa fields.
 *
 * Description:
 * Retrieve the current wpa supplicant configuration.
 *========================================================================
 */
GSList * 
pncGetWPA( void )
{
    return wpa;
}

/*
 *========================================================================
 * Name:   pncClearResolv
 * Prototype:  void pncClearResolv( void )
 *
 * Description:
 * Free up all DNS definition from the stored link list, if any.
 *========================================================================
 */
void 
pncClearResolv( void )
{
    if ( nameservers == NULL )
        return;

    // Clear any existing list.
    g_slist_foreach(nameservers, clearNameserver, NULL);
    g_slist_free(nameservers);
    nameservers = NULL;
}

/*
 *========================================================================
 * Name:   pncLoadResolv
 * Prototype:  void pncLoadResolv( void )
 *
 * Description:
 * Read the file /etc/resolv.conf into a public data structure.
 *========================================================================
 */
void 
pncLoadResolv( void )
{
    FILE *fd;
    char buf[128];
    char *filename;
    char *tsave     = NULL;
    char *tok       = NULL;
    char *ptr       = NULL;
    char *address   = NULL;
    int  i;

    GSList *item;

    // Clear any existing config.
    pncClearResolv();

    // First, create three entries.  There must always be three entries in this list.
    nameservers = g_slist_append(nameservers, NULL);
    nameservers = g_slist_append(nameservers, NULL);
    nameservers = g_slist_append(nameservers, NULL);
    item = g_slist_nth(nameservers, 0);
    item->data = NULL;
    item = g_slist_nth(nameservers, 1);
    item->data = NULL;
    item = g_slist_nth(nameservers, 2);
    item->data = NULL;

    filename = pncGetFilename( PNC_ID_RESOLV );
    piboxLogger(LOG_INFO, "pncLoadResolv: %s\n", filename);
    fd = fopen(filename, "r");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open resolv.conf file: %s - %s\n", filename, strerror(errno));
        g_free(filename);
        return;
    }
    g_free(filename);

    memset(buf, 0, 128);
    while( fgets(buf, 127, fd) != NULL )
    {
        // Ignore comments
        if ( buf[0] == '#' )
            continue;

        // Strip leading white space
        ptr = buf;
        ptr = piboxTrim(ptr);

        // Ignore blank lines
        if ( strlen(ptr) == 0 )
            continue;

        // Strip newline
        piboxStripNewline(ptr);

        // Grab first token
        tok = strtok_r(ptr, " ", &tsave);

        // Is this a nameserver statement?
        if ( strcmp(tok, RESOLV_OPTIONS[N_NAMESERVER]) == 0 )
        {
            address = strtok_r(NULL, " ", &tsave);

            // We should check for a valid IP here!
            if ( address == NULL )
                continue;
            if ( strlen(address) == 0 )
                continue;

            // Add it to the first empty entry.
            for(i=0; i<3; i++)
            {
                item = g_slist_nth(nameservers, i);
                if ( item->data == NULL )
                {
                    item->data = g_strdup(address);
                    break;
                }
            }
        }
    }

    fclose(fd);
}

/*
 *========================================================================
 * Name:   pncGetNameservers
 * Prototype:  GSList *pncGetNameservers()
 *
 * Returns:
 * A pointer to the GSList holding nameservers.
 *
 * Description:
 * Retrieve the list of nameservers found in /etc/resolv.conf.
 *========================================================================
 */
GSList * 
pncGetNameservers( void )
{
    return nameservers;
}

/*
 *========================================================================
 * Name:   pncGetNameserver
 * Prototype:  gchar *pncGetNameserver( gint idx )
 *
 * Description:
 * Retrieve the specified nameserver.
 *
 * Returns:
 * A buffer containing the nameserver address or NULL if the index is invalid.
 * The caller is responsible for freeing the buffer.
 *========================================================================
 */
gchar * 
pncGetNameserver( gint idx )
{
    GSList *nameservers = NULL;
    gchar  *name;

    nameservers = pncGetNameservers();
    if ( nameservers == NULL )
        return NULL;

    name = g_slist_nth_data(nameservers, idx);
    return g_strdup(name);
}

/*
 *========================================================================
 * Name:   pncSetResolvField
 * Prototype:  void pncSetResolvField( gchar *dns0, gchar *dns1, gchar *dns2 )
 *
 * Description:
 * Write out the DNS configuration to the named file.
 * 
 * Arguments:
 * dns0          Name or IP address of first nameserver
 * dns1          Name or IP address of second nameserver
 * dns2          Name or IP address of third nameserver
 *========================================================================
 */
void 
pncSetResolvField( gchar *dns0, gchar *dns1, gchar *dns2 )
{
    GSList *item;
    gchar  *name;

    piboxLogger(LOG_INFO, "%s: args = %s %s %s\n", __FUNCTION__, dns0, dns1, dns2);

    if ( nameservers == NULL )
        return;

    // Iterate over the interfaces, writing to the specified file.
    item = g_slist_nth(nameservers, 0);
    if ( item->data != NULL )
    {
        g_free(item->data);
        item->data = NULL;
    }
    if ( (dns0 != NULL) && (dns0 != NULL) ) 
        item->data = g_strdup(dns0);

    item = g_slist_nth(nameservers, 1);
    if ( item->data != NULL )
    {
        g_free(item->data);
        item->data = NULL;
    }
    if ( (dns1 != NULL) && (dns1 != NULL) ) 
        item->data = g_strdup(dns1);

    item = g_slist_nth(nameservers, 2);
    if ( item->data != NULL )
    {
        g_free(item->data);
        item->data = NULL;
    }
    if ( (dns2 != NULL) && (dns2 != NULL) ) 
        item->data = g_strdup(dns2);
}

/*
 *========================================================================
 * Name:   pncClearHostAP
 * Prototype:  void pncClearHostAP( void )
 *
 * Description:
 * Free up all interface definition from the stored link list, if any.
 *========================================================================
 */
void 
pncClearHostAP( void )
{
    if ( hostap == NULL )
        return;

    // Clear any existing list.
    g_slist_foreach(hostap, clearHostAP, NULL);
    g_slist_free(hostap);
    hostap = NULL;
}

/*
 *========================================================================
 * Name:   pncLoadHostAP
 * Prototype:  void pncLoadHostAP()
 *
 * Description:
 * Read the file /etc/hostapd.conf into a public data structure.
 *
 * Note: silently ignores unknown tokens
 *========================================================================
 */
void
pncLoadHostAP( void )
{
    FILE *fd;
    char buf[128];
    char *filename;
    char *tsave     = NULL;
    char *tok       = NULL;
    char *ptr       = NULL;
    char *ptrStart  = NULL;
    char *field     = NULL;
    int  i;

    GSList *item;

    // Clear list before loading it.
    pncClearHostAP();

    // Create the list entries - we pre-allocate them.
    for(i=0; i<PNC_ID_HOSTAP_MAX; i++)
    {
        hostap = g_slist_append(hostap, NULL);
        item = g_slist_nth(hostap, i);
        item->data = NULL;
    }

    filename = pncGetFilename( PNC_ID_HOSTAPDCONF );
    piboxLogger(LOG_INFO, "pncLoadHostAP: %s\n", filename);
    fd = fopen(filename, "r");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open hostapd file: %s - %s\n", filename, strerror(errno));
        goto skipHostAPRead;
    }

    memset(buf, 0, 128);
    while( fgets(buf, 127, fd) != NULL )
    {
        // Ignore comments
        if ( buf[0] == '#' )
            continue;

        // Strip leading white space
        ptr = buf;
        ptr = piboxTrim(ptr);

        // Ignore blank lines
        if ( strlen(ptr) == 0 )
            continue;

        // Strip newline
        piboxStripNewline(ptr);

        // Grab first token
        tok = strtok_r(ptr, "=", &tsave);

        if ( strcasecmp(tok, HOSTAP_OPTIONS[PNC_ID_HAP_INTERFACE]) == 0 )
        {
            field = strtok_r(NULL, "=", &tsave);
            item = g_slist_nth(hostap, PNC_ID_HAP_INTERFACE);
            item->data = g_strdup(field);
        }
        else if ( strcasecmp(tok, HOSTAP_OPTIONS[PNC_ID_HAP_DRIVER]) == 0 )
        {
            field = strtok_r(NULL, "=", &tsave);
            item = g_slist_nth(hostap, PNC_ID_HAP_DRIVER);
            item->data = g_strdup(field);
        }
        else if ( strcasecmp(tok, HOSTAP_OPTIONS[PNC_ID_HAP_SSID]) == 0 )
        {
            field = strtok_r(NULL, "=", &tsave);
            item = g_slist_nth(hostap, PNC_ID_HAP_SSID);
            item->data = g_strdup(field);
        }
        else if ( strcasecmp(tok, HOSTAP_OPTIONS[PNC_ID_HAP_CHANNEL]) == 0 )
        {
            field = strtok_r(NULL, "=", &tsave);
            item = g_slist_nth(hostap, PNC_ID_HAP_CHANNEL);
            item->data = g_strdup(field);
        }
        else if ( strcasecmp(tok, HOSTAP_OPTIONS[PNC_ID_HAP_AUTH_ALGS]) == 0 )
        {
            field = strtok_r(NULL, "=", &tsave);
            item = g_slist_nth(hostap, PNC_ID_HAP_AUTH_ALGS);
            item->data = g_strdup(field);
        }
        else if ( strcasecmp(tok, HOSTAP_OPTIONS[PNC_ID_HAP_WPA]) == 0 )
        {
            field = strtok_r(NULL, "=", &tsave);
            item = g_slist_nth(hostap, PNC_ID_HAP_WPA);
            item->data = g_strdup(field);
        }
        else if ( strcasecmp(tok, HOSTAP_OPTIONS[PNC_ID_HAP_WPA_PSK_FILE]) == 0 )
        {
            field = strtok_r(NULL, "=", &tsave);
            if ( !isCLIFlagSet( CLI_TEST) )
                pncSetFilename( PNC_ID_HOSTAPDPSK, field );
        }
        else if ( strcasecmp(tok, HOSTAP_OPTIONS[PNC_ID_HAP_WPA_KEY_MGMT]) == 0 )
        {
            field = strtok_r(NULL, "=", &tsave);
            item = g_slist_nth(hostap, PNC_ID_HAP_WPA_KEY_MGMT);
            item->data = g_strdup(field);
        }
        else if ( strcasecmp(tok, HOSTAP_OPTIONS[PNC_ID_HAP_RSN_PAIRWISE]) == 0 )
        {
            field = strtok_r(NULL, "=", &tsave);
            item = g_slist_nth(hostap, PNC_ID_HAP_RSN_PAIRWISE);
            item->data = g_strdup(field);
        }
    }

skipHostAPRead:
    g_free(filename);

    // Fill in fields that should be there, if they aren't already.
    item = g_slist_nth(hostap, PNC_ID_HAP_INTERFACE);
    if ( item->data == NULL )
        item->data = g_strdup("wlan0");
    item = g_slist_nth(hostap, PNC_ID_HAP_DRIVER);
    if ( item->data == NULL )
        item->data = g_strdup("nl80211");
    item = g_slist_nth(hostap, PNC_ID_HAP_AUTH_ALGS);
    if ( item->data == NULL )
        item->data = g_strdup("1");
    item = g_slist_nth(hostap, PNC_ID_HAP_WPA);
    if ( item->data == NULL )
        item->data = g_strdup("2");
    item = g_slist_nth(hostap, PNC_ID_HAP_WPA_PSK_FILE);
    filename = pncGetFilename( PNC_ID_HOSTAPDPSK );
    if ( item->data == NULL )
        item->data = g_strdup( filename );
    g_free( filename );
    item = g_slist_nth(hostap, PNC_ID_HAP_WPA_KEY_MGMT);
    if ( item->data == NULL )
        item->data = g_strdup("WPA-PSK");
    item = g_slist_nth(hostap, PNC_ID_HAP_RSN_PAIRWISE);
    if ( item->data == NULL )
        item->data = g_strdup("CCMP");
    item = g_slist_nth(hostap, PNC_ID_HAP_WPA_PAIRWISE);
    if ( item->data == NULL )
        item->data = g_strdup("CCMP");
    item = g_slist_nth(hostap, PNC_ID_HAP_MACADDR_ACL);
    if ( item->data == NULL )
        item->data = g_strdup("0");
    item = g_slist_nth(hostap, PNC_ID_HAP_IGNORE_BROADCAST_SSID);
    if ( item->data == NULL )
        item->data = g_strdup("0");
    item = g_slist_nth(hostap, PNC_ID_HAP_HW_MODE);
    if ( item->data == NULL )
        item->data = g_strdup("g");
}

/*
 *========================================================================
 * Name:   pncGetHostAP
 * Prototype:  GSList *pncGetHostAP()
 *
 * Returns:
 * A pointer to the GSList holding hostap fields.
 *
 * Description:
 * Retrieve the current hostapd configuration.
 *========================================================================
 */
GSList * 
pncGetHostAP( void )
{
    return hostap;
}

/*
 *========================================================================
 * Name:   pncGetHostAPPassword
 * Prototype:  gchar *pncGetHostAPPassword( gchar *path )
 *
 * Arguments:
 * gchar *path   Fully qualified path to the hostapd-psk file.
 *
 * Returns:
 * A pointer to the password string or NULL if no password is found.
 * Caller is responsible for free'ing the returned pointer.
 *
 * Description:
 * Retrieve the current hostapd password, if any.
 *========================================================================
 */
gchar * 
pncGetHostAPPassword( gchar *path )
{
    FILE *fd;
    char buf[512];
    char *tsave     = NULL;
    char *tok       = NULL;
    char *ptr       = NULL;

    gchar *password = NULL;
    struct stat stat_buf;

    if ( path == NULL )
        return NULL;

    // Make sure file exists.
    if ( stat(path, &stat_buf) != 0 )
        return NULL;

    // Open file and read it - should be just one line.
    piboxLogger(LOG_INFO, "pncGetHostAPPassword: %s\n", path);
    fd = fopen(path, "r");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open hostapd-psk file: %s - %s\n", path, strerror(errno));
        return NULL;
    }

    // Max line length: 511 characters
    memset(buf, 0, 512);
    while( fgets(buf, 511, fd) != NULL )
    {
        // Ignore comments
        if ( buf[0] == '#' )
            continue;

        // Strip leading white space
        ptr = buf;
        ptr = piboxTrim(ptr);

        // Ignore blank lines
        if ( strlen(ptr) == 0 )
            continue;

        // Strip newline
        piboxStripNewline(ptr);

        // Grab first token
        tok = strtok_r(ptr, " ", &tsave);
        if ( tok == NULL )
            return NULL;

        // Grab second token
        tok = strtok_r(NULL, " ", &tsave);
        if ( tok == NULL )
            return NULL;

        // We have the password.  Dup it, close the file and return.
        password = g_strdup(tok);
        fclose(fd);
        return password;
    }

    return password;
}

/*
 *========================================================================
 * Name:   pncReadNetType
 * Prototype:  void pncReadNetType( char *filename )
 *
 * Arguments:
 * char *filename   The name of the file to write to.
 *
 * Returns:
 * gint   One of PNC_ID_WIRELESS (wireless client), PNC_ID_WAP (wireless access point)
 *
 * Description:
 * Read the NetType file that says whether wireless client or hostAP is being used.
 *========================================================================
 */
gint 
pncReadNetType( char *filename )
{
    FILE *fd;
    char buf[512];

    // Open the configuration file
    piboxLogger(LOG_INFO, "loadNetType: %s\n", filename);
    fd = fopen(filename, "r");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR,"Failed to open NetType file: %s - %s\n", filename, strerror(errno));
        return PNC_ID_WIRELESS;
    }

    memset(buf, 0, 512);
    fgets(buf, 511, fd);
    fclose(fd);
    piboxStripNewline(buf);
    piboxLogger(LOG_INFO, "netType contents: *%s*\n", buf);

    // Defaults to wireless client.
    if ( strncmp(buf, "client", 6) == 0 )
    {
        piboxLogger(LOG_INFO, "netType file: client\n");
        return PNC_ID_WIRELESS;
    }
    else if ( strncmp(buf, "wap", 3) == 0 )
    {
        piboxLogger(LOG_INFO, "netType file: wap\n");
        return PNC_ID_WAP;
    }
    else 
    {
        piboxLogger(LOG_INFO, "netType file: unknown\n");
        return PNC_ID_WIRELESS;
    }
}

/*
 *========================================================================
 * Name:   pncGetHomeKey
 * Prototype:  gchar *pncGetHomeKey( void )
 *
 * Description:
 * Returns the key configured for use as the home button.
 *========================================================================
 */
gchar * 
pncGetHomeKey( void )
{
    return homeKey;
}

